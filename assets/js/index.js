// JavaScript Document
$(document).ready(function(){
	$("#gantimenu").click(function(){
		if ($(window).width() == "1366"){
			$("#menu").css("width","50px");
			$("#menu").css("transition","0.3s");
			$(".menu").css("width","50px");
			$(".menu").css("transition","0.3s");
			$(".menutitle").css("font-size","0px");
			$(".menutitle").css("transition","0.3s");			
			$("#gantimenu").css("display","none");
			$("#gantimenu1").css("display","inline");
			$("#content1").css("width","95.21%");	
			$("#content1").css("transition","0.3s");
			$(".admin").css("height","0px");
			$(".admin img").css("width","0px");
			$(".admin h3").css("font-size","0px");
			$(".admin h4").css("font-size","0px");		
			$(".admin_status_img").css("height","0px");
		}
		else if ($(window).width() == "1280"){
			$("#menu").css("width","50px");
			$("#menu").css("transition","0.3s");
			$(".menu").css("width","50px");
			$(".menu").css("transition","0.3s");
			$(".menu_teks a").css("font-size","0px");
			$(".menu_teks a").css("transition","0.3s");
			$(".menu_teks_akhir a").css("font-size","0px");
			$(".menu_teks_akhir a").css("transition","0.3s");
			$("#gantimenu").css("display","none");
			$("#gantimenu1").css("display","inline");
			$("#content1").css("width","95.21%");	
			$("#content1").css("transition","0.3s");
			$(".admin").css("height","0px");
			$(".admin img").css("width","0px");
			$(".admin h3").css("font-size","0px");
			$(".admin h4").css("font-size","0px");		
			$(".admin_status_img").css("height","0px");
		}
		else if ($(window).width() == "1024"){
			$("#menu").css("width","50px");
			$("#menu").css("transition","0.3s");
			$(".menu").css("width","50px");
			$(".menu").css("transition","0.3s");
			$(".menu_teks a").css("font-size","0px");
			$(".menu_teks a").css("transition","0.3s");
			$(".menu_teks_akhir a").css("font-size","0px");
			$(".menu_teks_akhir a").css("transition","0.3s");
			$("#gantimenu").css("display","none");
			$("#gantimenu1").css("display","inline");
			$("#content1").css("width","95.01%");	
			$("#content1").css("transition","0.3s");
			$(".admin").css("height","0px");
			$(".admin img").css("width","0px");
			$(".admin h3").css("font-size","0px");
			$(".admin h4").css("font-size","0px");		
			$(".admin_status_img").css("height","0px");
		}
		else if ($(window).width() == "800"){
			$("#menu").css("width","50px");
			$("#menu").css("transition","0.3s");
			$(".menu").css("width","50px");
			$(".menu").css("transition","0.3s");
			$(".menu_teks a").css("font-size","0px");
			$(".menu_teks a").css("transition","0.3s");
			$(".menu_teks_akhir a").css("font-size","0px");
			$(".menu_teks_akhir a").css("transition","0.3s");
			$("#gantimenu").css("display","none");
			$("#gantimenu1").css("display","inline");
			$("#content1").css("width","93.41%");	
			$("#content1").css("transition","0.3s");
			$(".admin").css("height","0px");
			$(".admin img").css("width","0px");
			$(".admin h3").css("font-size","0px");
			$(".admin h4").css("font-size","0px");		
			$(".admin_status_img").css("height","0px");
		}
	else {
		alert('Maksimalkan browser ke ukuran default, Support untuk lebar resolusi 1366px, 1280px, 1024px, 800px');
		}	
	});
	
	
	$("#gantimenu1").click(function(){   
		if ($(window).width() == "1366"){
			$("#menu").css("width","200px");
			$("#menu").css("transition","0.3s");
			$(".menu").css("width","200px");
			$(".menu").css("transition","0.3s");
			$(".menutitle").css("font-size","20px");
			$(".menutitle").css("transition","0.3s");
			$("#content1").css("width","85.054%");	
			$("#content1").css("transition","0.3s");
			$(".admin").css("height","80px");
			$(".admin").css("transition","0.3s");
			$(".admin img").css("width","35%");
			$(".admin img").css("transition","0.3s");
			$(".admin h3").css("font-size","16px");
			$(".admin h3").css("transition","0.3s");
			$(".admin h4").css("font-size","11px");
			$(".admin h4").css("transition","0.3s");
			$(".admin_status_img").css("height","70px");
			$("#gantimenu1").css("display","none");
			$("#gantimenu").css("display","inline");
		}
		else if ($(window).width() == "1280"){
			$("#menu").css("width","150px");
			$("#menu").css("transition","0.3s");
			$(".menu").css("width","150px");
			$(".menu").css("transition","0.3s");
			$(".menu_teks a").css("font-size","11px");
			$(".menu_teks a").css("transition","0.3s");
			$("#gantimenu1").css("display","none");
			$("#gantimenu").css("display","inline");	
			$(".menu_teks_akhir a").css("font-size","22px");
			$(".menu_teks_akhir a").css("transition","0.3s");
			$("#content1").css("width","88.054%");	
			$("#content1").css("transition","0.3s");
			$(".admin").css("height","80px");
			$(".admin").css("transition","0.3s");
			$(".admin img").css("width","35%");
			$(".admin img").css("transition","0.3s");
			$(".admin h3").css("font-size","16px");
			$(".admin h3").css("transition","0.3s");
			$(".admin h4").css("font-size","11px");
			$(".admin h4").css("transition","0.3s");
			$(".admin_status_img").css("height","60px");
			$(".admin_status_img").css("width","60px");
			$(".admin_status_det h4").css("font-size","0px");
			$(".admin_status_det h3").css("font-size","14px");	
		}
		else if ($(window).width() == "1024"){
			$("#menu").css("width","150px");
			$("#menu").css("transition","0.3s");
			$(".menu").css("width","150px");
			$(".menu").css("transition","0.3s");
			$(".menu_teks a").css("font-size","11px");
			$(".menu_teks a").css("transition","0.3s");
			$("#gantimenu1").css("display","none");
			$("#gantimenu").css("display","inline");	
			$(".menu_teks_akhir a").css("font-size","22px");
			$(".menu_teks_akhir a").css("transition","0.3s");
			$("#content1").css("width","84.054%");	
			$("#content1").css("transition","0.3s");
			$(".admin").css("height","80px");
			$(".admin").css("transition","0.3s");
			$(".admin img").css("width","35%");
			$(".admin img").css("transition","0.3s");
			$(".admin h3").css("font-size","16px");
			$(".admin h3").css("transition","0.3s");
			$(".admin h4").css("font-size","11px");
			$(".admin h4").css("transition","0.3s");
			$(".admin_status_img").css("height","60px");
			$(".admin_status_img").css("width","60px");
			$(".admin_status_det h4").css("font-size","0px");
			$(".admin_status_det h3").css("font-size","14px");			
		}
		else if ($(window).width() == "800"){
			$("#menu").css("width","120px");
			$("#menu").css("transition","0.3s");
			$(".menu").css("width","120px");
			$(".menu").css("transition","0.3s");
			$(".menu_teks a").css("font-size","11px");
			$(".menu_teks a").css("transition","0.3s");
			$("#gantimenu1").css("display","none");
			$("#gantimenu").css("display","inline");	
			$(".menu_teks_akhir a").css("font-size","22px");
			$(".menu_teks_akhir a").css("transition","0.3s");
			$("#content1").css("width","84.054%");	
			$("#content1").css("transition","0.3s");
			$(".admin").css("height","80px");
			$(".admin").css("transition","0.3s");
			$(".admin img").css("width","35%");
			$(".admin img").css("transition","0.3s");
			$(".admin h3").css("font-size","16px");
			$(".admin h3").css("transition","0.3s");
			$(".admin h4").css("font-size","11px");
			$(".admin h4").css("transition","0.3s");
			$(".admin_status_img").css("height","60px");
			$(".admin_status_img").css("width","60px");
			$(".admin_status_det h4").css("font-size","0px");
			$(".admin_status_det h3").css("font-size","13px");			
		}			 
	});

});
function batal(){
$(".simpan_jadwal").css("display","none");	
	}
function hari1(){
$(".simpan_jadwal").css("display","none");
document.tambah.dosen.value = document.clear();
document.tambah.kelas1.value = document.clear();	
document.tambah.matkul.value = document.clear();
document.tambah.ruang.value = document.clear();
document.tambah.status[0].checked = document.clear();
document.tambah.status[1].checked = document.clear();
document.tambah.dosen_status1.value = '';
document.tambah.dosen_cek1.value = '';
document.tambah.kelas_status1.value = '';
document.tambah.kelas_cek1.value = '';
document.tambah.ruang_status1.value = '';
document.tambah.ruang_cek1.value = '';
document.tambah.status_status.value = '';
document.tambah.status_cek.value = '';
}

function tekan(){
$(".simpan_jadwal").css("display","none");
document.tambah.dosen.value = document.clear();
document.tambah.kelas1.value = document.clear();	
document.tambah.ruang.value = document.clear();
document.tambah.status[0].checked = document.clear();
document.tambah.status[1].checked = document.clear();
var matkulstr=(document.tambah.matkul.value);
var jam1 = document.tambah.jam1.value;
document.tambah.dosen_status1.value = '';
document.tambah.dosen_cek1.value = '';
document.tambah.kelas_status1.value = '';
document.tambah.kelas_cek1.value = '';
document.tambah.ruang_status1.value = '';
document.tambah.ruang_cek1.value = '';
document.tambah.status_status.value = '';
document.tambah.status_cek.value = '';
$(".hasil").html("Tunggu sebentar ...")
	$.ajax({
		type:"post",
		url:"hitung_jadwal.php",
		data:"q="+ matkulstr+'&jam1='+jam1,
		success: function(data){
			$(".hasil").html(data);
			var durasi = $(".hasil").text();	
			document.tambah.jam2.value = durasi;
			document.tambah.dosen.value = document.clear();
		}
	});
}

function dosen1(){
$(".simpan_jadwal").css("display","none");	
document.tambah.kelas1.value = document.clear();	
document.tambah.ruang.value = document.clear();
document.tambah.status[0].checked = document.clear();
document.tambah.status[1].checked = document.clear();
document.tambah.kelas_status1.value = '';
document.tambah.kelas_cek1.value = '';
document.tambah.ruang_status1.value = '';
document.tambah.ruang_cek1.value = '';
document.tambah.status_status.value = '';
document.tambah.status_cek.value = '';
	var jam1 = document.tambah.jam1.value; 
	var jam2 = document.tambah.jam2.value;
	var dosenstr = document.tambah.dosen.value;
	var haristr = document.tambah.hari.value;
		$.ajax({
			type:"post",
			url:"cek.php",
			data:"nip="+ dosenstr+'&jam1='+jam1+'&jam2='+jam2+'&hari='+haristr,
			success: function(data){
				$(".hasil_dosen").html(data);
				var hasil3 = $(".hasil_dosen").text();	
				var explode = hasil3.split(',');
				var status = explode[0];
				var keterangan = explode[1];
				document.tambah.dosen_cek1.value = hasil3;
				document.tambah.dosen_status1.value = status;
				if(document.tambah.dosen_status1.value == "False"){
					$(".input30_status_dosen").css("color","red");
					$(".input30_status_dosen").css("font-weight","500");
					}
				else if(document.tambah.dosen_status1.value=="True"){
					$(".input30_status_dosen").css("color","green");
					$(".input30_status_dosen").css("font-weight","500");
					}
				}
		});

}

function kelastekan(){
$(".simpan_jadwal").css("display","none");		
document.tambah.ruang.value = document.clear();
document.tambah.status[0].checked = document.clear();
document.tambah.status[1].checked = document.clear();
document.tambah.ruang_status1.value = '';
document.tambah.ruang_cek1.value = '';
document.tambah.status_status.value = '';
document.tambah.status_cek.value = '';
	var jam1 = document.tambah.jam1.value; 
	var jam2 = document.tambah.jam2.value;
	var kelasstr = document.tambah.kelas1.value;
	var haristr = document.tambah.hari.value;
		$.ajax({
			type:"post",
			url:"cek.php",
			data:"kd="+ kelasstr+'&jam1='+jam1+'&jam2='+jam2+'&hari='+haristr,
			success: function(data){
				$(".hasil_kelas").html(data);
				var hasil3 = $(".hasil_kelas").text();	
				var explode = hasil3.split(',');
				var status = explode[0];
				var keterangan = explode[1];
				document.tambah.kelas_cek1.value = hasil3;
				document.tambah.kelas_status1.value = status;
				if(document.tambah.kelas_status1.value == "False"){
					$(".input30_status_kelas").css("color","red");
					$(".input30_status_kelas").css("font-weight","500");
					}
				else if(document.tambah.kelas_status1.value=="True"){
					$(".input30_status_kelas").css("color","green");
					$(".input30_status_kelas").css("font-weight","500");
					}
				}
		});
	}



function ruang1(){	
$(".simpan_jadwal").css("display","none");	
document.tambah.status[0].checked = document.clear();
document.tambah.status[1].checked = document.clear();
document.tambah.status_status.value = '';
document.tambah.status_cek.value = '';
	var jam1 = document.tambah.jam1.value; 
	var jam2 = document.tambah.jam2.value;
	var ruangstr = document.tambah.ruang.value;
	var haristr = document.tambah.hari.value;
		$.ajax({
			type:"post",
			url:"cek.php",
			data:"ruang="+ ruangstr+'&jam1='+jam1+'&jam2='+jam2+'&hari='+haristr,
			success: function(data){
			$(".hasil_ruang").html(data);
				var hasil3 = $(".hasil_ruang").text();	
				var explode = hasil3.split(',');
				var status = explode[0];
				var keterangan = explode[1];
				document.tambah.ruang_cek1.value = hasil3;
				document.tambah.ruang_status1.value = status;
				if(document.tambah.ruang_status1.value == "False"){
					$(".input30_status_ruang").css("color","red");
					$(".input30_status_ruang").css("font-weight","500");
					}
				else if(document.tambah.ruang_status1.value=="True"){
					$(".input30_status_ruang").css("color","green");
					$(".input30_status_ruang").css("font-weight","500");
					}
				}
		});
	}



function status_klik(){
	$(".simpan_jadwal").css("display","none");	
	var kelasstr = document.tambah.kelas1.value;
	var matkulstr = document.tambah.matkul.value;
	var statusstr = document.tambah.status.value;
	$.ajax({
			type:"post",
			url:"cek.php",
			data:"status="+ statusstr+'&kelas='+kelasstr+'&matkul='+matkulstr,
			success: function(data){
			$(".hasil_status").html(data);
				var hasil3 = $(".hasil_status").text();	
				var explode = hasil3.split(',');
				var status = explode[0];
				var keterangan = explode[1];
				document.tambah.status_cek.value = hasil3;
				document.tambah.status_status.value = status;
				if(document.tambah.status_status.value == "False"){
					$(".input30_status_status").css("color","red");
					$(".input30_status_status").css("font-weight","500");
					}
				else if(document.tambah.status_status.value=="True"){
					$(".input30_status_status").css("color","green");
					$(".input30_status_status").css("font-weight","500");
					}
				}
		});
	}

function next_klik(){
$(".simpan_jadwal").css("display","none");	
var dosen = document.tambah.dosen_status1.value;
var kelas = document.tambah.kelas_status1.value;
var ruang = document.tambah.ruang_status1.value;
var status = document.tambah.status_status.value;
	if (dosen =="True" && kelas=="True" && ruang=="True" && status=="True"){
		$(".simpan_jadwal").css("display","inline");	
	}
	else if (dosen =="" || kelas=="" || ruang=="" || status==""){
		alert('Lengkapi semua!!!');	
	}
	else{
		alert ('Jika semua bernilai TRUE/BENAR makan akan di lanjut.');
		}
}
