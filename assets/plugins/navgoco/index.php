<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
<script type="text/javascript" src="jquery-latest.min.js"></script>
<script type="text/javascript" src="src/jquery.cookie.js"></script>
<script type="text/javascript" src="src/jquery.navgoco.js"></script>
<link rel="stylesheet" href="src/jquery.navgoco.css" type="text/css" media="screen" />
</head>

<body>
<ul class="nav">
    <li><a href="#">Menu</a>
    </li>
    <li><a href="#">Produk</a>
        <ul>
            <li><a href="#">1.1 Submenu</a></li>
            <li><a href="#">1.2 Submenu</a></li>
            <li><a href="#">1.3 Submenu</a></li>
        </ul>
    </li>
    <li><a href="#">Transaksi</a>
        <ul>
            <li><a href="#">1.1 Submenu</a></li>
            <li><a href="#">1.2 Submenu</a></li>
            <li><a href="#">1.3 Submenu</a></li>
        </ul>
    </li>
    <!-- etc... -->
</ul>

<script type="text/javascript">
$(document).ready(function() {
    $('.nav').navgoco();
});
</script>
</body>
</html>