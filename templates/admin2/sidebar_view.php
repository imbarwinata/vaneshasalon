<div id="menu">
	<div class="admin">
    	<div class="admin_status">
        	<img class="admin_status_img" src="../assets/img/tari.png">
            <div class="admin_status_det"><h3></h3><h4>Administrator</h4></div>
        </div>
    </div>
    
        <ul class="nav">
            <li><a href="home.php?page=status" style="background:none;"><div class="fa fa-cogs" style="padding-left:6px;"> &nbsp;</div>Status Salon</a></li>
            <li><a href="#" style="background:none;"><div class="fa fa-home"> &nbsp;</div>Home</a>
                <ul>
                    <li><a href="home.php?page=home_banner"><div class="fa fa-info"> &nbsp;</div>Banner</a></li>
                    <li><a href="home.php?page=why_vanesha"><div class="fa fa-info"> &nbsp;</div>Kenapa Vanesha ?</a></li>
                    <li><a href="home.php?page=testimony"><div class="fa fa-info"> &nbsp;</div>Testimony</a></li>
                </ul>
            </li>
            <li><a href="about.php?page=about" style="background:none;"><div class="fa fa-users" style="padding-left:6px;"> &nbsp;</div>Tentang Kami</a></li>
            <li><a href="#" style="background:none;"><div class="fa fa-product-hunt"> &nbsp;</div>Layanan</a>
                <ul>
                    <li><a href="services.php?page=services"><div class="fa fa-info"> &nbsp;</div>Layanan</a></li>
                    <li><a href="services.php?page=category"><div class="fa fa-info"> &nbsp;</div>Kategori</a></li>
                </ul>
            </li>
            <li><a href="article.php?page=article" style="background:none;"><div class="fa fa-newspaper-o"> &nbsp;</div><div class="menutitle">Artikel</div></a></li>
            <li><a href="#" style="background:none;"><div class="fa fa-user" style="background:none; padding-left: 6px;"> &nbsp;</div>Pelanggan</a>
                <ul>
                    <li><a href="member.php?page=city"><div class="fa fa-info"> &nbsp;</div>Kota</a></li>
                    <li><a href="member.php?page=location"><div class="fa fa-info"> &nbsp;</div>Lokasi</a></li>
                    <li><a href="member.php?page=member"><div class="fa fa-info"> &nbsp;</div>Data Pelanggan</a></li> 
                 </ul>
            </li>

            <li><a href="#" style="background:none;"><div class="fa fa-shopping-bag" style="background:none;"> &nbsp;</div>Pesanan</a>
                <ul>
                    <li><a href="member.php?page=order"><div class="fa fa-info"> &nbsp;</div>Daftar Pesanan</a></li>
                    <li><a href="member.php?page=confirmation"><div class="fa fa-info"> &nbsp;</div>Konfirmasi</a></li> 
                    <li><a href="member.php?page=report"><div class="fa fa-info"> &nbsp;</div>Laporan</a></li> 
                 </ul>
            </li>
            <li><a href="#" style="background:none;"><div class="fa fa-list" style="background:none; padding-left:5px;"> &nbsp;</div>Konten</a>
                <ul>
                    <li><a href="content.php?page=logo"><div class="fa fa-info"> &nbsp;</div>Logo</a></li>
                    <li><a href="content.php?page=contact_us"><div class="fa fa-info"> &nbsp;</div>Kontak Kami</a></li>
                    <li><a href="content.php?page=faq"><div class="fa fa-info"> &nbsp;</div>F.A.Q</a></li>
                    <li><a href="content.php?page=social_media_link"><div class="fa fa-info"> &nbsp;</div>Social Media Link</a></li>
                    <li><a href="content.php?page=privacy_policy"><div class="fa fa-info"> &nbsp;</div>Privacy and Policy</a></li>
                    <li><a href="content.php?page=terms_and_condition"><div class="fa fa-info"> &nbsp;</div>Terms and Condition</a></li>
                </ul>
            </li>
            <li><a class="onclick" onclick="if(confirm('Yakin akan keluar ?')){ window.location.href=('home.php?page=logout') }" style="background:none;"><div class="fa fa-sign-out"> &nbsp;</div>Log Out</a></li>
            <br><br><br>
            <!-- etc... -->
        </ul> 
</div>
<style>
    .onclick:hover{ cursor: pointer;  }
</style>