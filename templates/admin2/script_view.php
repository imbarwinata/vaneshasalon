<!-- ./wrapper -->
<style type="text/css">
  .dataTables_filter {
     width: 50%;
     float: right;
     text-align: right;
  }
  .dataTables_paginate{
     width: 50%;
     float: right;
     text-align: right;
  }
</style>
<script src="../assets/js/jquery-1.12.0.min.js"></script>
<script type="text/javascript" src="../assets/js/jquery-latest.min.js"></script>
<script type="text/javascript" src="../assets/plugins/navgoco/src/jquery.cookie.js"></script>
<script type="text/javascript" src="../assets/plugins/navgoco/src/jquery.navgoco.js"></script>
<script src="../assets/js/index.js"></script>
<?php include('../templates/admin/datatable_view.php'); ?>
<script type="text/javascript">
$(document).ready(function() {
    $('.nav').navgoco();
});
</script>
<script src="../assets/plugins/bootstrap-file-upload/bootstrap-fileupload.js"></script>
<script src="../assets/plugins/ckeditor_full/ckeditor.js"  language="javascript" type="text/javascript"></script>
<script>
    var roxyFileman = '../assets/plugins/ckeditor_full/plugins/fileman/index.html';
    $(function () {
        CKEDITOR.replace('ckeditor1', {filebrowserBrowseUrl: roxyFileman,
            filebrowserImageBrowseUrl: roxyFileman + '?type=image',
            removeDialogTabs: 'link:upload;image:upload'});
    });
</script>
<script src="../assets/plugins/fontawesome-picker/js/fontawesome-iconpicker.min.js"></script>
<script type="text/javascript">
    $(".icon_picker").iconpicker();
    $("ul.nav li ul li a" ).hover(function() {
      $("ul.nav li a" ).css("color","white");
    });
</script>