<!-- 
https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css
https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css
 -->
<link rel="stylesheet" href="../assets/plugins/bootstrap-3.3.7-dist/css/bootstrap.min.css">
<link rel="stylaesheet" href="../assets/plugins/datatables/dataTables.bootstrap.css">
<link rel="stylaesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
<script src="../assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(function() {
    $("#example").DataTable();
  });
</script>