<section class="container pre-footer" style="padding: 25px 0px 50px 0px;">
		<div class="col-sm-3 footer-part">
			<h3>Services</h3>
			<ul>
			<?php
		    $category = $db->query("SELECT * FROM servicescategory WHERE ServicesCategoryShow='1' ORDER BY ServicesCategoryName ASC","result");
		    if($category!=false){
		      foreach ($category as $list_category): ?>
		    	<li><a href="index.php?link=layanan&category=<?= $list_category->ServicesCategoryPermalink; ?>"><?= $list_category->ServicesCategoryName; ?></a></li>
		    <?php
		      endforeach;
		    }
		    ?>
				<li><a href="?link=article">article</a></li>
			</ul>
		</div>
		<div class="col-sm-3 footer-part">
			<h3>member</h3>
			<ul>
		<?php
		if(!isset($_SESSION['MemberID'])){ ?>
				<li><a href="?link=register">Daftar</a></li>
				<li><a href="?link=account">Masuk</a></li>
		<?php
		}else{ ?>
				<li><a href="?link=account">Akun Saya</a></li>
		        <li><a href="?link=history_order">Daftar Pemesanan</a></li>
		        <li><a href="#" onClick="if(confirm('Yakin akan keluar ?')){ window.location=('?link=logout') }">Keluar</a></li>
		<?php
		}
		?>
			</ul>
		</div>
		<div class="col-sm-3 footer-part">
			<h3>company</h3>
			<ul>
				<li><a href="?link=aboutus">tentang kami</a></li>
				<li><a href="?link=contactus">kontak kami</a></li>
				<li><a href="?link=faq">F.A.Q</a></li>
				<li><a href="?link=terms_and_condition">terms and condition</a></li>
				<li><a href="?link=privacy_policy">privacy policy</a></li>
			</ul>
		</div>
		<div class="col-sm-3 footer-part">
			<h3>follow us</h3>
			<div class="social-media" style="margin-top: 20px;">
	<?php
		$social_media = $db->query("SELECT * FROM contentpoint WHERE ContentPointLabel='Social Media Link' AND ContentPointShow='1' ORDER BY ContentPointDate ASC","result");
		if($social_media!=false){
			foreach ($social_media as $list_social_media): ?>
				<a href="<?= $list_social_media->ContentPointSubDescription; ?>"><i class="fa <?= $list_social_media->ContentPointIcon; ?>"></i></a>		
	<?php
			endforeach;
		}
	?>
			</div>
			
			<!--<h3 style="margin-top: 40px;">subscribe us</h3>
			<p class="footer-subscribe">Dapatkan notifikasi ketika kami merilis layanan baru.</p>
			<div class="input-group">
			  <input type="text" class="form-control" placeholder="Masukkan email anda ..." aria-describedby="basic-addon1">
				<span class="input-group-addon" id="basic-addon">
					<button type="button"><i class="fa fa-send"></i></button>
				</span>
			</div>-->
		</div>
	</section>
	<section class="container-fluid footer">
		<div class="container center-block">
			<label class="footer-content text-center">Copyright 2017 Vanesha Salon. All Right Reserved.</label>
		</div>
	</section>