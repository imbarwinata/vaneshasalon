<style type="text/css">
  .info{ margin-top: 15px;  }
</style>
<?php
$logo = $db->query("SELECT * FROM content WHERE ContentLabel='Logo'","row");
$status = $db->query("SELECT * FROM status","row");
if($status['StatusActive']==0){
  $keterangan = "Salon Tutup";
}elseif($status['StatusActive']==1){
  $keterangan = "Salon Buka";
}elseif($status['StatusActive']==2){
  $keterangan = "Salon Tutup, Pegawai Sibuk";
}
?>
<nav class="navbar navbar-default navbar-fixed-top" id="navbar" role="navigation bg-white" style="border-bottom: none;">
  <div style="margin: 0 auto;">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button style="border:1px solid rgba(128, 67, 123,0.3);" type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
		<i class="fa fa-align-justify" style="color:rgba(128, 67, 123,1.0);"></i>
      </button>
<?php
  if($logo!=false){
    if(file_exists("img/content/logo/".$logo['ContentImage'])){  ?>
      <a class="navbar-brand" href="index.php"><img style="height:150%;" src="img/content/logo/<?= $logo['ContentImage']; ?>" alt="<?= $logo['ContentTitle']; ?>"></a>
<?php
    }else{ ?>
      <a class="navbar-brand" href="index.php"><img style="height:150%;" src="design/img/no_image.png" alt="no image"></a>
<?php
    }
  }else{ ?>
      <a class="navbar-brand" href="index.php"><img style="height:150%;" src="design/img/no_image.png" alt="no image"></a>
<?php
  }
?>

    </div>

   	<div id="logo-primary" class="text-center" style="" onClick="window.location=('index.php')">
<?php
  if($logo!=false){
    if(file_exists("img/content/logo/".$logo['ContentImage'])){  ?>
      <img src="img/content/logo/<?= $logo['ContentImage']; ?>" alt="<?= $logo['ContentTitle']; ?>">
<?php
    }else{ ?>
      <img src="design/img/no_image.png" alt="no image">
<?php
    }
  }else{ ?>
      <img src="design/img/no_image.png" alt="no image">
<?php
  }
?>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse bg-white navbar-primary element-center" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="pull-left"><marquee class="info" behavior="" direction="">Info : <?= $keterangan; ?></marquee></li>
		<li><a class="navbar-link" href="index.php?link=aboutus"><span>Tentang Kami</span></a></li>
    <?php
    $category = $db->query("SELECT * FROM servicescategory WHERE ServicesCategoryShow='1' ORDER BY ServicesCategoryName ASC","result");
    if($category!=false){
      foreach ($category as $list_category): ?>
    <li><a class="navbar-link" href="index.php?link=layanan&category=<?= $list_category->ServicesCategoryPermalink; ?>"><span><?= $list_category->ServicesCategoryName; ?></span></a></li>
    <?php
      endforeach;
    }
    ?>
		<li><a class="navbar-link" href="index.php?link=article"><span>Artikel</span></a></li>
		<li><a class="navbar-link" href="index.php?link=contactus"><span>Kontak Kami</span></a></li>
    <li class="dropdown">
      <a class="dropdown-toggle navbar-link" data-toggle="dropdown" href="#"><span>Akun</span>
      <span class="caret"></span></a>
      <ul class="dropdown-menu submenu">
    <?php
    if(isset($_SESSION['MemberID'])){ ?>
        <li class="submenu-child"><a href="?link=cart">Keranjang</a></li>
        <li class="submenu-child"><a href="?link=checkout">Checkout</a></li>
        <li role=separator class=divider></li>
        <li class="submenu-child"><a href="?link=account">Akun Saya</a></li>
        <li class="submenu-child"><a href="?link=history_order">Daftar Pemesanan</a></li>
        <li class="submenu-child"><a href="#" onClick="if(confirm('Yakin akan keluar ?')){ window.location=('?link=logout') }">Keluar</a></li>
    <?php
    }else{ ?>
        <li><a href="?link=account">Masuk</a></li>
        <li><a href="?link=register">Daftar</a></li>
    <?php
    }
    ?>
      </ul>
    </li>
<!--     <li class="social pull-right"><a href="#" id="search" onClick="toggleSearch()"><i class="fa fa-search"></i></a></li>
      </ul>
    <form class="form-search pull-right" style="border-bottom: none;" method="get">  
        <div class="input-group col-lg-3 pull-right search-area">
    <span class="input-group-addon" id="basic-addon">
      <button type="submit"><i class="fa fa-search"></i></button>
    </span>
    <input type="text" class="form-control" placeholder="Cari disini ..." aria-describedby="basic-addon1">
  </div>
    </form> -->
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>