<?php
function login($email, $password, $mysqli){
	// Menggunakan perintah prepared statement untuk menghindari SQL injection
	if($stmt = $mysqli->prepare("SELECT MemberID, MemberPassword, MemberName, MemberEmail FROM member WHERE MemberEmail = ? AND MemberActive = ?")){
		$stmt->bind_param('si', $email,$active); // Menyimpan data inputan username ke variabel "$email"
		$active = 1;
		$stmt->execute(); // Menjalankan perintah query MySQL diatas
		$stmt->store_result();
		$stmt->bind_result($id, $dbpassword , $nama, $email); // Menyimpan nilai hasil query ke variabel
		$stmt->fetch();
		
		if($stmt->num_rows == 1){ // Jika user ada/ditemukan
			if($dbpassword == md5($password)){ // Lakukan pengecekan password sesuai atau tidak dengan data di database
				// Jika sama ciptakan SESSION id dan password_login
				$_SESSION['MemberID'] = $id;
				$_SESSION['MemberPassword'] = md5($password);
				$_SESSION['MemberName'] = $nama;
				$_SESSION['MemberEmail'] = $email;
				// Login berhasil
				return true;
			}else{
				// Password tidak sesuai
				return false;	
			}
		}else{
			// User tidak ditemukan
			return false;	
		}
	}
}

function cek_login($mysqli){
	// Cek apakah semua variabel session ada / tidak
	if(isset($_SESSION['MemberID'], $_SESSION['MemberPassword'])){
		$id = $_SESSION['MemberID'];
		$password_login = $_SESSION['MemberPassword'];
		
		if($stmt = $mysqli->prepare("SELECT MemberPassword FROM member WHERE MemberID = ? LIMIT 1")){
			$stmt->bind_param('i', $id); // Menyimpan data id user ke variabel "$id"
			$stmt->execute(); // Menjalankan perintah query MySQL diatas
			$stmt->store_result();
			
			if($stmt->num_rows == 1){ // Jika user ada/ditemukan
				$stmt->bind_result($password);
				$stmt->fetch();
				
				if($password_login == $password){ // Jika passwordnya sesuai
					// User melakukan login
					return true;	
				}else{
					// User tidak melakukan login
					return false;	
				}
			}else{
				// User tidak melakukan login
				return false;	
			}
		}else{
			// User tidak melakukan login
			return false;	
		}
	}else{
		// User tidak melakukan login
		return false;	
	}
}
?>