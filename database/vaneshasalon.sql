-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 20, 2017 at 11:56 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vaneshasalon`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `AdminID` int(11) NOT NULL,
  `AdminUserName` varchar(20) NOT NULL,
  `AdminPassword` varchar(50) NOT NULL,
  `AdminName` varchar(50) NOT NULL,
  `AdminEmail` varchar(50) NOT NULL,
  `AdminEntryName` varchar(50) NOT NULL,
  `AdminEntryDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`AdminID`, `AdminUserName`, `AdminPassword`, `AdminName`, `AdminEmail`, `AdminEntryName`, `AdminEntryDate`) VALUES
(1, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'Administrator', 'administrator@gmail.com', '-', '2017-02-15 17:16:07'),
(2, 'imbarwinata', 'adec79fd87d04960046628531c4a56b714d160d8', 'Imbar Winata', 'imbarwinata@gmail.com', 'admin', '2017-02-15 18:07:00');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `CityID` varchar(20) NOT NULL,
  `CityName` varchar(50) NOT NULL,
  `CityShow` int(1) NOT NULL,
  `CityDate` datetime NOT NULL,
  `CityFare` int(11) NOT NULL,
  `CityDefault` int(1) NOT NULL,
  `AdminID` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`CityID`, `CityName`, `CityShow`, `CityDate`, `CityFare`, `CityDefault`, `AdminID`) VALUES
('C170415001', 'Bogor', 1, '2017-04-15 20:11:08', 20000, 1, 1),
('C170415002', 'Jakarta', 1, '2017-04-15 20:11:35', 50000, 0, 1),
('C170415003', 'Depok', 1, '2017-04-15 20:11:28', 40000, 0, 1),
('C170415004', 'Tangerang', 1, '2017-04-15 20:11:43', 60000, 0, 1),
('C170415005', 'Bekasi', 1, '2017-04-15 20:11:21', 50000, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `confirmation`
--

CREATE TABLE `confirmation` (
  `ConfirmationID` varchar(20) NOT NULL,
  `OrderID` varchar(20) NOT NULL,
  `MemberID` varchar(20) NOT NULL,
  `ConfirmationImage` text NOT NULL,
  `ConfirmationStatus` int(1) NOT NULL,
  `ConfirmationDate` datetime NOT NULL,
  `ConfirmationName` varchar(50) NOT NULL,
  `ConfirmationAccount` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `confirmation`
--

INSERT INTO `confirmation` (`ConfirmationID`, `OrderID`, `MemberID`, `ConfirmationImage`, `ConfirmationStatus`, `ConfirmationDate`, `ConfirmationName`, `ConfirmationAccount`) VALUES
('OC170419001', 'O170419001', 'M17041900001', 'OC170419001.png', 0, '2017-04-19 01:52:49', 'Fuji Lestari', 0),
('OC170422001', 'O170419001', 'M17041900001', 'OC170422001.jpg', 1, '2017-04-22 13:29:46', 'Akikah', 0);

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE `content` (
  `ContentID` varchar(20) NOT NULL,
  `ContentLabel` varchar(30) NOT NULL,
  `ContentTitle` varchar(30) NOT NULL,
  `ContentSubTitle` varchar(50) NOT NULL,
  `ContentSubDescription` text NOT NULL,
  `ContentDescription` text NOT NULL,
  `ContentLink` text NOT NULL,
  `ContentImage` text NOT NULL,
  `ContentDate` date NOT NULL,
  `AdminID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`ContentID`, `ContentLabel`, `ContentTitle`, `ContentSubTitle`, `ContentSubDescription`, `ContentDescription`, `ContentLink`, `ContentImage`, `ContentDate`, `AdminID`) VALUES
('C170325003', 'Privacy and Policy', 'Privacy and Policy', '', '', '<p>Vivamus suscipit tortor eget felis porttitor volutpat. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Nulla porttitor accumsan tincidunt. Donec sollicitudin molestie malesuada. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.</p>\r\n\r\n<p>Pellentesque in ipsum id orci porta dapibus. Nulla porttitor accumsan tincidunt. Curabitur aliquet quam id dui posuere blandit. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Donec rutrum congue leo eget malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n\r\n<p>Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Cras ultricies ligula sed magna dictum porta. Proin eget tortor risus. Vivamus suscipit tortor eget felis porttitor volutpat. Donec sollicitudin molestie malesuada. Curabitur aliquet quam id dui posuere blandit.</p>', '', '', '2017-03-25', 1),
('C170325004', 'Terms and Condition', 'Terms and Condition', '', '', '<p>Vivamus suscipit tortor eget felis porttitor volutpat. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Nulla porttitor accumsan tincidunt. Donec sollicitudin molestie malesuada. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.</p>\r\n\r\n<p>Pellentesque in ipsum id orci porta dapibus. Nulla porttitor accumsan tincidunt. Curabitur aliquet quam id dui posuere blandit. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Donec rutrum congue leo eget malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n\r\n<p>Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Cras ultricies ligula sed magna dictum porta. Proin eget tortor risus. Vivamus suscipit tortor eget felis porttitor volutpat. Donec sollicitudin molestie malesuada. Curabitur aliquet quam id dui posuere blandit.</p>', '', '', '2017-03-29', 1),
('C170325005', 'Contact Us', '', '', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3963.5059686855384!2d106.78811901436514!3d-6.583849095238506!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69c44bb9631d99%3A0x268f33ded1ed2b13!2sH.+Bir+Ali%2C+Ciwaringin%2C+Bogor+Tengah%2C+Kota+Bogor%2C+Jawa+Barat+16124!5e0!3m2!1sid!2sid!4v1492842110131" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', 'Monday - Friday ( 10.00 - 17.00 )', 'Saturday - Sunday ( 10.30 - 17.30)', 'vaneshasalon@gmail.com', '2017-04-22', 1),
('C170325006', 'Logo', 'Logo Website', '', '', '', '', 'C170325006.png', '2017-03-25', 0),
('C170326007', 'Why Vanesha', 'Kenapa Memilih Vanesha ?', '', '', 'Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Sed porttitor lectus nibh. Donec rutrum congue leo eget malesuada. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Sed porttitor lectus nibh. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Cras ultricies ligula sed magna dictum porta. Sed porttitor lectus nibh.', 'index.php?link=aboutus', 'dafa.jpg', '2017-03-26', 1),
('C170422001', 'Home Banner', 'Mau tinggal takengon ?', 'Praesent sapien massa, convallis a pellentesque ne', '', '', 'index.php?link=article', 'background-banner2.jpg', '2017-04-22', 1);

-- --------------------------------------------------------

--
-- Table structure for table `contentpoint`
--

CREATE TABLE `contentpoint` (
  `ContentPointID` varchar(20) NOT NULL,
  `ContentPointLabel` varchar(30) NOT NULL,
  `ContentPointTitle` varchar(50) NOT NULL,
  `ContentPointSubDescription` text NOT NULL,
  `ContentPointDescription` longtext NOT NULL,
  `ContentPointImage` text NOT NULL,
  `ContentPointDate` datetime NOT NULL,
  `ContentPointIcon` text NOT NULL,
  `ContentPointOrder` int(3) NOT NULL,
  `ContentPointShow` int(1) NOT NULL,
  `ContentPointPermalink` varchar(60) NOT NULL,
  `ContentPointView` int(11) NOT NULL,
  `AdminID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contentpoint`
--

INSERT INTO `contentpoint` (`ContentPointID`, `ContentPointLabel`, `ContentPointTitle`, `ContentPointSubDescription`, `ContentPointDescription`, `ContentPointImage`, `ContentPointDate`, `ContentPointIcon`, `ContentPointOrder`, `ContentPointShow`, `ContentPointPermalink`, `ContentPointView`, `AdminID`) VALUES
('A17032200001', 'Article', 'Lorem Ipsum', 'Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Proin eget tortor risus. Donec sollicitudin molestie malesuada. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sollicitudin molestie malesuada. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla porttitor accumsan tincidunt. Donec sollicitudin molestie malesuada. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi.', '<p>Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Proin eget tortor risus. Donec sollicitudin molestie malesuada. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sollicitudin molestie malesuada. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla porttitor accumsan tincidunt. Donec sollicitudin molestie malesuada. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi.</p>\r\n\r\n<p>Nulla porttitor accumsan tincidunt. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Donec sollicitudin molestie malesuada. Curabitur aliquet quam id dui posuere blandit. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla porttitor accumsan tincidunt. Nulla quis lorem ut libero malesuada feugiat. Sed porttitor lectus nibh. Proin eget tortor risus.</p>', '16.jpg', '2017-03-22 00:00:00', '', 0, 1, 'Lorem-Ipsum-A17032200001', 2, 1),
('A17032200002', 'About', 'History', '', '<p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Nulla quis lorem ut libero malesuada feugiat. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Cras ultricies ligula sed magna dictum porta. Sed porttitor lectus nibh.&nbsp;<br />\r\n<br />\r\nCras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Donec rutrum congue leo eget malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vivamus suscipit tortor eget felis porttitor volutpat. Cras ultricies ligula sed magna dictum porta.</p>', 'Fotolia_63073892_Subscription_Monthly_M1.jpeg', '2017-03-23 06:20:11', '', 0, 1, 'History-A17032200002', 0, 1),
('A17032800001', 'Article', 'Tips Perawatan Rambut', 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. \r\nVestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.', '<p>Vivamus suscipit tortor eget felis porttitor volutpat. Pellentesque in ipsum id orci porta dapibus. Nulla quis lorem ut libero malesuada feugiat. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Vivamus suscipit tortor eget felis porttitor volutpat. Cras ultricies ligula sed magna dictum porta. Cras ultricies ligula sed magna dictum porta. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Nulla porttitor accumsan tincidunt. Vivamus suscipit tortor eget felis porttitor volutpat.</p>\r\n\r\n<p>Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Donec rutrum congue leo eget malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur aliquet quam id dui posuere blandit. Nulla porttitor accumsan tincidunt. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Curabitur aliquet quam id dui posuere blandit.</p>\r\n\r\n<p>Curabitur aliquet quam id dui posuere blandit. Curabitur aliquet quam id dui posuere blandit. Sed porttitor lectus nibh. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Sed porttitor lectus nibh. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Donec rutrum congue leo eget malesuada.</p>\r\n\r\n<p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Cras ultricies ligula sed magna dictum porta. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Nulla quis lorem ut libero malesuada feugiat. Sed porttitor lectus nibh. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Nulla quis lorem ut libero malesuada feugiat. Nulla quis lorem ut libero malesuada feugiat. Proin eget tortor risus. Pellentesque in ipsum id orci porta dapibus.</p>', 'Images_Why_Hawnha2016-12-14_13_01_24.jpg', '2017-03-28 09:15:41', '', 0, 1, 'Tips-Perawatan-Rambut-A17032800001', 3, 1),
('AB17032200001', 'About', 'Vision and Mission', '', '<p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Proin eget tortor risus.&nbsp;<br />\r\n<br />\r\nDonec sollicitudin molestie malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque.</p>', 'beauty-salon-albury-1.jpg', '2017-03-22 19:49:36', '', 0, 1, 'Vision-and-MissionAB17032200001', 0, 1),
('AB17032200002', 'About', 'Objectives', '', '<p>Donec sollicitudin molestie malesuada. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a.<br />\r\n<br />\r\nQuisque velit nisi, pretium ut lacinia in, elementum id enim. Curabitur aliquet quam id dui posuere blandit. Curabitur aliquet quam id dui posuere blandit. Nulla porttitor accumsan tincidunt.</p>', '2-Aman-Luxury-Spa-Mayfair.jpg', '2017-03-22 19:50:40', '', 0, 1, 'ObjectivesAB17032200002', 0, 1),
('CPT17040500001', 'Testimony', 'Imbar Winata', 'Web Developer', 'Cras ultricies ligula sed magna dictum porta. Nulla quis lorem ut libero malesuada feugiat. Proin eget tortor risus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Sed porttitor lectus nibh. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula.', 'Vector Art.jpg', '2017-04-05 07:56:32', '', 0, 1, '', 0, 1),
('CPT17040500002', 'Testimony', 'Fuji Lestari', 'Mahasiswa', 'Donec sollicitudin molestie malesuada. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Pellentesque in ipsum id orci porta dapibus. Curabitur aliquet quam id dui posuere blandit. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Nulla porttitor accumsan tincidunt. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vivamus suscipit tortor eget felis porttitor volutpat.', 'CPT17040500002.png', '2017-04-05 22:06:18', '', 0, 1, '', 0, 1),
('FAQ17032200001', 'FAQ', 'Cara order pesanan ?', '', '<p>Cras ultricies ligula sed magna dictum porta. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Donec sollicitudin molestie malesuada. Donec rutrum congue leo eget malesuada. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi.</p>\r\n\r\n<p>Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Curabitur aliquet quam id dui posuere blandit. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Pellentesque in ipsum id orci porta dapibus.</p>', '', '2017-04-22 13:25:04', '', 0, 1, 'Cara-order-pesanan-?-FAQ17032200001', 0, 1),
('FAQ17032200002', 'FAQ', 'Section 2', '', '<p>Cras ultricies ligula sed magna dictum porta. Cras ultricies ligula sed magna dictum porta. Vivamus suscipit tortor eget felis porttitor volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Sed porttitor lectus nibh. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque in ipsum id orci porta dapibus.</p>\r\n\r\n<p>Sed porttitor lectus nibh. Curabitur aliquet quam id dui posuere blandit. Nulla porttitor accumsan tincidunt. Curabitur aliquet quam id dui posuere blandit. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Donec sollicitudin molestie malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n\r\n<p>Nulla quis lorem ut libero malesuada feugiat. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla porttitor accumsan tincidunt. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Curabitur aliquet quam id dui posuere blandit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla porttitor accumsan tincidunt.</p>', '', '2017-03-25 23:11:03', '', 0, 1, 'Section-2-FAQ17032200002', 0, 1),
('FAQ17032200003', 'FAQ', 'Section 3', '', '<h2>Section 3</h2>\r\n\r\n<p>Sed porttitor lectus nibh. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Pellentesque in ipsum id orci porta dapibus. Sed porttitor lectus nibh. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.</p>\r\n\r\n<p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Vivamus suscipit tortor eget felis porttitor volutpat. Nulla porttitor accumsan tincidunt. Curabitur aliquet quam id dui posuere blandit. Donec sollicitudin molestie malesuada. Cras ultricies ligula sed magna dictum porta. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ultricies ligula sed magna dictum porta. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi.</p>\r\n\r\n<p>Nulla quis lorem ut libero malesuada feugiat. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Sed porttitor lectus nibh. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sollicitudin molestie malesuada. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Pellentesque in ipsum id orci porta dapibus. Nulla quis lorem ut libero malesuada feugiat. Cras ultricies ligula sed magna dictum porta.</p>\r\n\r\n<p><span style="font-family:Courier New,Courier,monospace">Cras ultricies ligula sed magna dictum porta. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vivamus suscipit tortor eget felis porttitor volutpat. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vivamus suscipit tortor eget felis porttitor volutpat. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.</span></p>', '', '2017-03-25 23:12:07', '', 0, 1, 'Section-3-FAQ17032200003', 0, 1),
('FAQ17032500001', 'FAQ', 'Section 4', '', '<p>asdasdasdas</p>', '', '2017-03-25 23:12:42', '', 0, 1, 'Section-4FAQ17032500001', 0, 1),
('SML17032200002', 'Social Media Link', 'Google Plus', 'https://www.google.co.id/', '', '', '2017-03-22 21:04:35', 'fa-google-plus', 0, 1, '', 7, 1),
('SML17032200003', 'Social Media Link', 'Twitter', 'https://www.twitter.com/', '', '', '2017-03-22 21:10:05', 'fa-twitter', 0, 1, '', 7, 1),
('SML17042200001', 'Social Media Link', 'Facebook', 'http://facebook.com/tari-vanesha', '', '', '2017-04-22 13:26:44', 'fa-facebook-official', 0, 1, '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `LocationID` varchar(20) NOT NULL,
  `LocationTitle` varchar(30) NOT NULL,
  `LocationKodePos` varchar(5) NOT NULL,
  `LocationFare` int(11) NOT NULL,
  `LocationShow` int(1) NOT NULL,
  `LocationDate` datetime NOT NULL,
  `AdminID` int(11) NOT NULL,
  `CityID` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`LocationID`, `LocationTitle`, `LocationKodePos`, `LocationFare`, `LocationShow`, `LocationDate`, `AdminID`, `CityID`) VALUES
('L170215001', 'Cibinong', '16911', 8000, 1, '2017-04-15 13:57:56', 1, 'C170415001'),
('L170215002', 'Tamansari', '16610', 10000, 1, '2017-04-15 13:59:01', 1, 'C170415001'),
('L170215003', 'Babakan Madang', '16810', 10000, 1, '2017-04-15 13:27:37', 1, 'C170415001'),
('L170215004', 'Cikoan', '16810', 7000, 1, '2017-04-15 13:58:07', 1, 'C170415001'),
('L170215005', 'Ciomas', '16610', 6000, 1, '2017-04-15 13:58:39', 1, 'C170415001'),
('L170215006', 'Gunung Sindur', '16340', 10000, 1, '2017-04-15 13:58:44', 1, 'C170415001'),
('L170215007', 'Leuwisadeng', '16640', 10000, 1, '2017-04-15 13:58:48', 1, 'C170415001'),
('L170215008', 'Ranca Bungur', '16310', 12000, 1, '2017-04-15 13:58:56', 1, 'C170415001'),
('L170215009', 'Pamijahan', '16810', 12000, 1, '2017-04-15 13:58:52', 1, 'C170415001'),
('L170215010', 'Bojonggede', '16920', 12000, 1, '2017-04-15 13:57:42', 1, 'C170415001'),
('L170215011', 'Cibungbulang', '16630', 5000, 1, '2017-04-15 13:58:01', 1, 'C170415001');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `MemberID` varchar(20) NOT NULL,
  `MemberName` varchar(50) NOT NULL,
  `MemberEmail` varchar(40) NOT NULL,
  `MemberPhone` varchar(13) NOT NULL,
  `MemberAddress` text NOT NULL,
  `MemberPassword` varchar(40) NOT NULL,
  `MemberActive` int(1) NOT NULL,
  `MemberLocation` varchar(50) NOT NULL,
  `LocationID` varchar(20) NOT NULL,
  `CityID` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`MemberID`, `MemberName`, `MemberEmail`, `MemberPhone`, `MemberAddress`, `MemberPassword`, `MemberActive`, `MemberLocation`, `LocationID`, `CityID`) VALUES
('M17041900001', 'Fuji Lestari', 'fujilestari223@gmail.com', '085819848207', 'Jl. Ciwaringin', '4cef2f30ac7d33419d00c1d93a090095', 1, 'Ciwaringin', '0', 'C170415001'),
('M17041900002', 'Imbar Winata', 'imbarwinata@gmail.com', '085811106473', 'Jl. Erfah 1 RT 02/11', '4cef2f30ac7d33419d00c1d93a090095', 1, 'Ciwaringin', '0', 'C170415001');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `ServicesID` varchar(20) NOT NULL,
  `ServicesName` varchar(50) NOT NULL,
  `ServicesPrice` int(11) NOT NULL,
  `ServicesDescription` text NOT NULL,
  `ServicesImage` text NOT NULL,
  `ServicesDate` date NOT NULL,
  `ServicesShow` int(1) NOT NULL,
  `ServicesFeatured` int(1) NOT NULL,
  `AdminID` int(11) NOT NULL,
  `ServicesCategoryID` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`ServicesID`, `ServicesName`, `ServicesPrice`, `ServicesDescription`, `ServicesImage`, `ServicesDate`, `ServicesShow`, `ServicesFeatured`, `AdminID`, `ServicesCategoryID`) VALUES
('S17032500001', 'Sugar Wax', 158000, 'Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Proin eget tortor risus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quis lorem ut libero malesuada feugiat. Cras ultricies ligula sed magna dictum porta.', 'S17032500001.jpg', '2017-03-26', 1, 1, 1, 'SC17032500002'),
('S17032600001', 'Soft Wax', 148000, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quis lorem ut libero malesuada feugiat. Pellentesque in ipsum id orci porta dapibus. Vivamus suscipit tortor eget felis porttitor volutpat. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur aliquet quam id dui posuere blandit. Cras ultricies ligula sed magna dictum porta. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.', 'S17032600001.jpeg', '2017-03-26', 1, 1, 1, 'SC17032500002'),
('S17032600002', 'Nails One', 210000, 'Vivamus suscipit tortor eget felis porttitor volutpat. Pellentesque in ipsum id orci porta dapibus. Proin eget tortor risus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla quis lorem ut libero malesuada feugiat. Nulla porttitor accumsan tincidunt. Cras ultricies ligula sed magna dictum porta. Nulla porttitor accumsan tincidunt. Donec rutrum congue leo eget malesuada.', 'S17032600002.jpg', '2017-04-05', 1, 1, 1, 'SC17032500004');

-- --------------------------------------------------------

--
-- Table structure for table `servicescategory`
--

CREATE TABLE `servicescategory` (
  `ServicesCategoryID` varchar(20) NOT NULL,
  `ServicesCategoryName` varchar(50) NOT NULL,
  `ServicesCategoryDescription` text NOT NULL,
  `ServicesCategoryPermalink` varchar(60) NOT NULL,
  `ServicesCategoryShow` int(1) NOT NULL,
  `ServicesCategoryDate` datetime NOT NULL,
  `AdminID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `servicescategory`
--

INSERT INTO `servicescategory` (`ServicesCategoryID`, `ServicesCategoryName`, `ServicesCategoryDescription`, `ServicesCategoryPermalink`, `ServicesCategoryShow`, `ServicesCategoryDate`, `AdminID`) VALUES
('SC17032500002', 'Waxing', '<p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Proin eget tortor risus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quis lorem ut libero malesuada feugiat. Cras ultricies ligula sed magna dictum porta.&nbsp;<br />\r\n<br />\r\nNulla quis lorem ut libero malesuada feugiat. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Cras ultricies ligula sed magna dictum porta.</p>', 'WaxingSC17032500002', 1, '2017-03-25 11:08:00', 1),
('SC17032500003', 'Facial', '<p>Pellentesque in ipsum id orci porta dapibus. Donec sollicitudin molestie malesuada. Vivamus suscipit tortor eget felis porttitor volutpat. Nulla quis lorem ut libero malesuada feugiat. Donec rutrum congue leo eget malesuada. Pellentesque in ipsum id orci porta dapibus. Nulla porttitor accumsan tincidunt. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula.</p>', 'FacialSC17032500003', 1, '2017-03-25 14:44:23', 1),
('SC17032500004', 'Hair', '<p>Pellentesque in ipsum id orci porta dapibus. Donec sollicitudin molestie malesuada. Vivamus suscipit tortor eget felis porttitor volutpat. Nulla quis lorem ut libero malesuada feugiat. Donec rutrum congue leo eget malesuada. Pellentesque in ipsum id orci porta dapibus. Nulla porttitor accumsan tincidunt. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula.</p>', 'HairSC17032500004', 1, '2017-03-25 14:44:32', 1),
('SC17032500005', 'Nails', '<p>Pellentesque in ipsum id orci porta dapibus. Donec sollicitudin molestie malesuada. Vivamus suscipit tortor eget felis porttitor volutpat. Nulla quis lorem ut libero malesuada feugiat. Donec rutrum congue leo eget malesuada. Pellentesque in ipsum id orci porta dapibus. Nulla porttitor accumsan tincidunt. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula.</p>', 'NailsSC17032500005', 1, '2017-03-25 14:44:45', 1);

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `StatusID` varchar(20) NOT NULL,
  `StatusActive` int(1) NOT NULL,
  `AdminID` int(11) NOT NULL,
  `StatusDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`StatusID`, `StatusActive`, `AdminID`, `StatusDate`) VALUES
('S170405001', 2, 1, '2017-05-06 13:36:50');

-- --------------------------------------------------------

--
-- Table structure for table `trorder`
--

CREATE TABLE `trorder` (
  `OrderID` varchar(20) NOT NULL,
  `MemberID` varchar(20) NOT NULL,
  `LocationID` varchar(20) NOT NULL,
  `CityID` varchar(20) NOT NULL,
  `OrderTotal` int(11) NOT NULL,
  `OrderCart` int(11) NOT NULL,
  `OrderName` varchar(50) NOT NULL,
  `OrderPhone` varchar(13) NOT NULL,
  `OrderAddress` text NOT NULL,
  `OrderStatus` int(1) NOT NULL,
  `OrderExpired` datetime NOT NULL,
  `OrderDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trorder`
--

INSERT INTO `trorder` (`OrderID`, `MemberID`, `LocationID`, `CityID`, `OrderTotal`, `OrderCart`, `OrderName`, `OrderPhone`, `OrderAddress`, `OrderStatus`, `OrderExpired`, `OrderDate`) VALUES
('O170419001', 'M17041900001', '0', 'C170415001', 178000, 158000, 'Fuji Lestari', '085819848207', 'Jl. Ciwaringin', 4, '2016-04-19 04:47:54', '2016-04-19 01:47:54'),
('O170419002', 'M17041900001', '0', 'C170415001', 388000, 368000, 'Fuji Lestari', '085819848207', 'Jl. Ciwaringin', 0, '2017-04-19 05:00:06', '2017-04-19 02:00:06'),
('O170430001', 'M17041900002', '0', 'C170415001', 178000, 158000, 'Imbar Winata', '085811106473', 'Jl. Erfah 1 RT 02/11', 0, '2017-04-30 13:35:54', '2017-04-30 10:35:54');

-- --------------------------------------------------------

--
-- Table structure for table `trorderdetail`
--

CREATE TABLE `trorderdetail` (
  `OrderDetailID` varchar(20) NOT NULL,
  `OrderID` varchar(20) NOT NULL,
  `ServicesID` varchar(20) NOT NULL,
  `OrderDetailQuantity` int(2) NOT NULL,
  `OrderDetailSubtotal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trorderdetail`
--

INSERT INTO `trorderdetail` (`OrderDetailID`, `OrderID`, `ServicesID`, `OrderDetailQuantity`, `OrderDetailSubtotal`) VALUES
('OD17041900001', 'O170419001', 'S17032500001', 1, 158000),
('OD17041900002', 'O170419002', 'S17032500001', 1, 158000),
('OD17041900003', 'O170419002', 'S17032600002', 1, 210000),
('OD17043000001', 'O170430001', 'S17032500001', 1, 158000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`AdminID`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`CityID`);

--
-- Indexes for table `confirmation`
--
ALTER TABLE `confirmation`
  ADD PRIMARY KEY (`ConfirmationID`);

--
-- Indexes for table `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`ContentID`);

--
-- Indexes for table `contentpoint`
--
ALTER TABLE `contentpoint`
  ADD PRIMARY KEY (`ContentPointID`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`LocationID`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`MemberID`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`ServicesID`);

--
-- Indexes for table `servicescategory`
--
ALTER TABLE `servicescategory`
  ADD PRIMARY KEY (`ServicesCategoryID`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`StatusID`);

--
-- Indexes for table `trorder`
--
ALTER TABLE `trorder`
  ADD PRIMARY KEY (`OrderID`);

--
-- Indexes for table `trorderdetail`
--
ALTER TABLE `trorderdetail`
  ADD PRIMARY KEY (`OrderDetailID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `AdminID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
