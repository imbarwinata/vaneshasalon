<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Ruang Administrator</title>
<?php include('../templates/admin2/css_view.php'); ?>
</head>
<body>
<div id="wrapper">
    <?php include('../templates/admin2/header_view.php'); ?>
    <div id="bodi">
        <?php include('../templates/admin2/sidebar_view.php'); ?>
        
        <div id="content1">
        	<h2 class="judul_artikel">List About</h2>
            <div class="content2" style="font-size:18px; padding-top: 20px;">
                <table id="example" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Title</th>
                        <th>Last Update</th>
                        <th>Show</th>
                        <th style="width:160px;">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                      $data = $db->query("SELECT * FROM contentpoint WHERE ContentPointLabel = 'About' ORDER BY ContentPointDate DESC","result");
                      if($data!=NULL){
                        foreach ($data as $data_list):
                    ?>
                        <tr>
                          <td><?= $data_list->ContentPointTitle; ?></td>
                          <td><?php echo $date->time_elapsed_string($data_list->ContentPointDate,true); ?></td>
                          <td>
                            <?php
                                if($data_list->ContentPointShow == 1){
                            ?>
                                <span class="label label-success">Show</span>
                            <?php
                                } else {
                            ?>
                                <span class="label label-danger">Hide</span>
                            <?php
                                }
                            ?>
                          </td>
                          <td>
                          <?php
                            if($data_list->ContentPointShow==0){ ?>
                            <a href="?page=<?php echo $_GET['page']; ?>_show&id=<?= $data_list->ContentPointID; ?>" class="btn btn-default btn-flat" data-toggle="tooltip" title="Show"><i class="fa fa-eye-slash"></i></a>
                          <?php
                            }else{ ?>
                            <a href="?page=<?php echo $_GET['page']; ?>_hide&id=<?= $data_list->ContentPointID; ?>" class="btn btn-primary btn-flat" data-toggle="tooltip" title="Hide"><i class="fa fa-eye"></i></a>
                          <?php
                            }
                          ?>
                            <a href="?page=<?php echo $_GET['page']; ?>_edit&id=<?= $data_list->ContentPointID; ?>" class="btn btn-warning btn-flat" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a>
                            <a onclick="if(confirm('Data will be deleted ?')){ window.location=('?page=<?php echo $_GET['page']; ?>_delete&id=<?= $data_list->ContentPointID; ?>') }" class="btn btn-danger btn-flat" data-toggle="tooltip" title="Delete"><i class="fa fa-trash"></i></a>
                          </td>
                        </tr>
                    <?php 
                       endforeach;
                     }
                    ?>
                    </tbody>
                  </table>
            </div>
            


        </div>
		</div>
		
	</div>
</div>
<?php include('../templates/admin2/script_view.php'); ?>
</body>
</html>