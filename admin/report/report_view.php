<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Ruang Administrator</title>
<?php include('../templates/admin2/css_view.php'); ?>
<style>
  .report{
    padding-left: 40px;
    list-style: none;;
  }
  .report li{ 
    padding-bottom: 10px;
  }
</style>
</head>
<body>
<div id="wrapper">
    <?php include('../templates/admin2/header_view.php'); ?>
    <div id="bodi">
        <?php include('../templates/admin2/sidebar_view.php'); ?>
        
        <div id="content1">
        	<h2 class="judul_artikel">Laporan</h2>
            <div class="content2" style="font-size:18px; padding-top: 20px;">
                
                <h3><i class="fa fa-user"></i> &nbsp; Laporan pelanggan</h3>
                <hr>
                <ul class="report">
                  <li><a target="_blank" href="?page=report_member"><i class="fa fa-calendar"></i> &nbsp;Cetak laporan pelanggan</a></li>
                </ul>
                <br>
                <h3 style="padding-left: 5px; "><i class="fa fa-shopping-cart"></i> &nbsp;Laporan pemesanan</h3>
                <hr> 
                <ul class="report">
                  <li><a target="_blank" href="?page=report_all_order"><i class="fa fa-calendar"></i> &nbsp;Cetak seluruh laporan pesanan</a></li>
                  <li><i class="fa fa-calendar"></i> &nbsp;Cetak laporan pesanan bulanan</li>
                  <li>
                    <form target="_blank" method="get" style="padding-left:10px;">
                      <div class="col-lg-3">
                        <div class="form-group">
                          <input type="hidden" value="report_month_order" name="page">
                          <select name="tahun" class="form-control">
                          <?php
                              foreach (range($year, $date->getYear()) as $value):
                          ?>
                            <option value="<?= $value; ?>"><?= $value; ?></option>
                          <?php
                              endforeach;
                          ?>
                          </select>
                        </div>
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                          <select name="bulan" class="form-control">
                          <?php
                              foreach (range(1, 12) as $month):
                          ?>
                            <option value="<?= strlen($month)==1 ? "0".$month:$month; ?>"><?= $date->convertFormat("F",$year."-".$month."-01"); ?></option>
                          <?php
                              endforeach;
                          ?>
                          </select>
                        </div>
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                          <button class="form-control" type="submit"><i class="fa fa-print"></i> &nbsp;Cetak</button>
                        </div>
                      </div>
                    </form>
                  </li>
                </ul>

            </div>
        </div>
		</div>
		
	</div>
</div>
<?php include('../templates/admin2/script_view.php'); ?>
</body>
</html>