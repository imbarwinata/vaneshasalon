<html>
<head>
<title>Laporan Data Pelanggan</title>
<?php include('../templates/admin2/css_view.php'); ?>
 <style type="text/css" media="print">
  @page { size: portrait; }
</style>
</head>
<body>
<br>
<div class="container">
<a style="margin-bottom:20px;" class="btn btn-default" onClick="javascript:window.print()"><i class="fa fa-print"></i> &nbsp; Cetak</a>
    <table class="table table-striped">
      <tr>
        <th>Nama Pelanggan</th>
        <th>Total Transaksi</th>
        <th>Total Harga Pemesanan</th>
        <th>Status</th>
      </tr>
<?php
$member = $db->query("SELECT * FROM member","result");
if($member!=false){
    foreach ($member as $list_member):
        $total_trans = $db->query("SELECT count(OrderID) AS total, sum(OrderTotal) as total_price FROM trorder WHERE MemberID='".$list_member->MemberID."' AND OrderStatus='4'","row");
?>
        <tr class="value">
            <td><?= $list_member->MemberName; ?></td>
            <td><?= $total_trans['total']; ?></td>
            <td><?= empty($total_trans['total_price']) ? "Rp. ".number_format(0,0,",","."):"Rp. ".number_format($total_trans['total_price'],0,",","."); ?></td>
            <td><?= $list_member->MemberActive == 1 ? "Aktif":"Tidak Aktif"; ?></td>
        </tr>
<?php
    endforeach;
}
?>
    </table>
</div>
</body>
</html>