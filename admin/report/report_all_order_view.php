<html>
<head>
<title>Laporan Data Pemesanan</title>
<?php include('../templates/admin2/css_view.php'); ?>
 <style type="text/css" media="print">
  @page { size: portrait; }
</style>
</head>
<body>
<br>
<div class="container">
<a style="margin-bottom:20px;" class="btn btn-default" onClick="javascript:window.print()"><i class="fa fa-print"></i> &nbsp; Cetak</a>
    <table class="table table-striped">
      <thead>
        <th colspan="4" style="text-align:center;"><h3>Laporan Pemesanan</h3></th>
      </thead>
      <tr>
        <th>Nama Pemesan</th>
        <th>Jam</th>
        <th>Tanggal Transaksi</th>
        <th>Total Transaksi</th>
      </tr>
<?php
$order = $db->query("SELECT * FROM trorder AS o INNER JOIN member AS m ON o.MemberID=m.MemberID WHERE OrderStatus='4'","result");
if($order!=false){
    foreach ($order as $list_order):
        $total_trans = $db->query("SELECT count(OrderID) AS total, sum(OrderTotal) as total_price FROM trorder WHERE MemberID='".$list_order->MemberID."' AND OrderStatus='4'","row");
?>
        <tr class="value">
            <td><?= $list_order->OrderName; ?></td>
            <td><?= $date->convertFormat("H:m:s",$list_order->OrderDate); ?></td>
            <td><?= $date->convertFormat("d F Y",$list_order->OrderDate); ?></td>
            <td><?= empty($total_trans['total_price']) ? "Rp. ".number_format(0,0,",","."):"Rp. ".number_format($total_trans['total_price'],0,",","."); ?></td>
        </tr>
<?php
    endforeach;
}
?>
    </table>
</div>
</body>
</html>