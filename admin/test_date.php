<!DOCTYPE html>
<html>
<head>
	<title></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
</head>
<body>
<?php
	include('../model/date_model.php');
	$date_model = new date_model();
	echo $date_model->getCurrentDate()."<br>";
	echo $date_model->getDate()."<br>";
	echo $date_model->getTime()."<br>";

	echo $date_model->getDay("full")."<br>";
	echo $date_model->convertFormat("Y/M/d H:i:s",$date_model->getCurrentDate())."<br>";
	echo $date_model->time_elapsed_string('2017-02-08 22:10:00')."<br>";

?>
</body>
</html>