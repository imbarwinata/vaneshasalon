<?php
//error_reporting(0);
session_start();
include_once('../config/koneksi.php');
include_once('../model/query_model.php');
include_once('../model/validation_model.php');
include_once('../model/date_model.php');
include_once('../model/generate_model.php');
include_once('../model/proccess_model.php');
include_once('fungsi.php');
$db = new query_model();
$db->path = "../"; // Setting root configurasi config database
$validation = new validation_model();
$date = new date_model();
$generate = new generate_model();
$generate->path = "../";
$proccess = new proccess_model();

/* Cek login */
if(cek_login($mysqli) == false){ // Jika user tidak login
	header('location: login.php'); // Alihkan ke halaman login (index.php)
	exit();	
}
$stmt = $mysqli->prepare("SELECT AdminUserName FROM admin WHERE AdminID = ?");
$stmt->bind_param('i', $_SESSION['AdminID']);
$stmt->execute();
$stmt->store_result();
$stmt->bind_result($username);
$stmt->fetch();
/* Cek login - End */

/* Location */
if($_GET['page']=="location"){ 
	include "location/location_view.php"; 
}
elseif($_GET['page']=="location_add"){ 
	include "location/location_form_view.php"; 
}
elseif($_GET['page']=="location_edit"){ 
	include "location/location_form_view.php"; 
}
elseif($_GET['page']=="location_delete"){ 
	if($_GET['id']!=NULL){
		$where = ['LocationID'=>$_GET['id']];
		$delete = $db->delete("location",$where,"notlike");
		if($delete==true){ 
			$proccess->generateSuccessMessage("success","location",$_GET['id'],"success","Delete");
            echo "<script>window.location=('?page=location');</script>";
		}
		else{ 
			$proccess->generateSuccessMessage("failed","location",$_GET['id'],"failed","Delete");
			echo "<script>window.location=('?page=location');</script>";
		}
	}
	else{
		echo "<script>window.location=('?page=location');</script>";
	}
}
/* Location - end */

/* City */
if($_GET['page']=="city"){ 
	include "city/city_view.php"; 
}
elseif($_GET['page']=="city_add"){ 
	include "city/city_form_view.php"; 
}
elseif($_GET['page']=="city_edit"){ 
	include "city/city_form_view.php"; 
}
/*elseif($_GET['page']=="city_enabled"){ 
	$data['CityDefault'] = 0;
    $where = ['CityID'=>'C'];
    $query_reset = $db->update("city",$data,$where,"like");
    $id = $_GET['id'];
    $data2['CityDefault'] = 1;
    $where2 = ['CityID'=>$id];
    $query_reset = $db->update("city",$data2,$where2,"notlike");
	$proccess->generateSuccessMessage("success","city",$_GET['id'],"success","Default");
    echo "<script>window.location=('?page=city');</script>";
}*/
elseif($_GET['page']=="city_delete"){ 
	if($_GET['id']!=NULL){
		$where = ['CityID'=>$_GET['id']];
		$delete = $db->delete("city",$where,"notlike");
		if($delete==true){ 
			$proccess->generateSuccessMessage("success","city",$_GET['id'],"success","Delete");
            echo "<script>window.location=('?page=city');</script>";
		}
		else{ 
			$proccess->generateSuccessMessage("failed","city",$_GET['id'],"failed","Delete");
			echo "<script>window.location=('?page=city');</script>";
		}
	}
	else{
		echo "<script>window.location=('?page=city');</script>";
	}
}
/* City - end */

/* Member */
elseif($_GET['page']=="member"){ 
	include "member/member_view.php"; 
}
elseif($_GET['page']=="member_add"){ 
	include "member/member_form_view.php"; 
}
elseif($_GET['page']=="member_edit"){ 
	include "member/member_form_view.php"; 
}
elseif($_GET['page']=="member_active"){ 
	$data['MemberActive'] = 1;
    $where = ['MemberID'=>$_GET['id']];
	$query_update = $db->update("member",$data,$where,"notlike");
	$proccess->generateSuccessMessage("success","member",$_GET['id'],"success","Aktif");
	header('Location: ?page=member');
}
elseif($_GET['page']=="member_nonactive"){
	$data['MemberActive'] = 0;
    $where = ['MemberID'=>$_GET['id']];
	$query_update = $db->update("member",$data,$where,"notlike");
	$proccess->generateSuccessMessage("success","member",$_GET['id'],"success","Nonaktif");
	header('Location: ?page=member');
}
/* Member - End */

/* Order */
elseif($_GET['page']=="order"){ 
	include "order/order_view.php"; 
}
elseif($_GET['page']=="order_detail"){
	if(!isset($_GET['id']) OR $_GET['id'] == ""){
		header('HTTP/1.0 404 Not Found', true, 404);
		exit();
	} 
	$cek_order = $db->query("SELECT * FROM trorder AS o LEFT JOIN location AS l ON o.LocationID=l.LocationID INNER JOIN member AS m ON o.MemberID=m.MemberID WHERE o.OrderID='".$_GET['id']."'","row");
	if($cek_order!=true){
		header('HTTP/1.0 404 Not Found', true, 404);
		exit();
	}
	include "order/order_detail_view.php";
}
elseif($_GET['page']=="order_edit"){
	if(!isset($_GET['id']) OR $_GET['id'] == ""){
		header('HTTP/1.0 404 Not Found', true, 404);
		exit();
	}
	$cek_order = $db->query("SELECT * FROM trorder AS o LEFT JOIN location AS l ON o.LocationID=l.LocationID INNER JOIN member AS m ON o.MemberID=m.MemberID WHERE o.OrderID='".$_GET['id']."'","row");
	if($cek_order!=true){
		header('HTTP/1.0 404 Not Found', true, 404);
		exit();
	}
	include "order/order_form_view.php";
}
elseif($_GET['page']=="confirmation"){ 
	include "order/confirmation_view.php"; 
}
elseif($_GET['page']=="confirmation_detail"){
	if(!isset($_GET['id']) OR $_GET['id'] == ""){
		header('HTTP/1.0 404 Not Found', true, 404);
		exit();
	} 
	$confirmation = $db->query("SELECT * FROM confirmation AS c INNER JOIN trorder AS o ON c.OrderID=o.OrderID INNER JOIN member AS m ON c.MemberID=m.MemberID WHERE c.ConfirmationID='".$_GET['id']."'","row");
	if($confirmation!=true){
		header('HTTP/1.0 404 Not Found', true, 404);
		exit();
	}
	include "order/confirmation_detail_view.php";
}
elseif($_GET['page']=="report"){ 
	$part=array();
	$year = 0000;
	$month = 00;
	$date_query = $db->query("SELECT min(OrderDate) AS min_date FROM trorder","row");
	if($date_query!=false){
		$part = explode(" ", $date_query['min_date']);
		$year = substr($part[0], 0,4);
		$month = substr($part[0], 5,2);
	}
	include "report/report_view.php"; 
}
elseif($_GET['page']=="report_member"){ 
	include "report/report_member_view.php"; 
}
elseif($_GET['page']=="report_all_order"){ 
	include "report/report_all_order_view.php"; 
}
elseif($_GET['page']=="report_month_order"){ 
	include "report/report_month_order_view.php"; 
}
/* Order-end */
else{ header('HTTP/1.0 404 Not Found', true, 404); }
?>