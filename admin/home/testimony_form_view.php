<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Ruang Administrator</title>
<?php include('../templates/admin2/css_view.php'); ?>
<style type="text/css">
  .form-group em{ font-size: 16px; font-style: italic; color: #d26a5c;}
</style>
</head>
<body>
<div id="wrapper">
    <?php include('../templates/admin2/header_view.php'); ?>
    <div id="bodi">
        <?php include('../templates/admin2/sidebar_view.php'); ?>
        
        <div id="content1">
          <h2 class="judul_artikel"> 
        <?php
          if(preg_match("/add/", $_GET['page'])){
              $description = "Tambah";
          }elseif(preg_match("/edit/", $_GET['page'])){
              $description = "Perbaharui";
          }
        ?>
          <?= $description; ?> Testimony</h2>
            <div class="content2" style="font-size:18px; padding-top: 20px;">
        <?php
          /*  INSERT CONTENT  */
          if(preg_match("/add/", $_GET['page'])){
            $page_flag = "Insert";
            if($_POST){
              if($_POST['title']==NULL OR $_POST['show']==NULL OR $_FILES['image']['name']==""){
                if($_POST['title']==NULL){ $validation_title = "Nama tidak boleh kosong."; }
                if($_POST['jabatan']==NULL){ $validation_jabatan = "Jabatan tidak boleh kosong."; }
                if($_POST['subdescription']==NULL){ $validation_subdescription = "Konten testimony tidak boleh kosong."; }
                if($_POST['show']==NULL){ $validation_show = "Pilih apakah konten akan ditampilkan atau tidak."; }
                if($_POST['image']==NULL){ $validation_image = "Photo tidak boleh kosong."; }
              }
              else{
                /* INSERT PROSES */
                $id = $generate->generate_custom_id("CPT","ymd","contentpoint","ContentPointID",5);
                $data['ContentPointID'] = $id;
                $data['ContentPointLabel'] = 'Testimony';
                $data['ContentPointTitle'] = $validation->default_rules($_POST['title']);
                $data['ContentPointSubDescription'] = $validation->default_rules($_POST['jabatan']);
                $data['ContentPointDescription'] = $validation->default_rules($_POST['subdescription']);
                $data['ContentPointShow'] = $validation->default_rules($_POST['show']);
                $data['ContentPointDate'] = $date->getCurrentDate();
                if($_FILES['image']['name'] != NULL){
                  $temp = explode(".", $_FILES["image"]["name"]);
                  $newfilename = $id . '.' . end($temp);
                  $data['ContentPointImage'] = $newfilename;
                  copy($_FILES ["image"]["tmp_name"], "../img/content/testimony/".$data['ContentPointImage']);
                }
                $data['AdminID'] = $_SESSION['AdminID'];
                $query_insert = $db->insert("contentpoint",$data);
                if($query_insert==false){
                  $proccess->generateSuccessMessage("danger","testimony",$id,"failed","Insert");
                  echo "<script>window.location=('?page=testimony');</script>";
                }else{
                  $proccess->generateSuccessMessage("success","testimony",$id,"success","Insert");
                  echo "<script>window.location=('?page=testimony');</script>";
                }
              }
            }
        ?>
        
              <form enctype="multipart/form-data" role="form" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
                <div class="box-body">
                  <div class="form-group">
                    <label>Nama</label>
                    <input name="title" type="text" value="<?php if($_POST['title']!=NULL){ echo $_POST['title']; } ?>" class="form-control" placeholder="Nama ..">
                <?php
                  if(isset($validation_title)){ ?>
                    <em><?php echo $validation_title; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                    <label>Jabatan / Status</label>
                    <input name="jabatan" type="text" value="<?php if($_POST['jabatan']!=NULL){ echo $_POST['jabatan']; } ?>" class="form-control" placeholder="Jabatan ...">
                <?php
                  if(isset($validation_jabatan)){ ?>
                    <em><?php echo $validation_jabatan; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                      <label>Konten</label>
                      <textarea class="form-control" rows="5" name="subdescription"><?php if($_POST['subdescription']!=NULL){ echo $_POST['subdescription']; } ?></textarea>
                <?php
                  if(isset($validation_subdescription)){ ?>
                    <em><?php echo $validation_subdescription; ?></em>
                <?php
                  }
                ?>
                  </div>
                  
                 <div class="form-group">
                      <label id="label_image">Photo</label>
                      <div class="fileupload fileupload-new" data-provides="fileupload">
                          <div class="fileupload-new thumbnail thumbnail-o-upload">
                          <?php
                              if($page_flag == "Update"){
                                  if($data['ContentPointImage'] == ''){
                          ?>
                              <img src="<?php echo "http://" . $_SERVER['SERVER_NAME']; ?>/vaneshasalon/img/default/noimage.png"/>
                          <?php
                                  } else {
                          ?>
                                  <img src="<?= "../img/content/testimony/".$data['ContentPointImage'] ?>"/>
                          <?php
                                  }
                              } else {
                          ?>
                              <img src="<?php echo "http://" . $_SERVER['SERVER_NAME']; ?>/vaneshasalon/img/default/noimage.png"/>
                          <?php
                              }
                          ?>
                          </div>
                          <div class="fileupload-preview fileupload-exists thumbnail thumbnail-o-upload"></div>
                          <div>
                              <span class="btn btn-default btn-file">
                              <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Pilih gambar</span>
                              <span class="fileupload-exists"><i class="fa fa-undo"></i> Ubah</span>
                              <input type="file" class="default" name="image" onchange="checkImage('image')" accept="image/*"/>
                              </span>
                          </div>
                          <p class="help-block" id="error_image_photo"></p>
                        <em><?php echo $validation_image; ?></em>
                      </div>
                  </div>
                  
                  <div class="form-group">
                    <label>Show / Hide</label><br>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="1" <?php if($_POST['show']!=NULL){ if($_POST['show']=="1"){ echo "checked"; } } ?>/> Show
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="0" <?php if($_POST['show']!=NULL){ if($_POST['show']=="0"){ echo "checked"; } } ?>/> Hide
                    </label>
                <?php
                  if(isset($validation_show)){ ?>
                    <br>
                    <em><?php echo $validation_show; ?></em>
                <?php
                  }
                ?>
                  </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary pull-right">Save</button>
                  <button type="button" onclick="window.location=('?page=testimony');" class="btn btn-warning pull-right" style="margin-right:5px;">Cancel</button>
                </div>
              </form>

          <?php
          }

          /*  UPDATE CONTENT  */
          elseif(preg_match("/edit/", $_GET['page'])){
            $page_flag = "Update";
            $data = $db->query("SELECT * FROM contentpoint WHERE ContentPointID='".$_GET['id']."'","row");
            if($_POST){
              if($_POST['title']==NULL OR $_POST['jabatan']==NULL OR $_POST['subdescription']==NULL OR $_POST['show']==NULL){
                if($_POST['title']==NULL){ $validation_title = "Nama tidak boleh kosong."; }
                if($_POST['jabatan']==NULL){ $validation_jabatan = "Jabatan tidak boleh kosong."; }
                if($_POST['subdescription']==NULL){ $validation_subdescription = "Konten testimony tidak boleh kosong."; }
                if($_POST['show']==NULL){ $validation_show = "Pilih apakah konten akan ditampilkan atau tidak."; }
                if($_POST['image']==NULL){ $validation_image = "Photo tidak boleh kosong."; }
                print_r($_POST);
                exit();
              }
              else{
                /* UPDATE PROSES */
                $id = $_GET['id'];
                $cek_data = $db->query("SELECT * FROM contentpoint WHERE ContentPointID = '$id'","row");
                $data['ContentPointTitle'] = $validation->default_rules($_POST['title']);
                $data['ContentPointSubDescription'] = $validation->default_rules($_POST['jabatan']);
                $data['ContentPointDescription'] = $validation->default_rules($_POST['subdescription']);
                $data['ContentPointShow'] = $validation->default_rules($_POST['show']);
                $data['ContentPointDate'] = $date->getCurrentDate();
                $data['AdminID'] = $_SESSION['AdminID'];
                $where = ['ContentPointID'=>$_GET['id']];
                if($_FILES['image']['name'] != NULL){
                  unlink("../img/content/testimony/".$cek_data['ContentPointImage']);
                  $data['ContentPointImage'] = $_FILES['image']['name'];
                }
                copy($_FILES ["image"]["tmp_name"], "../img/content/testimony/".$_FILES['image']['name']);
                $query_update = $db->update("contentpoint",$data,$where,"notlike");
                if($query_update==false){
                  $proccess->generateSuccessMessage("danger","testimony",$_GET['id'],"failed","Update");
                  echo "<script>window.location=('?page=testimony');</script>";
                }else{
                  $proccess->generateSuccessMessage("success","testimony",$_GET['id'],"success","Update");
                  echo "<script>window.location=('?page=testimony');</script>";
                }
              }
            }
        ?>
              <form enctype="multipart/form-data" role="form" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
                <div class="box-body">
                  <div class="box-body">
                  <div class="form-group">
                    <label>Nama</label>
                    <input name="title" type="text" value="<?php if($_POST){ if($_POST['title']!=NULL){ echo $_POST['title']; } else{echo "";} }else{ echo $data['ContentPointTitle']; } ?>" class="form-control" placeholder="Nama ..">
                <?php
                  if(isset($validation_title)){ ?>
                    <em><?php echo $validation_title; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                    <label>Jabatan / Status</label>
                    <input name="jabatan" type="text" value="<?php if($_POST){ if($_POST['jabatan']!=NULL){ echo $_POST['jabatan']; } else{echo "";} }else{ echo $data['ContentPointSubDescription']; } ?>" class="form-control">
                <?php
                  if(isset($validation_jabatan)){ ?>
                    <em><?php echo $validation_jabatan; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                      <label>Konten</label>
                      <textarea class="form-control" rows="5" name="subdescription"><?php if($_POST){ if($_POST['subdescription']!=NULL){ echo $_POST['subdescription']; } else{echo "";} }else{ echo $data['ContentPointDescription']; } ?></textarea>
                <?php
                  if(isset($validation_subdescription)){ ?>
                    <em><?php echo $validation_subdescription; ?></em>
                <?php
                  }
                ?>
                  </div>
                  
                 <div class="form-group">
                      <label id="label_image">Photo</label>
                      <div class="fileupload fileupload-new" data-provides="fileupload">
                          <div class="fileupload-new thumbnail thumbnail-o-upload">
                          <?php
                              if($page_flag == "Update"){
                                  if($data['ContentPointImage'] == ''){
                          ?>
                              <img src="<?php echo "http://" . $_SERVER['SERVER_NAME']; ?>/vaneshasalon/img/default/noimage.png"/>
                          <?php
                                  } else {
                          ?>
                                  <img src="<?= "../img/content/testimony/".$data['ContentPointImage'] ?>"/>
                          <?php
                                  }
                              } else {
                          ?>
                              <img src="<?php echo "http://" . $_SERVER['SERVER_NAME']; ?>/vaneshasalon/img/default/noimage.png"/>
                          <?php
                              }
                          ?>
                          </div>
                          <div class="fileupload-preview fileupload-exists thumbnail thumbnail-o-upload"></div>
                          <div>
                              <span class="btn btn-default btn-file">
                              <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Pilih gambar</span>
                              <span class="fileupload-exists"><i class="fa fa-undo"></i> Ubah</span>
                              <input type="file" class="default" name="image" onchange="checkImage('image')" accept="image/*"/>
                              </span>
                          </div>
                          <p class="help-block" id="error_image_photo"></p>
                      </div>
                  </div>

                  <div class="form-group">
                    <label>Show / Hide</label><br>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="1" <?php if($_POST['show']!=NULL){ if($_POST['show']=="0"){ echo "checked"; } }else{if($data['ContentPointShow']=="1"){ echo "checked"; }}?>/> Show
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="0" <?php if($_POST['show']!=NULL){ if($_POST['show']=="0"){ echo "checked"; } }else{ if($data['ContentPointShow']=="0"){ echo "checked"; } } ?>/> Hide
                    </label>
                <?php
                  if(isset($validation_show)){ ?>
                    <br>
                    <em><?php echo $validation_show; ?></em>
                <?php
                  }
                ?>
                  </div>

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary pull-right">Save</button>
                  <button type="button" onclick="window.location=('?page=testimony');" class="btn btn-warning pull-right" style="margin-right:5px;">Cancel</button>
                </div>
              </form>
            <?php } ?>

            </div>
        </div>
    </div>
    
  </div>
</div>
<?php include('../templates/admin2/script_view.php'); ?>
</body>
</html>