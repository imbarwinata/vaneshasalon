<?php
$list_status = [0 => 'Tutup',1 => 'Buka'];
if($_POST){
  if($_POST['status']==NULL){
    if($_POST['status']==NULL){ $validation_link = "Status toko tidak boleh kosong."; }
  }else{
    /* Update PROSES */
      $cek_data = $db->query("SELECT * FROM status","row");
      $data['StatusActive'] = $validation->default_rules($_POST['status']);
      $data['StatusDate'] = $date->getCurrentDate();
      $data['AdminID'] = $_SESSION['AdminID'];
      $where = ['StatusID'=> $cek_data['StatusID']];
      $query_update = $db->update("status",$data,$where,"notlike");
      if($query_update==false){
        $_SESSION['success_type'] = "danger"; /* succes,info,warning,danger */
        $_SESSION['success_message'] = "Update status toko failed.";
        $_SESSION['success_parameter'] = "Update failed.";
        header('Location: ?page=status');
        exit();
      }else{
        $_SESSION['success_type'] = "success"; /* succes,info,warning,danger */
        $_SESSION['success_message'] = "Update status toko success.";
        $_SESSION['success_parameter'] = "Update success.";
        header('Location: ?page=status');
        exit();
      }
  }
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Ruang Administrator</title>
<?php include('../templates/admin2/css_view.php'); ?>
<style type="text/css">
  .form-group em{ font-size: 16px; font-style: italic; color: #d26a5c;}
</style>
</head>
<body>
<div id="wrapper">
    <?php include('../templates/admin2/header_view.php'); ?>
    <div id="bodi">
        <?php include('../templates/admin2/sidebar_view.php'); ?>
        
        <div id="content1">
        	<h2 class="judul_artikel">Status Salon</h2>
            <div class="content2" style="font-size:18px; padding-top: 20px;">
                <?php
                  $data = $db->query("SELECT * FROM status");
                  if($data==false){
                      $id = $generate->generate_custom_id("S","ymd","status","StatusID",3);
                      $data['StatusID'] = $id;
                      $data['StatusDate'] = $date->getCurrentDate();
                      $data['StatusActive'] = 1;
                      $data['AdminID'] = $_SESSION['AdminID'];
                      $query_insert = $db->insert("status",$data);
                  }
                  else{
                    $data = $db->query("SELECT * FROM status","row");
                    $page_flag = "Update";
                  }
                ?>
                <form role="form" method="post" enctype="multipart/form-data" action="<?php $_SERVER["PHP_SELF"] ?>" style="width:94%; margin:0 auto;">
                  <div class="box-body">
                    <div class="form-group">
                      <label>Status : </label>
                      <select name="status" class="form-control">
                    <?php
                      foreach ($list_status as $key => $value): ?>
                        <option value="<?= $key; ?>" <?php if($data['StatusActive']==$key){ echo "selected"; } ?>><?= $value; ?></option>
                    <?php
                      endforeach;
                    ?>
                      </select>
                  <?php
                    if(isset($validation_title)){ ?>
                      <em><?php echo $validation_title; ?></em>
                  <?php
                    }
                  ?>
                    </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                  </div>
                </form>
            </div>
        </div>
		</div>
		
	</div>
</div>
<?php include('../templates/admin2/script_view.php'); ?>
</body>
</html>