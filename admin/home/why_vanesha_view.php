<?php
if($_POST){
  if($_POST['title']==NULL OR $_POST['subtitle']==NULL){
    if($_POST['title']==NULL){ $validation_title = "Judul tidak boleh kosong."; }
    if($_POST['subtitle']==NULL){ $validation_subtitle = "Sub Judul tidak boleh kosong."; }
    if($_POST['link']==NULL){ $validation_link = "Linktidak boleh kosong."; }
  }else{
    /* Update PROSES */
      $cek_data = $db->query("SELECT * FROM content WHERE ContentLabel = 'Why Vanesha'","row");
      $id = $generate->generate_custom_id("C","ymd","content","ContentID",3);
      $data['ContentID'] = $id;
      $data['ContentTitle'] = $validation->default_rules($_POST['title']);
      $data['ContentDescription'] = $validation->default_rules($_POST['subtitle']);
      $data['ContentLink'] = $validation->default_rules($_POST['link']);
      $data['ContentDate'] = $date->getCurrentDate();
      $data['AdminID'] = $_SESSION['AdminID'];
      if($_FILES['banner']['name'] != NULL){
        $data['ContentImage']=$_FILES['banner']['name'];        
        unlink("../img/content/why_vanesha/".$cek_data['ContentImage']);
      }
      $where = ['ContentLabel'=> 'Why Vanesha'];
      $query_update = $db->update("content",$data,$where,"notlike");
      copy ($_FILES ["banner"]["tmp_name"], "../img/content/why_vanesha/".$data['ContentImage']);
      if($query_update==false){
        $_SESSION['success_type'] = "danger"; /* succes,info,warning,danger */
        $_SESSION['success_message'] = "Update Why Vanesha failed.";
        $_SESSION['success_parameter'] = "Update failed.";
        header('Location: ?page=why_vanesha');
      }else{
        $_SESSION['success_type'] = "success"; /* succes,info,warning,danger */
        $_SESSION['success_message'] = "Update Why Vanesha success.";
        $_SESSION['success_parameter'] = "Update success.";
        header('Location: home.php?page=status');
      }
  }
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Ruang Administrator</title>
<?php include('../templates/admin2/css_view.php'); ?>
<style type="text/css">
  .form-group em{ font-size: 16px; font-style: italic; color: #d26a5c;}
</style>
</head>
<body>
<div id="wrapper">
    <?php include('../templates/admin2/header_view.php'); ?>
    <div id="bodi">
        <?php include('../templates/admin2/sidebar_view.php'); ?>
        
        <div id="content1">
        	<h2 class="judul_artikel">Perbaharui kenapa vanesha</h2>
            <div class="content2" style="font-size:18px; padding-top: 20px;">
                <?php
                  $data = $db->query("SELECT * FROM content WHERE ContentLabel='Why Vanesha'");
                  if($data==false){
                      $id = $generate->generate_custom_id("C","ymd","content","ContentID",3);
                      $data['ContentID'] = $id;
                      $data['ContentLabel'] = "Why Vanesha";
                      $data['ContentDate'] = $date->getCurrentDate();
                      $query_insert = $db->insert("content",$data);
                  }
                  else{
                    $data = $db->query("SELECT * FROM content WHERE ContentLabel = 'Why Vanesha'","row");
                    $page_flag = "Update";
                  }
                ?>
                <form role="form" method="post" enctype="multipart/form-data" action="<?php $_SERVER["PHP_SELF"] ?>" style="width:94%; margin:0 auto;">
                  <div class="box-body">
                    <div class="form-group">
                      <label>Judul</label>
                      <input name="title" type="text" value="<?php if($_POST['title']!=NULL){ echo $_POST['title']; }else{ echo $data['ContentTitle']; } ?>" class="form-control" placeholder="Judul Banner ..">
                  <?php
                    if(isset($validation_title)){ ?>
                      <em><?php echo $validation_title; ?></em>
                  <?php
                    }
                  ?>
                    </div>

                    <div class="form-group">
                      <label>Sub Judul</label>
                      <textarea class="form-control" rows="5" name="subtitle"><?php if($_POST['subtitle']!=NULL){ echo $_POST['subtitle']; }else{ echo $data['ContentDescription']; } ?></textarea>
                  <?php
                    if(isset($validation_subtitle)){ ?>
                      <em><?php echo $validation_subtitle; ?></em>
                  <?php
                    }
                  ?>
                    </div>

                    <div class="form-group">
                      <label>Link</label>
                      <input name="link" type="text" value="<?php if($_POST['link']!=NULL){ echo $_POST['link']; }else{ echo $data['ContentLink']; } ?>" class="form-control" placeholder="Link Banner ..">
                  <?php
                    if(isset($validation_link)){ ?>
                      <em><?php echo $validation_link; ?></em>
                  <?php
                    }
                  ?>
                    </div>

                    <div class="form-group">
                        <label id="label_image">Image Banner</label>
                        <div class="fileupload fileupload-new" data-provides="fileupload">
                            <div class="fileupload-new thumbnail thumbnail-o-upload">
                            <?php
                                if($page_flag == "Update"){
                                    if($data['ContentImage'] == ''){
                            ?>
                                <img src="<?php echo "http://" . $_SERVER['SERVER_NAME']; ?>/devsalon/img/default/noimage.png"/>
                            <?php
                                    } else {
                            ?>
                                    <img src="<?= "../img/content/why_vanesha/".$data['ContentImage'] ?>"/>
                            <?php
                                    }
                                } else {
                            ?>
                                <img src="<?php echo "http://" . $_SERVER['SERVER_NAME']; ?>/devsalon/img/default/noimage.png"/>
                            <?php
                                }
                            ?>
                            </div>
                            <div class="fileupload-preview fileupload-exists thumbnail thumbnail-o-upload"></div>
                            <div>
                                <span class="btn btn-default btn-file">
                                <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Pilih gambar</span>
                                <span class="fileupload-exists"><i class="fa fa-undo"></i> Ubah</span>
                                <input type="file" class="default" name="banner" onchange="checkImage('banner')" accept="image/*"/>
                                </span>
                            </div>
                            <p class="help-block" id="error_image_photo"></p>
                        </div>
                    </div>
                    <?php
                    if(isset($validation_banner)){ ?>
                      <em><?php echo $validation_banner; ?></em>
                  <?php
                    }
                  ?>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary pull-right">Save</button>
                  </div>
                </form>
            </div>
        </div>
		</div>
		
	</div>
</div>
<?php include('../templates/admin2/script_view.php'); ?>
</body>
</html>