<?php
error_reporting(0);
session_start();
include_once('../config/koneksi.php');
include_once('../model/query_model.php');
include_once('../model/validation_model.php');
include_once('../model/date_model.php');
include_once('../model/generate_model.php');
include_once('../model/proccess_model.php');
include_once('fungsi.php');
$db = new query_model();
$db->path = "../"; // Setting root configurasi config database
$validation = new validation_model();
$date = new date_model();
$generate = new generate_model();
$generate->path = "../";
$proccess = new proccess_model();

/* Cek login */
if(cek_login($mysqli) == false){ // Jika user tidak login
	header('location: login.php'); // Alihkan ke halaman login (index.php)
	exit();	
}
$stmt = $mysqli->prepare("SELECT AdminUserName FROM admin WHERE AdminID = ?");
$stmt->bind_param('i', $_SESSION['AdminID']);
$stmt->execute();
$stmt->store_result();
$stmt->bind_result($username);
$stmt->fetch();
/* Cek login - End */

/* Home */
if($_GET['page']=="home_banner"){
	include "home/home_banner_view.php";
}elseif($_GET['page']=="why_vanesha"){
	include "home/why_vanesha_view.php";
}elseif($_GET['page']=="status"){
	include "home/status_view.php";
}
elseif($_GET['page']=="testimony"){
	include "home/testimony_view.php";
}elseif($_GET['page']=="testimony_add"){
	include "home/testimony_form_view.php";
}elseif($_GET['page']=="testimony_edit"){
	include "home/testimony_form_view.php";
}elseif($_GET['page']=="testimony_show"){ 
	$data['ContentPointShow'] = 1;
    $where = ['ContentPointID'=>$_GET['id']];
	$query_update = $db->update("contentpoint",$data,$where,"notlike");
	$proccess->generateSuccessMessage("success","testimony",$_GET['id'],"success","Show");
	echo "<script>window.location=('?page=testimony');</script>";
}
elseif($_GET['page']=="testimony_hide"){
	$data['ContentPointShow'] = 0;
    $where = ['ContentPointID'=>$_GET['id']];
	$query_update = $db->update("contentpoint",$data,$where,"notlike");
	$proccess->generateSuccessMessage("success","testimony",$_GET['id'],"success","Hide");
	echo "<script>window.location=('?page=testimony');</script>";
}
/* Home - End */
elseif($_GET['page']=="logout"){ include "logout.php"; }
else{ header('HTTP/1.0 404 Not Found', true, 404); }
?>