<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Ruang Administrator</title>
<?php include('../templates/admin2/css_view.php'); ?>
<style type="text/css">
  .form-group em{ font-size: 16px; font-style: italic; color: #d26a5c;}
</style>
</head>
<body>
<div id="wrapper">
    <?php include('../templates/admin2/header_view.php'); ?>
    <div id="bodi">
        <?php include('../templates/admin2/sidebar_view.php'); ?>
        
        <div id="content1">
          <h2 class="judul_artikel"> 
        <?php
          if(preg_match("/add/", $_GET['page'])){
              $description = "Tambah";
          }elseif(preg_match("/edit/", $_GET['page'])){
              $description = "Perbaharui";
          }
        ?>
          <?= $description; ?> Kota</h2>
            <div class="content2" style="font-size:18px; padding-top: 20px;">
        <?php
          /*  INSERT CONTENT  */
          if(preg_match("/add/", $_GET['page'])){
            if($_POST){
              if($_POST['title']==NULL OR $_POST['show']==NULL OR $_POST['fare']==0){
                if($_POST['title']==NULL){ $validation_title = "Nama kota tidak boleh kosong."; }
                if($_POST['fare']==NULL){ $validation_fare = "Ongkos kota tidak boleh kosong."; }
                if($_POST['show']==NULL){ $validation_show = "Pilih apakah konten akan ditampilkan atau tidak."; }
              }
              else{
                /* INSERT PROSES */
                $id = $generate->generate_custom_id("C","ymd","city","CityID",3);
                $data['CityID'] = $id;
                $data['CityName'] = $validation->default_rules($_POST['title']);
                $data['CityShow'] = $validation->default_rules($_POST['show']);                
                $data['CityDate'] = $date->getCurrentDate();
                $data['CityFare'] = $validation->default_rules($_POST['fare']);
                $data['AdminID'] = $_SESSION['AdminID'];
                $query_insert = $db->insert("city",$data);
                
                if($query_insert==false){
                  $_SESSION['success_type'] = "danger"; /* succes,info,warning,danger */
                  $_SESSION['success_message'] = "Insert city $id failed.";
                  $_SESSION['success_parameter'] = "Insert failed.";
                  echo "<script>window.location=('?page=city');</script>";
                }else{
                  $_SESSION['success_type'] = "success"; /* succes,info,warning,danger */
                  $_SESSION['success_message'] = "Insert city $id success.";
                  $_SESSION['success_parameter'] = "Insert success.";
                  echo "<script>window.location=('?page=city');</script>";
                }
              }
            }
        ?>
              <form role="form" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
                <div class="box-body">
                  <div class="form-group">
                    <label>Nama Kota</label>
                    <input name="title" type="text" value="<?php if($_POST['title']!=NULL){ echo $_POST['title']; } ?>" class="form-control" placeholder="Nama Kota ..">
                <?php
                  if(isset($validation_title)){ ?>
                    <em><?php echo $validation_title; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                    <label>Ongkos</label>
                    <input name="fare" type="number" min="0" value="<?php if($_POST['fare']!=NULL){ echo $_POST['fare']; } ?>" class="form-control" placeholder="Ongkos ..">
                <?php
                  if(isset($validation_fare)){ ?>
                    <em><?php echo $validation_fare; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                    <label>Show / Hide</label><br>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="1" <?php if($_POST['show']!=NULL){ if($_POST['show']=="1"){ echo "checked"; } } ?>/> Show
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="0" <?php if($_POST['show']!=NULL){ if($_POST['show']=="0"){ echo "checked"; } } ?>/> Hide
                    </label>
                <?php
                  if(isset($validation_show)){ ?>
                    <br>
                    <em><?php echo $validation_show; ?></em>
                <?php
                  }
                ?>
                  </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                  <button type="button" onclick="window.location=('?page=city');" class="btn btn-warning pull-right" style="margin-right:5px;">Batal</button>
                </div>
              </form>

          <?php
          }

          /*  UPDATE CONTENT  */
          elseif(preg_match("/edit/", $_GET['page'])){
            $data = $db->query("SELECT * FROM city WHERE CityID='".$_GET['id']."'","row");
            if($_POST){
              if($_POST['title']==NULL OR $_POST['show']==NULL OR $_POST['fare']==0){
                if($_POST['title']==NULL){ $validation_title = "Nama kota tidak boleh kosong."; }
                if($_POST['fare']==0){ $validation_fare = "Ongkos tidak boleh kosong."; }
                if($_POST['show']==NULL){ $validation_show = "Pilih apakah konten akan ditampilkan atau tidak."; }
              }
              else{
                /* UPDATE PROSES */
                $data['CityName'] = $validation->default_rules($_POST['title']);
                $data['CityShow'] = $validation->default_rules($_POST['show']);
                $data['AdminID'] = $_SESSION['AdminID'];
                $data['CityDate'] = $date->getCurrentDate();
                $data['CityFare'] = $validation->default_rules($_POST['fare']);
                $where = ['CityID'=>$_GET['id']];
                $query_update = $db->update("city",$data,$where,"notlike");
                if($query_update==false){
                  $_SESSION['success_type'] = "danger"; /* succes,info,warning,danger */
                  $_SESSION['success_message'] = "Update city ".$_GET['id']." failed.";
                  $_SESSION['success_parameter'] = "Update failed.";
                  echo "<script>window.location=('?page=city');</script>";
                }else{
                  $_SESSION['success_type'] = "success"; /* succes,info,warning,danger */
                  $_SESSION['success_message'] = "Update city ".$_GET['id']." success.";
                  $_SESSION['success_parameter'] = "Update success.";
                  echo "<script>window.location=('?page=city');</script>";
                }
              }
            }
        ?>

              <form role="form" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
                <div class="box-body">
                  <div class="form-group">
                    <label>Lokasi</label>
                    <input name="title" type="text" value="<?php if($_POST){ if($_POST['title']!=NULL){ echo $_POST['title']; } else{echo "";} }else{ echo $data['CityName']; } ?>" class="form-control" placeholder="Nama Kota ..">
                <?php
                  if(isset($validation_title)){ ?>
                    <em><?php echo $validation_title; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                    <label>Ongkos</label>
                    <input name="fare" type="number" min="0" value="<?php if($_POST){ if($_POST['fare']!=NULL){ echo $_POST['fare']; } else{echo "";} }else{ echo $data['CityFare']; } ?>" class="form-control" placeholder="Ongkos ..">
                <?php
                  if(isset($validation_fare)){ ?>
                    <em><?php echo $validation_fare; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                    <label>Show / Hide</label><br>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="1" <?php if($_POST['show']!=NULL){ if($_POST['show']=="0"){ echo "checked"; } }else{if($data['CityShow']=="1"){ echo "checked"; }}?>/> Show
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="0" <?php if($_POST['show']!=NULL){ if($_POST['show']=="0"){ echo "checked"; } }else{ if($data['CityShow']=="0"){ echo "checked"; } } ?>/> Hide
                    </label>
                <?php
                  if(isset($validation_show)){ ?>
                    <br>
                    <em><?php echo $validation_show; ?></em>
                <?php
                  }
                ?>
                  </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                  <button type="button" onclick="window.location=('?page=city');" class="btn btn-warning pull-right" style="margin-right:5px;">Batal</button>
                </div>
              </form>
            <?php } ?>

            </div>
        </div>
    </div>
    
  </div>
</div>
<?php include('../templates/admin2/script_view.php'); ?>
</body>
</html>