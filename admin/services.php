<?php
error_reporting(0);
session_start();
include_once('../config/koneksi.php');
include_once('../model/query_model.php');
include_once('../model/validation_model.php');
include_once('../model/date_model.php');
include_once('../model/generate_model.php');
include_once('../model/proccess_model.php');
include_once('fungsi.php');
$db = new query_model();
$db->path = "../"; // Setting root configurasi config database
$validation = new validation_model();
$date = new date_model();
$generate = new generate_model();
$generate->path = "../";
$proccess = new proccess_model();

/* Cek login */
if(cek_login($mysqli) == false){ // Jika user tidak login
	header('location: login.php'); // Alihkan ke halaman login (index.php)
	exit();	
}
$stmt = $mysqli->prepare("SELECT AdminUserName FROM admin WHERE AdminID = ?");
$stmt->bind_param('i', $_SESSION['AdminID']);
$stmt->execute();
$stmt->store_result();
$stmt->bind_result($username);
$stmt->fetch();
/* Cek login - End */

/* Services */
if($_GET['page']=="services"){ 
	include "services/services_view.php"; 
}
elseif($_GET['page']=="services_add"){ 
	include "services/services_form_view.php"; 
}
elseif($_GET['page']=="services_edit"){ 
	include "services/services_form_view.php";
}
elseif($_GET['page']=="services_show"){ 
	$data['ServicesShow'] = 1;
    $where = ['ServicesID'=>$_GET['id']];
	$query_update = $db->update("services",$data,$where,"notlike");
	$proccess->generateSuccessMessage("success","services",$_GET['id'],"success","Show");
	echo "<script>window.location=('?page=services');</script>";
}
elseif($_GET['page']=="services_hide"){
	$data['ServicesShow'] = 0;
    $where = ['ServicesID'=>$_GET['id']];
	$query_update = $db->update("services",$data,$where,"notlike");
	$proccess->generateSuccessMessage("success","services",$_GET['id'],"success","Hide");
	echo "<script>window.location=('?page=services');</script>";
}
elseif($_GET['page']=="services_delete"){
	if($_GET['id']!=NULL){
		$id = $_GET['id'];
		$getData = $db->query("SELECT * FROM services WHERE ServicesID = '$id'","row");
		$where = ['ServicesID'=>$_GET['id']];
		$delete = $db->delete("services",$where,"notlike");
		if($delete==true){ 
	        unlink("../img/services/".$getData['ServicesImage']);
			$proccess->generateSuccessMessage("success","services",$_GET['id'],"success","Delete");
            echo "<script>window.location=('?page=services');</script>";
		}
		else{ 
			$proccess->generateSuccessMessage("danger","services",$_GET['id'],"failed","Delete");
            echo "<script>window.location=('?page=services');</script>";
		}
	}
	else{
            echo "<script>window.location=('?page=services');</script>";
	}
}
/* Services - end */

/* Services */
if($_GET['page']=="category"){ 
	include "services/services_category_view.php"; 
}
elseif($_GET['page']=="category_add"){ 
	include "services/services_category_form_view.php"; 
}
elseif($_GET['page']=="category_edit"){ 
	include "services/services_category_form_view.php";
}
elseif($_GET['page']=="category_show"){ 
	$data['ServicesCategoryShow'] = 1;
    $where = ['ServicesCategoryID'=>$_GET['id']];
	$query_update = $db->update("servicescategory",$data,$where,"notlike");
	$proccess->generateSuccessMessage("success","category",$_GET['id'],"success","Show");
	header('Location: ?page=category');
}
elseif($_GET['page']=="category_hide"){
	$data['ServicesCategoryShow'] = 0;
    $where = ['ServicesCategoryID'=>$_GET['id']];
	$query_update = $db->update("servicescategory",$data,$where,"notlike");
	$proccess->generateSuccessMessage("success","category",$_GET['id'],"success","Hide");
	header('Location: ?page=category');
}
elseif($_GET['page']=="category_delete"){
	if($_GET['id']!=NULL){
		$id = $_GET['id'];
		$getData = $db->query("SELECT * FROM servicescategory WHERE ServicesCategoryID = '$id'","row");
		$where = ['ServicesCategoryID'=>$_GET['id']];
		$delete = $db->delete("servicescategory",$where,"notlike");
		if($delete==true){ 
			$proccess->generateSuccessMessage("success","category",$_GET['id'],"success","Delete");
            header('Location: ?page=category');
		}
		else{ 
			$proccess->generateSuccessMessage("danger","category",$_GET['id'],"failed","Delete");
			header('Location: ?page=category');
		}
	}
	else{
		header('Location: ?page=category');
	}
}
/* Services - end */

else{ header('HTTP/1.0 404 Not Found', true, 404); }
?>