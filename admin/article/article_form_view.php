<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Ruang Administrator</title>
<?php include('../templates/admin2/css_view.php'); ?>
</head>
<body>
<div id="wrapper">
    <?php include('../templates/admin2/header_view.php'); ?>
    <div id="bodi">
        <?php include('../templates/admin2/sidebar_view.php'); ?>
        
        <div id="content1">
          <h2 class="judul_artikel"> 
        <?php
          if(preg_match("/add/", $_GET['page'])){
              $description = "Insert";
          }elseif(preg_match("/edit/", $_GET['page'])){
              $description = "Edit";
          }
        ?>
          <?= $description; ?> Article</h2>
            <div class="content2" style="font-size:18px; padding-top: 20px;">
        <?php
          /*  INSERT CONTENT  */
          if(preg_match("/add/", $_GET['page'])){
            $page_flag = "Insert";
            if($_POST){
              if($_POST['title']==NULL OR $_POST['show']==NULL OR $_POST['description'] == NULL OR $_POST['subdescription'] == NULL OR $_FILES['image']['name']==""){
                if($_POST['title']==NULL){ $validation_title = "Judul tidak boleh kosong."; }
                if($_POST['description']==NULL){ $validation_description = "Deskripsi tidak boleh kosong."; }
                if($_POST['subdescription']==NULL){ $validation_subdescription = "Sub Deskripsi tidak boleh kosong."; }
                if($_FILES['image']['name']==""){ $validation_image = "Gambar tidak boleh kosong."; }
                if($_POST['show']==NULL){ $validation_show = "Pilih apakah konten akan ditampilkan atau tidak."; }
              }
              else{
                /* INSERT PROSES */
                $id = $generate->generate_custom_id("A","ymd","contentpoint","ContentPointID",5);
                $data['ContentPointID'] = $id;
                $data['ContentPointLabel'] = "Article";
                $data['ContentPointTitle'] = $validation->default_rules($_POST['title']);
                $data['ContentPointPermalink'] = str_replace(" ", "-", $validation->default_rules($_POST['title'])).$id;
                $data['ContentPointSubDescription'] = $validation->default_rules($_POST['subdescription']);
                $data['ContentPointDescription'] = $validation->wysiwyg($_POST['description']);
                $data['ContentPointShow'] = $validation->default_rules($_POST['show']);
                $data['ContentPointDate'] = $date->getCurrentDate();
                if($_FILES['image']['name'] != NULL){
                  $data['ContentPointImage'] = $_FILES['image']['name'];
                  copy($_FILES ["image"]["tmp_name"], "../img/content/article/".$data['ContentPointImage']);
                }
                $data['AdminID'] = $_SESSION['AdminID'];
                $query_insert = $db->insert("contentpoint",$data);
                if($query_insert==false){
                  $proccess->generateSuccessMessage("danger","article",$_GET['id'],"failed","Insert");
                  echo "<script>window.location=('?page=article');</script>";
                }else{
                  $proccess->generateSuccessMessage("success","article",$_GET['id'],"success","Insert");
                  echo "<script>window.location=('?page=article');</script>";
                }
              }
            }
        ?>
              <form enctype="multipart/form-data" role="form" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
                <div class="box-body">
                  <div class="form-group">
                    <label>Judul</label>
                    <input name="title" type="text" value="<?php if($_POST['title']!=NULL){ echo $_POST['title']; } ?>" class="form-control" placeholder="Judul Artikel ..">
                <?php
                  if(isset($validation_title)){ ?>
                    <em><?php echo $validation_title; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                      <label>Sub Deskripsi</label>
                      <textarea class="form-control" rows="5" name="subdescription"><?php if($_POST['subdescription']!=NULL){ echo $_POST['subdescription']; } ?></textarea>
                <?php
                  if(isset($validation_subdescription)){ ?>
                    <em><?php echo $validation_subdescription; ?></em>
                <?php
                  }
                ?>
                  </div>

                  <div class="form-group">
                    <label for="exampleInputFile">Deskripsi</label>
                    <textarea name="description" class="form-control" id="ckeditor1"><?php if($_POST['description']!=NULL){ echo $_POST['description']; } ?></textarea>
                <?php
                  if(isset($validation_description)){ ?>
                    <em><?php echo $validation_description; ?></em>
                <?php
                  }
                ?>
                  </div>
                  
                 <div class="form-group">
                      <label id="label_image">Gambar</label>
                      <div class="fileupload fileupload-new" data-provides="fileupload">
                          <div class="fileupload-new thumbnail thumbnail-o-upload">
                          <?php
                              if($page_flag == "Update"){
                                  if($data['ContentPointImage'] == ''){
                          ?>
                              <img src="<?php echo "http://" . $_SERVER['SERVER_NAME']; ?>/vaneshasalon/img/default/noimage.png"/>
                          <?php
                                  } else {
                          ?>
                                  <img src="<?= "../img/content/article/".$data['ContentPointImage'] ?>"/>
                          <?php
                                  }
                              } else {
                          ?>
                              <img src="<?php echo "http://" . $_SERVER['SERVER_NAME']; ?>/vaneshasalon/img/default/noimage.png"/>
                          <?php
                              }
                          ?>
                          </div>
                          <div class="fileupload-preview fileupload-exists thumbnail thumbnail-o-upload"></div>
                          <div>
                              <span class="btn btn-default btn-file">
                              <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Pilih gambar</span>
                              <span class="fileupload-exists"><i class="fa fa-undo"></i> Ubah</span>
                              <input type="file" class="default" name="image" onchange="checkImage('image')" accept="image/*"/>
                              </span>
                          </div>
                          <?php
                          if(isset($validation_image)){ ?>
                            <br>
                            <em><?php echo $validation_image; ?></em>
                        <?php
                          }
                        ?>
                          <p class="help-block" id="error_image_photo"></p>
                      </div>
                  </div>

                  <div class="form-group">
                    <label>Show / Hide</label><br>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="1" <?php if($_POST['show']!=NULL){ if($_POST['show']=="1"){ echo "checked"; } } ?>/> Show
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="0" <?php if($_POST['show']!=NULL){ if($_POST['show']=="0"){ echo "checked"; } } ?>/> Hide
                    </label>
                <?php
                  if(isset($validation_show)){ ?>
                    <br>
                    <em><?php echo $validation_show; ?></em>
                <?php
                  }
                ?>
                  </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                  <button type="button" onclick="window.location=('?page=article');" class="btn btn-warning pull-right" style="margin-right:5px;">Batal</button>
                </div>
              </form>

          <?php
          }

          /*  UPDATE CONTENT  */
          elseif(preg_match("/edit/", $_GET['page'])){
            $page_flag = "Update";
            $data = $db->query("SELECT * FROM contentpoint WHERE ContentPointID='".$_GET['id']."'","row");
            if($_POST){
              if($_POST['title']==NULL OR $_POST['show']==NULL OR $_POST['description'] == NULL OR $_POST['subdescription'] == NULL){
                if($_POST['title']==NULL){ $validation_title = "Judul tidak boleh kosong."; }
                if($_POST['description']==NULL){ $validation_description = "Deskripsi tidak boleh kosong."; }
                if($_POST['subdescription']==NULL){ $validation_subdescription = "Sub Deskripsi tidak boleh kosong."; }
                if($_POST['show']==NULL){ $validation_show = "Pilih apakah konten akan ditampilkan atau tidak."; }
              }
              else{
                /* UPDATE PROSES */
                $id = $_GET['id'];
                $cek_data = $db->query("SELECT * FROM contentpoint WHERE ContentPointID = '$id'","row");
                $data['ContentPointTitle'] = $validation->default_rules($_POST['title']);
                $data['ContentPointPermalink'] = str_replace(" ", "-", $validation->default_rules($_POST['title']))."-".$_GET['id'];
                $data['ContentPointSubDescription'] = $validation->default_rules($_POST['subdescription']);
                $data['ContentPointDescription'] = $validation->wysiwyg($_POST['description']);
                $data['ContentPointShow'] = $validation->default_rules($_POST['show']);
                $data['ContentPointDate'] = $date->getCurrentDate();
                $data['AdminID'] = $_SESSION['AdminID'];
                $where = ['ContentPointID'=>$_GET['id']];
                if($_FILES['image']['name'] != NULL){
                  unlink("../img/content/article/".$cek_data['ContentPointImage']);
                  $data['ContentPointImage'] = $_FILES['image']['name'];
                }
                copy($_FILES ["image"]["tmp_name"], "../img/content/article/".$_FILES['image']['name']);
                $query_update = $db->update("contentpoint",$data,$where,"notlike");
                if($query_update==false){
                  $proccess->generateSuccessMessage("danger","article",$_GET['id'],"failed","Update");
                  echo "<script>window.location=('?page=article');</script>";
                }else{
                  $proccess->generateSuccessMessage("success","article",$_GET['id'],"success","Update");
                  echo "<script>window.location=('?page=article');</script>";
                }
              }
            }
        ?>

              <form enctype="multipart/form-data" role="form" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
                <div class="box-body">
                  <div class="box-body">
                  <div class="form-group">
                    <label>Judul</label>
                    <input name="title" type="text" value="<?php if($_POST){ if($_POST['title']!=NULL){ echo $_POST['title']; } else{echo "";} }else{ echo $data['ContentPointTitle']; } ?>" class="form-control" placeholder="Judul Artikel ..">
                <?php
                  if(isset($validation_title)){ ?>
                    <em><?php echo $validation_title; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                      <label>Sub Deskripsi</label>
                      <textarea class="form-control" rows="5" name="subdescription"><?php if($_POST){ if($_POST['subdescription']!=NULL){ echo $_POST['subdescription']; } else{echo "";} }else{ echo $data['ContentPointSubDescription']; } ?></textarea>
                <?php
                  if(isset($validation_subdescription)){ ?>
                    <em><?php echo $validation_subdescription; ?></em>
                <?php
                  }
                ?>
                  </div>

                  <div class="form-group">
                    <label for="exampleInputFile">Deskripsi</label>
                    <textarea name="description" class="form-control" id="ckeditor1"><?php if($_POST){ if($_POST['description']!=NULL){ echo $_POST['description']; } else{echo "";} }else{ echo $data['ContentPointDescription']; } ?></textarea>
                <?php
                  if(isset($validation_description)){ ?>
                    <em><?php echo $validation_description; ?></em>
                <?php
                  }
                ?>
                  </div>
                  
                 <div class="form-group">
                      <label id="label_image">Gambar</label>
                      <div class="fileupload fileupload-new" data-provides="fileupload">
                          <div class="fileupload-new thumbnail thumbnail-o-upload">
                          <?php
                              if($page_flag == "Update"){
                                  if($data['ContentPointImage'] == ''){
                          ?>
                              <img src="<?php echo "http://" . $_SERVER['SERVER_NAME']; ?>/vaneshasalon/img/default/noimage.png"/>
                          <?php
                                  } else {
                          ?>
                                  <img src="<?= "../img/content/article/".$data['ContentPointImage'] ?>"/>
                          <?php
                                  }
                              } else {
                          ?>
                              <img src="<?php echo "http://" . $_SERVER['SERVER_NAME']; ?>/vaneshasalon/img/default/noimage.png"/>
                          <?php
                              }
                          ?>
                          </div>
                          <div class="fileupload-preview fileupload-exists thumbnail thumbnail-o-upload"></div>
                          <div>
                              <span class="btn btn-default btn-file">
                              <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Pilih gambar</span>
                              <span class="fileupload-exists"><i class="fa fa-undo"></i> Ubah</span>
                              <input type="file" class="default" name="image" onchange="checkImage('image')" accept="image/*"/>
                              </span>
                          </div>
                          <p class="help-block" id="error_image_photo"></p>
                      </div>
                  </div>

                  <div class="form-group">
                    <label>Show / Hide</label><br>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="1" <?php if($_POST['show']!=NULL){ if($_POST['show']=="0"){ echo "checked"; } }else{if($data['ContentPointShow']=="1"){ echo "checked"; }}?>/> Show
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="0" <?php if($_POST['show']!=NULL){ if($_POST['show']=="0"){ echo "checked"; } }else{ if($data['ContentPointShow']=="0"){ echo "checked"; } } ?>/> Hide
                    </label>
                <?php
                  if(isset($validation_show)){ ?>
                    <br>
                    <em><?php echo $validation_show; ?></em>
                <?php
                  }
                ?>
                  </div>

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                  <button type="button" onclick="window.location=('?page=article');" class="btn btn-warning pull-right" style="margin-right:5px;">Batal</button>
                </div>
              </form>
            <?php } ?>

            </div>
        </div>
    </div>
    
  </div>
</div>
<?php include('../templates/admin2/script_view.php'); ?>
</body>
</html>