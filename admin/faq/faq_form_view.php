<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Ruang Administrator</title>
<?php include('../templates/admin2/css_view.php'); ?>
</head>
<body>
<div id="wrapper">
    <?php include('../templates/admin2/header_view.php'); ?>
    <div id="bodi">
        <?php include('../templates/admin2/sidebar_view.php'); ?>
        
        <div id="content1">
          <h2 class="judul_artikel"> 
        <?php
          if(preg_match("/add/", $_GET['page'])){
              $description = "Tambah";
          }elseif(preg_match("/edit/", $_GET['page'])){
              $description = "Perbaharui";
          }
        ?>
          <?= $description; ?> F.A.Q</h2>
            <div class="content2" style="font-size:18px; padding-top: 20px;">
        <?php
          /*  INSERT CONTENT  */
          if(preg_match("/add/", $_GET['page'])){
            $page_flag = "Insert";
            if($_POST){
              if($_POST['title']==NULL OR $_POST['show']==NULL){
                if($_POST['title']==NULL){ $validation_title = "The title can not be empty."; }
                if($_POST['description']==NULL){ $validation_description = "The description can not be empty."; }
                if($_POST['show']==NULL){ $validation_show = "The show can not be empty."; }
              }
              else{
                /* INSERT PROSES */
                $id = $generate->generate_custom_id("FAQ","ymd","contentpoint","ContentPointID",5);
                $data['ContentPointID'] = $id;
                $data['ContentPointLabel'] = "FAQ";
                $data['ContentPointTitle'] = $validation->default_rules($_POST['title']);
                $data['ContentPointPermalink'] = str_replace(" ", "-", $validation->default_rules($_POST['title'])).$id;
                $data['ContentPointDescription'] = $validation->wysiwyg($_POST['description']);
                $data['ContentPointShow'] = $validation->default_rules($_POST['show']);
                $data['ContentPointDate'] = $date->getCurrentDate();
                $data['AdminID'] = $_SESSION['AdminID'];
                $query_insert = $db->insert("contentpoint",$data);
                if($query_insert==false){
                  $proccess->generateSuccessMessage("danger","faq",$id,"failed","Insert");
                  echo "<script>window.location=('?page=faq');</script>";
                }else{
                  $proccess->generateSuccessMessage("success","faq",$id,"success","Insert");
                  echo "<script>window.location=('?page=faq');</script>";
                }
              }
            }
        ?>
              <form enctype="multipart/form-data" role="form" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
                <div class="box-body">
                  <div class="form-group">
                    <label>Pertanyaan</label>
                    <input name="title" type="text" value="<?php if($_POST['title']!=NULL){ echo $_POST['title']; } ?>" class="form-control" placeholder="Question ..">
                <?php
                  if(isset($validation_title)){ ?>
                    <em><?php echo $validation_title; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputFile">Jawaban</label>
                    <textarea name="description" class="form-control" id="ckeditor1"><?php if($_POST['description']!=NULL){ echo $_POST['description']; } ?></textarea>
                <?php
                  if(isset($validation_description)){ ?>
                    <em><?php echo $validation_description; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                    <label>Show / Hide</label><br>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="1" <?php if($_POST['show']!=NULL){ if($_POST['show']=="1"){ echo "checked"; } } ?>/> Show
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="0" <?php if($_POST['show']!=NULL){ if($_POST['show']=="0"){ echo "checked"; } } ?>/> Hide
                    </label>
                <?php
                  if(isset($validation_show)){ ?>
                    <br>
                    <em><?php echo $validation_show; ?></em>
                <?php
                  }
                ?>
                  </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                  <button type="button" onclick="window.location=('?page=faq');" class="btn btn-warning pull-right" style="margin-right:5px;">Batal</button>
                </div>
              </form>

          <?php
          }

          /*  UPDATE CONTENT  */
          elseif(preg_match("/edit/", $_GET['page'])){
            $page_flag = "Update";
            $data = $db->query("SELECT * FROM contentpoint WHERE ContentPointID='".$_GET['id']."'","row");
            if($_POST){
              if($_POST['title']==NULL OR $_POST['show']==NULL){
                if($_POST['title']==NULL){ $validation_title = "The title can not be empty."; }
                if($_POST['description']==NULL){ $validation_description = "The description can not be empty."; }
                if($_POST['show']==NULL){ $validation_show = "The show can not be empty."; }
              }
              else{
                /* UPDATE PROSES */
                $id = $_GET['id'];
                $cek_data = $db->query("SELECT * FROM contentpoint WHERE ContentPointID = '$id'","row");
                $data['ContentPointTitle'] = $validation->default_rules($_POST['title']);
                $data['ContentPointPermalink'] = str_replace(" ", "-", $validation->default_rules($_POST['title']))."-".$_GET['id'];
                $data['ContentPointDescription'] = $validation->wysiwyg($_POST['description']);
                $data['ContentPointShow'] = $validation->default_rules($_POST['show']);
                $data['ContentPointDate'] = $date->getCurrentDate();
                $data['AdminID'] = $_SESSION['AdminID'];
                $where = ['ContentPointID'=>$_GET['id']];
                $query_update = $db->update("contentpoint",$data,$where,"notlike");
                if($query_update==false){
                  $proccess->generateSuccessMessage("danger","faq",$_GET['id'],"failed","Update");
                  echo "<script>window.location=('?page=faq');</script>";
                }else{
                  $proccess->generateSuccessMessage("success","faq",$_GET['id'],"success","Update");
                  echo "<script>window.location=('?page=faq');</script>";
                }
              }
            }
        ?>

              <form enctype="multipart/form-data" role="form" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
                <div class="box-body">
                  <div class="box-body">
                  <div class="form-group">
                    <label>Pertanyaan</label>
                    <input name="title" type="text" value="<?php if($_POST){ if($_POST['title']!=NULL){ echo $_POST['title']; } else{echo "";} }else{ echo $data['ContentPointTitle']; } ?>" class="form-control" placeholder="Question ..">
                <?php
                  if(isset($validation_title)){ ?>
                    <em><?php echo $validation_title; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputFile">Jawaban</label>
                    <textarea name="description" class="form-control" id="ckeditor1"><?php if($_POST){ if($_POST['description']!=NULL){ echo $_POST['description']; } else{echo "";} }else{ echo $data['ContentPointDescription']; } ?></textarea>
                <?php
                  if(isset($validation_description)){ ?>
                    <em><?php echo $validation_description; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                    <label>Show / Hide</label><br>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="1" <?php if($_POST['show']!=NULL){ if($_POST['show']=="0"){ echo "checked"; } }else{if($data['ContentPointShow']=="1"){ echo "checked"; }}?>/> Show
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="0" <?php if($_POST['show']!=NULL){ if($_POST['show']=="0"){ echo "checked"; } }else{ if($data['ContentPointShow']=="0"){ echo "checked"; } } ?>/> Hide
                    </label>
                <?php
                  if(isset($validation_show)){ ?>
                    <br>
                    <em><?php echo $validation_show; ?></em>
                <?php
                  }
                ?>
                  </div>

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                  <button type="button" onclick="window.location=('?page=faq');" class="btn btn-warning pull-right" style="margin-right:5px;">Batal</button>
                </div>
              </form>
            <?php } ?>

            </div>
        </div>
    </div>
    
  </div>
</div>
<?php include('../templates/admin2/script_view.php'); ?>
</body>
</html>