<?php
if($_POST){
    if($_POST['title']==NULL OR $_POST['subtitle']==NULL OR $_POST['email']==NULL OR $_POST['location']==NULL){
      if($_POST['title']==NULL){ $validation_title = "Jam kerja 1 tidak boleh kosong."; }
      if($_POST['subtitle']==NULL){ $validation_subtitle = "Jam kerja 2 tidak boleh kosong."; }
      if($_POST['email']==NULL){ $validation_email = "Email tidak boleh kosong."; }
      if($_POST['location']==NULL){ $validation_location = "Lokasi tidak boleh kosong."; }
    }else{
      /* Update PROSES */
        $cek_data = $db->query("SELECT * FROM content WHERE ContentLabel = 'Contact Us'","row");
        $data['ContentDescription'] = $validation->default_rules($_POST['title']);
        $data['ContentSubDescription'] = $validation->wysiwyg($_POST['location']);
        $data['ContentLink'] = $validation->default_rules($_POST['subtitle']);
        $data['ContentImage'] = $validation->default_rules($_POST['email']);
        $data['ContentDate'] = $date->getCurrentDate();
        $data['AdminID'] = $_SESSION['AdminID'];
        $where = ['ContentLabel'=> 'Contact Us'];
        $query_update = $db->update("content",$data,$where,"notlike");
        if($query_update==false){
          $_SESSION['success_type'] = "danger"; /* succes,info,warning,danger */
          $_SESSION['success_message'] = "Update Contact Us failed.";
          $_SESSION['success_parameter'] = "Update failed.";
          echo "<script>alert('Update Contact Us Failed.');</script>";
          echo "<script>window.location=('?page=contact_us');</script>";
        }else{
          $_SESSION['success_type'] = "success"; /* succes,info,warning,danger */
          $_SESSION['success_message'] = "Update Contact Us success.";
          $_SESSION['success_parameter'] = "Update success.";
          echo "<script>alert('Update Contact Us Success.');</script>";
          echo "<script>window.location=('?page=contact_us');</script>";
        }
    }
  }
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Ruang Administrator</title>
<?php include('../templates/admin2/css_view.php'); ?>
<style type="text/css">
  .form-group em{ font-size: 16px; font-style: italic; color: #d26a5c;}
</style>
</head>
<body>
<div id="wrapper">
    <?php include('../templates/admin2/header_view.php'); ?>
    <div id="bodi">
        <?php include('../templates/admin2/sidebar_view.php'); ?>
        
        <div id="content1">
        	<h2 class="judul_artikel">Perbaharui Kontak Kami</h2>
            <div class="content2" style="font-size:18px; padding-top: 20px;">
            <?php
              if(isset($_SESSION['success_message'])){
            ?>
                <div class="alert alert-<?= $_SESSION['success_type']; ?> alert-dismissable">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <strong><?= $_SESSION['success_parameter']; ?></strong> <?= $_SESSION['success_message']; ?>
                </div>
                <!-- Success Message - End -->
            <?php
                unset($_SESSION['success_type']);
                unset($_SESSION['success_message']);
                unset($_SESSION['success_parameter']);
              }
            ?>
                <?php
                  $data = $db->query("SELECT * FROM content WHERE ContentLabel='Contact Us'");
                  if($data==false){
                      $id = $generate->generate_custom_id("C","ymd","content","ContentID",3);
                      $data['ContentID'] = $id;
                      $data['ContentLabel'] = "Contact Us";
                      $data['ContentDate'] = $date->getCurrentDate();
                      $query_insert = $db->insert("content",$data);
                  }
                  else{
                    $data = $db->query("SELECT * FROM content WHERE ContentLabel = 'Contact Us'","row");
                    $page_flag = "Update";
                  }
                ?>
                <form role="form" method="post" enctype="multipart/form-data" style="width:94%; margin:0 auto;">
                  <div class="box-body">
                    <div class="form-group">
                      <label>Jam Kerja (Office Hours) 1</label>
                      <input name="title" type="text" value="<?php if($_POST['title']!=NULL){ echo $_POST['title']; }else{ echo $data['ContentDescription']; } ?>" class="form-control" placeholder="Office Hours 1 ..">
                  <?php
                    if(isset($validation_title)){ ?>
                      <em><?php echo $validation_title; ?></em>
                  <?php
                    }
                  ?>
                    </div>

                    <div class="form-group">
                      <label>Jam Kerja (Office Hours) 2</label>
                      <input name="subtitle" type="text" value="<?php if($_POST['subtitle']!=NULL){ echo $_POST['subtitle']; }else{ echo $data['ContentLink']; } ?>" class="form-control" placeholder="Office Hours 2 ..">
                  <?php
                    if(isset($validation_subtitle)){ ?>
                      <em><?php echo $validation_subtitle; ?></em>
                  <?php
                    }
                  ?>
                    </div>

                    <div class="form-group">
                      <label>Email</label>
                      <input name="email" type="email" value="<?php if($_POST['email']!=NULL){ echo $_POST['email']; }else{ echo $data['ContentImage']; } ?>" class="form-control" placeholder="Email ..">
                  <?php
                    if(isset($validation_email)){ ?>
                      <em><?php echo $validation_email; ?></em>
                  <?php
                    }
                  ?>
                    </div>

                    <div class="form-group">
                      <label>Lokasi</label>
                      <textarea name="location" rows="5" class="form-control"><?php if($_POST['location']!=NULL){ echo $_POST['location']; }else{ echo $data['ContentSubDescription']; } ?></textarea>
                  <?php
                    if(isset($validation_location)){ ?>
                      <em><?php echo $validation_location; ?></em>
                  <?php
                    }
                  ?>
                    </div>
                    
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                  </div>
                </form>
            </div>
        </div>
		</div>
		
	</div>
</div>
<?php include('../templates/admin2/script_view.php'); ?>
</body>
</html>