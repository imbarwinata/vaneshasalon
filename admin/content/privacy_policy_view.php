<?php
if($_POST){
    if($_POST['description']==NULL OR $_POST['title']==NULL){
      if($_POST['title']==NULL){ $validation_title = "The title can not be empty."; }
      if($_POST['description']==NULL){ $validation_description = "The description can not be empty."; }
    }else{
      /* Update PROSES */
        $cek_data = $db->query("SELECT * FROM content WHERE ContentLabel = 'Privacy and Policy'","row");
        $data['ContentTitle'] = $validation->default_rules($_POST['title']);
        $data['ContentDescription'] = $validation->wysiwyg($_POST['description']);
        $data['ContentDate'] = $date->getCurrentDate();
        $data['AdminID'] = $_SESSION['AdminID'];
        $where = ['ContentLabel'=> 'Privacy and Policy'];
        $query_update = $db->update("content",$data,$where,"notlike");
        if($query_update==false){
          $_SESSION['success_type'] = "danger"; /* succes,info,warning,danger */
          $_SESSION['success_message'] = "Update Privacy and Policy failed.";
          $_SESSION['success_parameter'] = "Update failed.";
          echo "<script>alert('Privacy and Policy Saved Failed.');</script>";
          echo "<script>window.location=('?page=privacy_policy');</script>";
        }else{
          $_SESSION['success_type'] = "success"; /* succes,info,warning,danger */
          $_SESSION['success_message'] = "Update Privacy and Policy success.";
          $_SESSION['success_parameter'] = "Update success.";
          echo "<script>alert('Privacy and Policy Saved Success.');</script>";
          echo "<script>window.location=('?page=privacy_policy');</script>";
        }
    }
  }
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Ruang Administrator</title>
<?php include('../templates/admin2/css_view.php'); ?>
<style type="text/css">
  .form-group em{ font-size: 16px; font-style: italic; color: #d26a5c;}
</style>
</head>
<body>
<div id="wrapper">
    <?php include('../templates/admin2/header_view.php'); ?>
    <div id="bodi">
        <?php include('../templates/admin2/sidebar_view.php'); ?>
        
        <div id="content1">
        	<h2 class="judul_artikel">Perbaharui Privacy and Policy</h2>
            <div class="content2" style="font-size:18px; padding-top: 20px;">
            <?php
              if(isset($_SESSION['success_message'])){
            ?>
                <div class="alert alert-<?= $_SESSION['success_type']; ?> alert-dismissable">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <strong><?= $_SESSION['success_parameter']; ?></strong> <?= $_SESSION['success_message']; ?>
                </div>
                <!-- Success Message - End -->
            <?php
                unset($_SESSION['success_type']);
                unset($_SESSION['success_message']);
                unset($_SESSION['success_parameter']);
              }
            ?>
                <?php
                  $data = $db->query("SELECT * FROM content WHERE ContentLabel='Privacy and Policy'");
                  if($data==false){
                      $id = $generate->generate_custom_id("C","ymd","content","ContentID",3);
                      $data['ContentID'] = $id;
                      $data['ContentLabel'] = "Privacy and Policy";
                      $data['ContentDate'] = $date->getCurrentDate();
                      $query_insert = $db->insert("content",$data);
                  }
                  else{
                    $data = $db->query("SELECT * FROM content WHERE ContentLabel = 'Privacy and Policy'","row");
                    $page_flag = "Update";
                  }
                ?>
                <form role="form" method="post" enctype="multipart/form-data" style="width:94%; margin:0 auto;">
                  <div class="box-body">
                    <div class="form-group">
                      <label>Judul</label>
                      <input name="title" type="text" value="<?php if($_POST['title']!=NULL){ echo $_POST['title']; }else{ echo $data['ContentTitle']; } ?>" class="form-control" placeholder="Title ..">
                  <?php
                    if(isset($validation_title)){ ?>
                      <em><?php echo $validation_title; ?></em>
                  <?php
                    }
                  ?>
                    </div>

                    <div class="form-group">
                    <label for="exampleInputFile">Deskripsi</label>
                    <textarea name="description" class="form-control" id="ckeditor1"><?php if($_POST['description']!=NULL){ echo $_POST['description']; }else{ echo $data['ContentDescription']; } ?></textarea>
                <?php
                  if(isset($validation_description)){ ?>
                    <em><?php echo $validation_description; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary pull-right">Save</button>
                  </div>
                </form>
            </div>
        </div>
		</div>
		
	</div>
</div>
<?php include('../templates/admin2/script_view.php'); ?>
</body>
</html>