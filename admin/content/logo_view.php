<?php
if($_POST){
  if($_POST['image_title']==NULL){
    if($_POST['image_title']==NULL){ $validation_image_title = "The image title can not be empty."; }
    if($_FILES['image']['name']==""){ $validation_image = "The image can not be empty."; }
  }else{
    /* Update PROSES */
      $cek_data = $db->query("SELECT * FROM content WHERE ContentLabel = 'Logo'","row");
      if($_FILES['image']['name'] != ""){
        $data['ContentImage']=$_FILES['banner']['name'];
        unlink("../img/content/banner/".$cek_data['ContentImage']);
    
        $temp = explode(".", $_FILES["image"]["name"]);
        $newfilename = $cek_data['ContentID'] . '.' . end($temp);
        $data['ContentImage'] = $newfilename;
      }
      $data['ContentTitle'] = $validation->default_rules($_POST['image_title']);

      $where = ['ContentLabel'=> 'Logo'];
      $query_update = $db->update("content",$data,$where,"notlike");
      copy($_FILES ["image"]["tmp_name"], "../img/content/logo/".$data['ContentImage']);
     if($query_update==false){
          $_SESSION['success_type'] = "danger"; /* succes,info,warning,danger */
          $_SESSION['success_message'] = "Update Contact Us failed.";
          $_SESSION['success_parameter'] = "Update failed.";
          echo "<script>alert('Update Logo Failed.');</script>";
          echo "<script>window.location=('content.php?page=logo');</script>";
        }else{
          $_SESSION['success_type'] = "success"; /* succes,info,warning,danger */
          $_SESSION['success_message'] = "Update Contact Us success.";
          $_SESSION['success_parameter'] = "Update success.";
          echo "<script>alert('Update Logo Success.');</script>";
          echo "<script>window.location=('content.php?page=logo');</script>";
      }
  }
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Ruang Administrator</title>
<?php include('../templates/admin2/css_view.php'); ?>
<style type="text/css">
  .form-group em{ font-size: 16px; font-style: italic; color: #d26a5c;}
</style>
</head>
<body>
<div id="wrapper">
    <?php include('../templates/admin2/header_view.php'); ?>
    <div id="bodi">
        <?php include('../templates/admin2/sidebar_view.php'); ?>
        
        <div id="content1">
        	<h2 class="judul_artikel">Perbaharui Logo Website</h2>
            <div class="content2" style="font-size:18px; padding-top: 20px;">
                <?php
                  $data = $db->query("SELECT * FROM content WHERE ContentLabel='Logo'");
                  if($data==false){
                      $id = $generate->generate_custom_id("C","ymd","content","ContentID",3);
                      $data['ContentID'] = $id;
                      $data['ContentLabel'] = "Logo";
                      $data['ContentDate'] = $date->getCurrentDate();
                      $query_insert = $db->insert("content",$data);
                  }
                  else{
                    $data = $db->query("SELECT * FROM content WHERE ContentLabel = 'Logo'","row");
                    $page_flag = "Update";
                  }
                ?>
                <form role="form" method="post" enctype="multipart/form-data" action="<?php $_SERVER["PHP_SELF"] ?>" style="width:94%; margin:0 auto;">
                  <div class="box-body">
                    <div class="form-group">
                      <label>Judul Gambar</label>
                      <input name="image_title" type="text" value="<?php if($_POST['image_title']!=NULL){ echo $_POST['image_title']; }else{ echo $data['ContentTitle']; } ?>" class="form-control" placeholder="Judul Gambar ..">
                  <?php
                    if(isset($validation_image_title)){ ?>
                      <em><?php echo $validation_image_title; ?></em>
                  <?php
                    }
                  ?>
                    </div>

                    <div class="form-group">
                        <label id="label_image">Gambar</label>
                        <div class="fileupload fileupload-new" data-provides="fileupload">
                            <div class="fileupload-new thumbnail thumbnail-o-upload">
                            <?php
                                if($page_flag == "Update"){
                                    if($data['ContentImage'] == ''){
                            ?>
                                <img src="<?php echo "http://" . $_SERVER['SERVER_NAME']; ?>/vaneshasalon/img/default/noimage.png"/>
                            <?php
                                    } else {
                            ?>
                                    <img src="<?= "../img/content/logo/".$data['ContentImage'] ?>"/>
                            <?php
                                    }
                                } else {
                            ?>
                                <img src="<?php echo "http://" . $_SERVER['SERVER_NAME']; ?>/vaneshasalon/img/default/noimage.png"/>
                            <?php
                                }
                            ?>
                            </div>
                            <div class="fileupload-preview fileupload-exists thumbnail thumbnail-o-upload"></div>
                            <div>
                                <span class="btn btn-default btn-file">
                                <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Pilih gambar</span>
                                <span class="fileupload-exists"><i class="fa fa-undo"></i> Ubah</span>
                                <input type="file" class="default" name="image" onchange="checkImage('image')" accept="image/*"/>
                                </span>
                            </div>
                            <p class="help-block" id="error_image_photo"></p>
                  <?php
                    if(isset($validation_image)){ ?>
                      <em><?php echo $validation_image; ?></em>
                  <?php
                    }
                  ?>
                        </div>
                    </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary pull-right">Save</button>
                  </div>
                </form>
            </div>
        </div>
		</div>
		
	</div>
</div>
<?php include('../templates/admin2/script_view.php'); ?>
</body>
</html>