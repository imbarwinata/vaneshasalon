<?php
error_reporting(0);
session_start();
include_once('../config/koneksi.php');
include_once('../model/query_model.php');
include_once('../model/validation_model.php');
include_once('../model/date_model.php');
include_once('../model/generate_model.php');
include_once('../model/proccess_model.php');
include_once('fungsi.php');
$db = new query_model();
$db->path = "../"; // Setting root configurasi config database
$validation = new validation_model();
$date = new date_model();
$generate = new generate_model();
$generate->path = "../";
$proccess = new proccess_model();

/* Cek login */
if(cek_login($mysqli) == false){ // Jika user tidak login
	header('location: login.php'); // Alihkan ke halaman login (index.php)
	exit();	
}
$stmt = $mysqli->prepare("SELECT AdminUserName FROM admin WHERE AdminID = ?");
$stmt->bind_param('i', $_SESSION['AdminID']);
$stmt->execute();
$stmt->store_result();
$stmt->bind_result($username);
$stmt->fetch();
/* Cek login - End */

/* About */
if($_GET['page']=="about"){ 
	include "about/about_view.php"; 
}
elseif($_GET['page']=="about_add"){ 
	include "about/about_form_view.php"; 
}
elseif($_GET['page']=="about_edit"){ 
	include "about/about_form_view.php";
}
elseif($_GET['page']=="about_show"){ 
	$data['ContentPointShow'] = 1;
    $where = ['ContentPointID'=>$_GET['id']];
	$query_update = $db->update("contentpoint",$data,$where,"notlike");
	$proccess->generateSuccessMessage("success","about",$_GET['id'],"success","Show");
	header('Location: ?page=about');
}
elseif($_GET['page']=="about_hide"){
	$data['ContentPointShow'] = 0;
    $where = ['ContentPointID'=>$_GET['id']];
	$query_update = $db->update("contentpoint",$data,$where,"notlike");
	$proccess->generateSuccessMessage("success","about",$_GET['id'],"success","Hide");
	header('Location: ?page=about');
}
elseif($_GET['page']=="about_delete"){
	if($_GET['id']!=NULL){
		$id = $_GET['id'];
		$getData = $db->query("SELECT * FROM contentpoint WHERE ContentPointID = '$id'","row");
		$where = ['ContentPointID'=>$_GET['id']];
		$delete = $db->delete("contentpoint",$where,"notlike");
		if($delete==true){ 
	        unlink("../img/content/about/".$getData['ContentPointImage']);
			$proccess->generateSuccessMessage("success","about",$_GET['id'],"success","Delete");
            header('Location: ?page=about');
		}
		else{ 
			$proccess->generateSuccessMessage("danger","about",$_GET['id'],"failed","Delete");
			header('Location: ?page=about');
		}
	}
	else{
		header('Location: ?page=about');
	}
}
/* About - end */
else{ header('HTTP/1.0 404 Not Found', true, 404); }
?>