<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Ruang Administrator</title>
<?php include('../templates/admin2/css_view.php'); ?>
</head>
<body>
<div id="wrapper">
    <?php include('../templates/admin2/header_view.php'); ?>
    <div id="bodi">
        <?php include('../templates/admin2/sidebar_view.php'); ?>
        
        <div id="content1">
        	<h2 class="judul_artikel">Daftar Pesanan</h2>
            <div class="content2" style="font-size:18px; padding-top: 20px;">
                <?php
                    if(isset($_SESSION['success_message'])){
                  ?>
                      <div class="alert alert-<?= $_SESSION['success_type']; ?> alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong><?= $_SESSION['success_parameter']; ?></strong> <?= $_SESSION['success_message']; ?>
                      </div>
                      <!-- Success Message - End -->
                  <?php
                      unset($_SESSION['success_type']);
                      unset($_SESSION['success_message']);
                      unset($_SESSION['success_parameter']);
                    }
                  ?>
                <table id="example" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Nama Pemesan</th>
                    <th>Total</th>
                    <th>Status</th>
                    <th style="width:110px;">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                  $data = $db->query("SELECT * FROM trorder ORDER BY OrderDate ASC","result");
                  foreach ($data as $data_list){
                ?>
                    <tr>
                      <td><?= $data_list->OrderName; ?></td>
                      <td><?= "Rp. ".number_format($data_list->OrderTotal,0,",","."); ?></td>
                      <td><?php
                          if($data_list->OrderStatus == 0){
                            echo "<span class='label label-warning'>Pending</span>";
                          }elseif ($data_list->OrderStatus == 1) {
                            echo "<span class='label label-danger'>Cancel</span>";
                          }elseif ($data_list->OrderStatus == 2) {
                            echo "<span class='label label-primary'>Process</span>";
                          }elseif ($data_list->OrderStatus == 3) {
                            echo "<span class='label label-info'>Departure to destination</span>";
                          }elseif ($data_list->OrderStatus == 4) {
                            echo "<span class='label label-success'>Finish</span>";
                          }
                      ?>
                      </td>
                      <td style="width:110px;">
                        <a href="?page=<?php echo $_GET['page']; ?>_detail&id=<?= $data_list->OrderID; ?>" class="btn btn-default btn-flat" data-toggle="tooltip" title="Detail"><i class="fa fa-search"></i></a>
                      <?php
                          if($data_list->OrderStatus == 2 OR $data_list->OrderStatus == 3 OR $data_list->OrderStatus == 4){ ?>
                        <a href="?page=<?php echo $_GET['page']; ?>_edit&id=<?= $data_list->OrderID; ?>" class="btn btn-warning btn-flat" data-toggle="tooltip" title="Detail"><i class="fa fa-edit"></i></a>
                      <?php
                          }
                      ?>
                      </td>
                    </tr>
                <?php 
                  }
                ?>
                </tbody>
              </table>
            </div>

        </div>
		</div>
		
	</div>
</div>
<?php include('../templates/admin2/script_view.php'); ?>
</body>
</html>