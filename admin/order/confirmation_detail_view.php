<?php
  if($_POST){
    $query['ConfirmationStatus'] = $validation->default_rules($_POST['status']);
    $where = ['ConfirmationID'=>$_GET['id']];
    $update = $db->update("confirmation",$query,$where,"notlike");
    if($_POST['status']==1){
      $query2['OrderStatus'] = 2;
      $where2 = ['OrderID'=>$confirmation['OrderID']];
      $update = $db->update("trorder",$query2,$where2,"notlike");
    }else{
      $query2['OrderStatus'] = 0;
      $where2 = ['OrderID'=>$confirmation['OrderID']];
      $update = $db->update("trorder",$query2,$where2,"notlike");
    }
    if($update==true){
      $proccess->generateSuccessMessage("success","confirmation",$_GET['id'],"success","Update");
      header('location: ?page=confirmation');
      exit();
    }else{
      $proccess->generateSuccessMessage("danger","confirmation",$_GET['id'],"failed","Update");
      header('location: ?page=confirmation');
      exit();
    }
  }
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Ruang Administrator</title>
<?php include('../templates/admin2/css_view.php'); ?>
</head>
<body>
<div id="wrapper">
    <?php include('../templates/admin2/header_view.php'); ?>
    <div id="bodi">
        <?php include('../templates/admin2/sidebar_view.php'); ?>
        
        <div id="content1">
        	<h2 class="judul_artikel">Detail Konfirmasi</h2>
            <div class="content2" style="font-size:18px; padding-top: 20px;">
                <?php
                    if(isset($_SESSION['success_message'])){
                  ?>
                      <div class="alert alert-<?= $_SESSION['success_type']; ?> alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong><?= $_SESSION['success_parameter']; ?></strong> <?= $_SESSION['success_message']; ?>
                      </div>
                      <!-- Success Message - End -->
                  <?php
                      unset($_SESSION['success_type']);
                      unset($_SESSION['success_message']);
                      unset($_SESSION['success_parameter']);
                    }
                  ?>
              <a href="?page=confirmation" class="btn btn-primary pull-right" data-toggle="tooltip" data-placement="left" title="Kembali"><span class="fa fa-backward"></span> Kembali</a><br><br>
                <form method="post">
                  <div class="form-group">
                    <label for="">Nama Nasabah</label>
                    <input name="nama" type="text" value="<?= $confirmation['ConfirmationName']; ?>" disabled="disabled" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="">Rekening Tujuan</label>
                    <?php
                    $account = "";
                    if($confirmation['ConfirmationAccount']==0){
                      $account = "BCA";
                    }elseif($confirmation['ConfirmationAccount']==1){
                      $account = "BRI";
                    }
                    ?>
                    <input name="account" type="text" value="<?= $account; ?>" disabled="disabled" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="">Waktu Konfirmasi</label>
                    <input name="date" type="text" value="<?= $date->convertFormat("H:i:s - d M Y",$confirmation['ConfirmationDate']); ?>" disabled="disabled" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="">Status Konfirmasi</label>
                    <?php
                    $account = "";
                    $list_status = [0=>'Konfirmasi belum dicek',1=>'Konfirmasi  diterima',2=>'Konfirmasi ditolak'];
                    ?>
                    <select name="status" class="form-control">
                    <?php
                      foreach ($list_status as $key => $value): ?>
                      <option value="<?= $key; ?>" <?php if($confirmation['ConfirmationStatus'] == $key){ echo "selected"; } ?>><?= $value; ?></option>
                    <?php
                      endforeach;
                    ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="">Bukti Transfer</label><br>
                    <img style="width:100%;" src="../img/bukti/<?= $confirmation['ConfirmationImage']; ?>">
                  </div>
                  <div class="form-group">
                    <button class="form-control btn btn-primary btn-block" type="submit">Simpan</button>
                  </div>
                </form>
            </div>

        </div>
		</div>
		
	</div>
</div>
<?php include('../templates/admin2/script_view.php'); ?>
</body>
</html>