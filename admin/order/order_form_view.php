28<?php
if($_POST){
  $query['OrderStatus'] = $validation->default_rules($_POST['status']);
  $where = ['OrderID'=>$cek_order['OrderID']];
  $update = $db->update("trorder",$query,$where,"notlike");
  if($update==true){
    $proccess->generateSuccessMessage("success","pesanan",$_GET['id'],"success","Update");
    header('location: ?page=order');
    exit();
    echo "<script>window.location=('?page=order');</script>";
  }else{
    $proccess->generateSuccessMessage("danger","pesanan",$_GET['id'],"failed","Update");
    header('location: ?page=order');
    exit();
    echo "<script>window.location=('?page=order');</script>";
  }
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Ruang Administrator</title>
<?php include('../templates/admin2/css_view.php'); ?>
</head>
<body>
<div id="wrapper">
    <?php include('../templates/admin2/header_view.php'); ?>
    <div id="bodi">
        <?php include('../templates/admin2/sidebar_view.php'); ?>
        
        <div id="content1">
        	<h2 class="judul_artikel">Perbaharui Status Pesanan <small>( <?= $cek_order['OrderID']; ?> )</small></h2>
            <div class="content2" style="font-size:18px; padding-top: 20px;">
                <?php
                    if(isset($_SESSION['success_message'])){
                  ?>
                      <div class="alert alert-<?= $_SESSION['success_type']; ?> alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong><?= $_SESSION['success_parameter']; ?></strong> <?= $_SESSION['success_message']; ?>
                      </div>
                      <!-- Success Message - End -->
                  <?php
                      unset($_SESSION['success_type']);
                      unset($_SESSION['success_message']);
                      unset($_SESSION['success_parameter']);
                    }
                  ?>
              <a href="?page=order" class="btn btn-primary pull-right" data-toggle="tooltip" data-placement="left" title="Kembali"><span class="fa fa-backward"></span> Kembali</a><br><br>
              <form method="post">
                <table class="table table-bordered table-striped">
                <tbody>
                  <tr>
                    <td style="width:25%;" colspan="3">Anggota <div class="pull-right">:</div> </td>
                    <td><?= $cek_order['MemberName']; ?></td>
                  </tr>
                  <tr>
                    <td style="width:25%;" colspan="3">Nama <div class="pull-right">:</div> </td>
                    <td><?= $cek_order['OrderName']; ?></td>
                  </tr>
                  <tr>
                    <td style="width:25%;" colspan="3">Nomor Telepon / HP <div class="pull-right">:</div> </td>
                    <td><?= $cek_order['OrderPhone']; ?></td>
                  </tr>
                  <tr>
                    <td style="width:25%;" colspan="3">Alamat <div class="pull-right">:</div> </td>
                    <td><?= $cek_order['OrderAddress']; ?></td>
                  </tr>
                   <tr>
                    <td style="width:25%;" colspan="3">Tanggal Order <div class="pull-right">:</div> </td>
                    <td><?= $date->convertFormat("H:i:s",$cek_order['OrderDateEntry'])." - ".$date->convertFormat("d M Y",$cek_order['OrderDateEntry']); ?></td>
                  </tr>
                   <tr>
                    <td style="width:25%;" colspan="3">Tanggal Pemesanan Layanan <div class="pull-right">:</div> </td>
                    <td><?= $date->convertFormat("H:i:s",$cek_order['OrderDate'])." - ".$date->convertFormat("d M Y",$cek_order['OrderDate']); ?></td>
                  </tr>
                  <tr>
                    <td style="width:25%;" colspan="3">Status Pesanan <div class="pull-right">:</div> </td>
                    <td>
                         <div class="form-group">
                          <select name="status" class="form-control" id="">
                      <?php 
                      $list_status = [2 =>'Process', 3 =>'Departure to destination', 4 =>'Finish']; 
                      foreach ($list_status as $key => $value): ?>
                        <option value="<?= $key; ?>" <?php if($key==$cek_order['OrderStatus']){ echo "selected"; } ?>><?= $value; ?></option>
                      <?php
                      endforeach;
                      ?>
                          </select>
                        </div>
                    </td>
                  </tr>
                  <tr>
                    <td style="width:25%;" colspan="3">Konfirmasi Pembayaran <div class="pull-right">:</div> </td>
                     <td><?php 
                        $cek_konfirmasi = $db->query("SELECT * FROM confirmation WHERE OrderID='".$cek_order['OrderID']."' AND MemberID='".$cek_order['MemberID']."'","row");
                        if($cek_konfirmasi==false){
                          echo "Belum ada konfirmasi";
                        }else{
                          if($cek_konfirmasi['ConfirmationStatus']==0){
                            echo "Konfirmasi belum dicek";
                          }elseif($cek_konfirmasi['ConfirmationStatus']==1){
                            echo "Konfirmasi diterima";
                          }elseif($cek_konfirmasi['ConfirmationStatus']==2){
                            echo "Konfirmasi ditolak";
                          }
                        }
                    ?></td>
                  </tr>
                </tbody>

              </table>


                <table class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th style="width:25%;">Layanan</th>
                    <th>Harga Layanan</th>
                    <th>Total Pesan</th>
                    <th>Subtotal</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                $total = 0;
                  $data = $db->query("SELECT * FROM trorderdetail AS od INNER JOIN services AS s ON od.ServicesID=s.ServicesID WHERE od.OrderID='".$cek_order['OrderID']."'","result");
                  foreach ($data as $data_list){
                    $total += $data_list->OrderDetailSubtotal;
                ?>
                    <tr>
                      <td><?= $data_list->ServicesName; ?></td>
                      <td>Rp. <?= number_format($data_list->ServicesPrice,2,",",","); ?></td>
                      <td><?= $data_list->OrderDetailQuantity; ?></td>
                      <td>Rp. <?= number_format($data_list->ServicesPrice * $data_list->OrderDetailQuantity,2,",",","); ?></td>
                    </tr>
                <?php 
                  }
                ?>
                  <tr>
                    <td style="text-align:right;" colspan="3">Subtotal : </td>
                    <td>Rp. <?= number_format($cek_order['OrderCart'],2,",","."); ?></td>
                  </tr>
                  <tr>
                  <?php
                    if($cek_order['LocationFare']==0){
                      $city_id = $cek_order['CityID'];
                      $getPrice = $db->query("SELECT * FROM city WHERE CityID='$city_id'","row");
                      $price = $getPrice['CityFare'];
                    }else{
                      $price = $cek_order['LocationFare'];
                    }
                  ?>
                    <td style="text-align:right;" colspan="3">Ongkos : </td>
                    <td>Rp. <?= number_format($price,2,",","."); ?></td>
                  </tr>
                  <tr>
                    <td style="text-align:right;" colspan="3">Total : </td>
                    <td>Rp. <?= number_format($cek_order['OrderTotal'],2,",","."); ?></td>
                  </tr>
                </tbody>

              </table>
              <button type="submit" class="btn btn-primary btn-block">Simpan</button>
              <br><br>
            </form>
            </div>

        </div>
		</div>
		
	</div>
</div>
<?php include('../templates/admin2/script_view.php'); ?>
</body>
</html>