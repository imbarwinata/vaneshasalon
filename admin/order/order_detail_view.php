<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Ruang Administrator</title>
<?php include('../templates/admin2/css_view.php'); ?>
</head>
<body>
<div id="wrapper">
    <?php include('../templates/admin2/header_view.php'); ?>
    <div id="bodi">
        <?php include('../templates/admin2/sidebar_view.php'); ?>
        
        <div id="content1">
        	<h2 class="judul_artikel">Rincian Daftar Pesanan <small>( <?= $cek_order['OrderID']; ?> )</small></h2>
            <div class="content2" style="font-size:18px; padding-top: 20px;">
                <?php
                    if(isset($_SESSION['success_message'])){
                  ?>
                      <div class="alert alert-<?= $_SESSION['success_type']; ?> alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong><?= $_SESSION['success_parameter']; ?></strong> <?= $_SESSION['success_message']; ?>
                      </div>
                      <!-- Success Message - End -->
                  <?php
                      unset($_SESSION['success_type']);
                      unset($_SESSION['success_message']);
                      unset($_SESSION['success_parameter']);
                    }
                  ?>
              <a href="?page=order" class="btn btn-primary pull-right" data-toggle="tooltip" data-placement="left" title="Kembali"><span class="fa fa-backward"></span> Kembali</a><br><br>
                <table class="table table-bordered table-striped">
                <tbody>
                  <tr>
                    <td style="width:25%;" colspan="3">Anggota <div class="pull-right">:</div> </td>
                    <td><?= $cek_order['MemberName']; ?></td>
                  </tr>
                  <tr>
                    <td style="width:25%;" colspan="3">Nama <div class="pull-right">:</div> </td>
                    <td><?= $cek_order['OrderName']; ?></td>
                  </tr>
                  <tr>
                    <td style="width:25%;" colspan="3">Nomor Telepon / HP <div class="pull-right">:</div> </td>
                    <td><?= $cek_order['OrderPhone']; ?></td>
                  </tr>
                  <tr>
                    <td style="width:25%;" colspan="3">Alamat <div class="pull-right">:</div> </td>
                    <td><?= $cek_order['OrderAddress']; ?></td>
                  </tr>
                  <tr>
                    <td style="width:25%;" colspan="3">Status Pesanan <div class="pull-right">:</div> </td>
                    <td>
                  <?php 
                      if($cek_order['OrderStatus'] == 0){
                        echo "Pending";
                      }elseif($cek_order['OrderStatus'] == 1){
                        echo "Cancel";
                      }elseif($cek_order['OrderStatus'] == 2){
                        echo "Process";
                      }elseif($cek_order['OrderStatus'] == 2){
                        echo "Departure to destination";
                      }elseif($cek_order['OrderStatus'] == 3){
                        echo "Finish";
                      }
                    ?>
                    </td>
                  </tr>
                  <tr>
                    <td style="width:25%;" colspan="3">Tanggal Pemesanan <div class="pull-right">:</div> </td>
                    <td><?= $date->convertFormat("H:i:s",$cek_order['OrderDate'])." - ".$date->convertFormat("d M Y",$cek_order['OrderDate']); ?></td>
                  </tr>
                  <tr>
                    <td style="width:25%;" colspan="3">Konfirmasi Pembayaran <div class="pull-right">:</div> </td>
                         <td><?php 
                        $cek_konfirmasi = $db->query("SELECT * FROM confirmation WHERE OrderID='".$cek_order['OrderID']."' AND MemberID='".$cek_order['MemberID']."'","row");
                        if($cek_konfirmasi==false){
                          echo "Belum ada konfirmasi";
                        }else{
                          if($cek_konfirmasi['ConfirmationStatus']==0){
                            echo "Konfirmasi belum dicek";
                          }elseif($cek_konfirmasi['ConfirmationStatus']==1){
                            echo "Konfirmasi diterima";
                          }elseif($cek_konfirmasi['ConfirmationStatus']==2){
                            echo "Konfirmasi ditolak";

                          }
                        }
                    ?></td>
                  </tr>
                </tbody>

              </table>


                <table class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th style="width:25%;">Layanan</th>
                    <th>Harga Layanan</th>
                    <th>Total Pesan</th>
                    <th>Subtotal</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                $total = 0;
                  $data = $db->query("SELECT * FROM trorderdetail AS od INNER JOIN services AS s ON od.ServicesID=s.ServicesID WHERE od.OrderID='".$cek_order['OrderID']."'","result");
                  foreach ($data as $data_list){
                    $total += $data_list->OrderDetailSubtotal;
                ?>
                    <tr>
                      <td><?= $data_list->ServicesName; ?></td>
                      <td>Rp. <?= number_format($data_list->ServicesPrice,2,",",","); ?></td>
                      <td><?= $data_list->OrderDetailQuantity; ?></td>
                      <td>Rp. <?= number_format($data_list->ServicesPrice * $data_list->OrderDetailQuantity,2,",",","); ?></td>
                    </tr>
                <?php 
                  }
                ?>
                  <tr>
                    <td style="text-align:right;" colspan="3">Subtotal : </td>
                    <td>Rp. <?= number_format($cek_order['OrderCart'],2,",","."); ?></td>
                  </tr>
                  <tr>
                    <td style="text-align:right;" colspan="3">Ongkos : </td>
                    <td>Rp. <?= number_format($cek_order['OrderTotal']-$cek_order['OrderCart'],2,",","."); ?></td>
                  </tr>
                  <tr>
                    <td style="text-align:right;" colspan="3">Total : </td>
                    <td>Rp. <?= number_format($cek_order['OrderTotal'],2,",","."); ?></td>
                  </tr>
                </tbody>

              </table>
            </div>

        </div>
		</div>
		
	</div>
</div>
<?php include('../templates/admin2/script_view.php'); ?>
</body>
</html>