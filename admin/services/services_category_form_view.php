<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Ruang Administrator</title>
<?php include('../templates/admin2/css_view.php'); ?>
</head>
<body>
<div id="wrapper">
    <?php include('../templates/admin2/header_view.php'); ?>
    <div id="bodi">
        <?php include('../templates/admin2/sidebar_view.php'); ?>
        
        <div id="content1">
          <h2 class="judul_artikel"> 
        <?php
          if(preg_match("/add/", $_GET['page'])){
              $description = "Tambah";
          }elseif(preg_match("/edit/", $_GET['page'])){
              $description = "Perbaharui";
          }
        ?>
          <?= $description; ?> Kategori Layanan</h2>
            <div class="content2" style="font-size:18px; padding-top: 20px;">
        <?php
          /*  INSERT CONTENT  */
          if(preg_match("/add/", $_GET['page'])){
            $page_flag = "Insert";
            if($_POST){
              if($_POST['title']==NULL OR $_POST['show']==NULL OR $_POST['description'] == NULL){
                if($_POST['title']==NULL){ $validation_title = "Nama tidak boleh kosong."; }
                if($_POST['description']==NULL){ $validation_description = "Deskripsi tidak boleh kosong."; }
                if($_POST['show']==NULL){ $validation_show = "Pilih apakah konten akan ditampilkan atau tidak."; }
              }
              else{
                /* INSERT PROSES */
                $id = $generate->generate_custom_id("SC","ymd","servicescategory","ServicesCategoryID",5);
                $data['ServicesCategoryID'] = $id;
                $data['ServicesCategoryName'] = $validation->default_rules($_POST['title']);
                $data['ServicesCategoryPermalink'] = str_replace(" ", "-", $validation->default_rules($_POST['title'])).$id;
                $data['ServicesCategoryDescription'] = $validation->wysiwyg($_POST['description']);
                $data['ServicesCategoryShow'] = $validation->default_rules($_POST['show']);
                $data['ServicesCategoryDate'] = $date->getCurrentDate();
                $data['AdminID'] = $_SESSION['AdminID'];
                $query_insert = $db->insert("servicescategory",$data);
                if($query_insert==false){
                  $proccess->generateSuccessMessage("danger","services category",$_GET['id'],"failed","Insert");
                  echo "<script>window.location=('?page=category');</script>";
                }else{
                  $proccess->generateSuccessMessage("success","services category",$_GET['id'],"success","Insert");
                  echo "<script>window.location=('?page=category');</script>";
                }
              }
            }
        ?>
              <form enctype="multipart/form-data" role="form" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
                <div class="box-body">
                  <div class="form-group">
                    <label>Nama Kategori Layanan</label>
                    <input name="title" type="text" value="<?php if($_POST['title']!=NULL){ echo $_POST['title']; } ?>" class="form-control" placeholder="Nama Kategori Layanan ..">
                <?php
                  if(isset($validation_title)){ ?>
                    <em><?php echo $validation_title; ?></em>
                <?php
                  }
                ?>
                  </div>
                  
                  <div class="form-group">
                    <label for="exampleInputFile">Deskripsi</label>
                    <textarea name="description" class="form-control" id="ckeditor1"><?php if($_POST['description']!=NULL){ echo $_POST['description']; } ?></textarea>
                <?php
                  if(isset($validation_description)){ ?>
                    <em><?php echo $validation_description; ?></em>
                <?php
                  }
                ?>
                  </div>
                  
                  <div class="form-group">
                    <label>Show / Hide</label><br>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="1" <?php if($_POST['show']!=NULL){ if($_POST['show']=="1"){ echo "checked"; } } ?>/> Show
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="0" <?php if($_POST['show']!=NULL){ if($_POST['show']=="0"){ echo "checked"; } } ?>/> Hide
                    </label>
                <?php
                  if(isset($validation_show)){ ?>
                    <br>
                    <em><?php echo $validation_show; ?></em>
                <?php
                  }
                ?>
                  </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                  <button type="button" onclick="window.location=('?page=category');" class="btn btn-warning pull-right" style="margin-right:5px;">Batal</button>
                </div>
              </form>

          <?php
          }

          /*  UPDATE CONTENT  */
          elseif(preg_match("/edit/", $_GET['page'])){
            $page_flag = "Update";
            $data = $db->query("SELECT * FROM ServicesCategory WHERE ServicesCategoryID='".$_GET['id']."'","row");
            if($_POST){
              if($_POST['title']==NULL OR $_POST['show']==NULL OR $_POST['description'] == NULL){
                if($_POST['title']==NULL){ $validation_title = "Nama tidak boleh kosong."; }
                if($_POST['description']==NULL){ $validation_description = "Deskripsi tidak boleh kosong."; }
                if($_POST['show']==NULL){ $validation_show = "Pilih apakah konten akan ditampilkan atau tidak."; }
              }
              else{
                /* UPDATE PROSES */
                $id = $_GET['id'];
                $cek_data = $db->query("SELECT * FROM servicescategory WHERE ServicesCategoryID = '$id'","row");
                $data['ServicesCategoryName'] = $validation->default_rules($_POST['title']);
                $data['ServicesCategoryPermalink'] = str_replace(" ", "-", $validation->default_rules($_POST['title']))."-".$_GET['id'];
                $data['ServicesCategoryDescription'] = $validation->wysiwyg($_POST['description']);
                $data['ServicesCategoryShow'] = $validation->default_rules($_POST['show']);
                $data['ServicesCategoryDate'] = $date->getCurrentDate();
                $data['AdminID'] = $_SESSION['AdminID'];
                $where = ['ServicesCategoryID'=>$_GET['id']];
                $query_update = $db->update("ServicesCategory",$data,$where,"notlike");
                if($query_update==false){
                  $proccess->generateSuccessMessage("danger","services category",$_GET['id'],"failed","Update");
                  echo "<script>window.location=('?page=category');</script>";
                }else{
                  $proccess->generateSuccessMessage("success","services category",$_GET['id'],"success","Update");
                  echo "<script>window.location=('?page=category');</script>";
                }
              }
            }
        ?>

              <form enctype="multipart/form-data" role="form" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
                <div class="box-body">
                  <div class="box-body">
                  <div class="form-group">
                    <label>Nama Kategori Layanan</label>
                    <input name="title" type="text" value="<?php if($_POST){ if($_POST['title']!=NULL){ echo $_POST['title']; } else{echo "";} }else{ echo $data['ServicesCategoryName']; } ?>" class="form-control" placeholder="Nama Kategori Layanan ..">
                <?php
                  if(isset($validation_title)){ ?>
                    <em><?php echo $validation_title; ?></em>
                <?php
                  }
                ?>
                  </div>

                  <div class="form-group">
                    <label for="exampleInputFile">Deskripsi</label>
                    <textarea name="description" class="form-control" id="ckeditor1"><?php if($_POST){ if($_POST['description']!=NULL){ echo $_POST['description']; } else{echo "";} }else{ echo $data['ServicesCategoryDescription']; } ?></textarea>
                <?php
                  if(isset($validation_description)){ ?>
                    <em><?php echo $validation_description; ?></em>
                <?php
                  }
                ?>
                  </div>
                  
                  <div class="form-group">
                    <label>Show / Hide</label><br>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="1" <?php if($_POST['show']!=NULL){ if($_POST['show']=="0"){ echo "checked"; } }else{if($data['ServicesCategoryShow']=="1"){ echo "checked"; }}?>/> Show
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="0" <?php if($_POST['show']!=NULL){ if($_POST['show']=="0"){ echo "checked"; } }else{ if($data['ServicesCategoryShow']=="0"){ echo "checked"; } } ?>/> Hide
                    </label>
                <?php
                  if(isset($validation_show)){ ?>
                    <br>
                    <em><?php echo $validation_show; ?></em>
                <?php
                  }
                ?>
                  </div>

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                  <button type="button" onclick="window.location=('?page=category');" class="btn btn-warning pull-right" style="margin-right:5px;">Batal</button>
                </div>
              </form>
            <?php } ?>

            </div>
        </div>
    </div>
    
  </div>
</div>
<?php include('../templates/admin2/script_view.php'); ?>
</body>
</html>