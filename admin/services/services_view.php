<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Ruang Administrator</title>
<?php include('../templates/admin2/css_view.php'); ?>
</head>
<body>
<div id="wrapper">
    <?php include('../templates/admin2/header_view.php'); ?>
    <div id="bodi">
        <?php include('../templates/admin2/sidebar_view.php'); ?>
        
        <div id="content1">
        	<h2 class="judul_artikel">Daftar Layanan</h2>
            <div class="content2" style="font-size:18px; padding-top: 20px;">
                <?php
                    if(isset($_SESSION['success_message'])){
                  ?>
                      <div class="alert alert-<?= $_SESSION['success_type']; ?> alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong><?= $_SESSION['success_parameter']; ?></strong> <?= $_SESSION['success_message']; ?>
                      </div>
                      <!-- Success Message - End -->
                  <?php
                      unset($_SESSION['success_type']);
                      unset($_SESSION['success_message']);
                      unset($_SESSION['success_parameter']);
                    }
              $data_category = $db->query("SELECT * FROM servicescategory ORDER BY ServicesCategoryName ASC","result");
              if(count($data_category)==0){ ?>
              <a href="?page=category_add; ?>_add" class="btn btn-flat btn-primary pull-right" data-toggle="tooltip" data-placement="left" title="Tambah Data"><span class="fa fa-plus"></span> Tambah Data Category</a><br><br>
              <?php
              }else{ ?>
              <a href="?page=<?php echo $_GET['page']; ?>_add" class="btn btn-flat btn-primary pull-right" data-toggle="tooltip" data-placement="left" title="Tambah Data"><span class="fa fa-plus"></span> Tambah Data</a><br><br>
              <?php
              }
              ?>

                <table id="example" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Nama Layanan</th>
                    <th>Haga</th>
                    <th>Terakhir diperbaharui</th>
                    <th>Featured</th>
                    <th>Show</th>
                    <th style="width:160px;">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                  $data = $db->query("SELECT * FROM services ORDER BY ServicesName ASC","result");
                  foreach ($data as $data_list){
                ?>
                    <tr>
                      <td><?= $data_list->ServicesName; ?></td>
                      <td><?= "Rp. ".number_format($data_list->ServicesPrice,0,',','.'); ?></td>
                      <td><?= $date->time_elapsed_string($data_list->ServicesDate); ?></td>
                      <td>
                        <?php
                            if($data_list->ServicesFeatured == 1){
                        ?>
                            <span class="label label-success">Yes</span>
                        <?php
                            } else {
                        ?>
                            <span class="label label-danger">No</span>
                        <?php
                            }
                        ?>
                      </td>
                      <td>
                        <?php
                            if($data_list->ServicesShow == 1){
                        ?>
                            <span class="label label-success">Show</span>
                        <?php
                            } else {
                        ?>
                            <span class="label label-danger">Hide</span>
                        <?php
                            }
                        ?>
                      </td>
                      <td>
                      <?php
                        if($data_list->ServicesShow==0){ ?>
                        <a href="?page=<?php echo $_GET['page']; ?>_show&id=<?= $data_list->ServicesID; ?>" class="btn btn-flat btn-default btn-flat" data-toggle="tooltip" title="Show"><i class="fa fa-eye-slash"></i></a>
                      <?php
                        }else{ ?>
                        <a href="?page=<?php echo $_GET['page']; ?>_hide&id=<?= $data_list->ServicesID; ?>" class="btn btn-flat btn-primary btn-flat" data-toggle="tooltip" title="Hide"><i class="fa fa-eye"></i></a>
                      <?php
                        }
                      ?>
                        <a href="?page=<?php echo $_GET['page']; ?>_edit&id=<?= $data_list->ServicesID; ?>" class="btn btn-flat btn-warning btn-flat" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a>
                        <a onclick="if(confirm('Data will be deleted ?')){ window.location=('?page=<?php echo $_GET['page']; ?>_delete&id=<?= $data_list->ServicesID; ?>') }" class="btn btn-flat btn-danger btn-flat" data-toggle="tooltip" title="Delete"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                <?php 
                  }
                ?>
                </tbody>
              </table>
            </div>
            


        </div>
		</div>
		
	</div>
</div>
<?php include('../templates/admin2/script_view.php'); ?>
</body>
</html>