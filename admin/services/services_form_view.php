<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Ruang Administrator</title>
<?php include('../templates/admin2/css_view.php'); ?>
<style type="text/css">
  .form-group em{ font-size: 16px; font-style: italic; color: red;}
</style>
</head>
<body>
<div id="wrapper">
    <?php include('../templates/admin2/header_view.php'); ?>
    <div id="bodi">
        <?php include('../templates/admin2/sidebar_view.php'); ?>
        
        <div id="content1">
          <h2 class="judul_artikel"> 
        <?php
          if(preg_match("/add/", $_GET['page'])){
              $description = "Tambah";
          }elseif(preg_match("/edit/", $_GET['page'])){
              $description = "Perbaharui";
          }
        ?>
          <?= $description; ?> Layanan</h2>
            <div class="content2" style="font-size:18px; padding-top: 20px;">
        <?php
          /*  INSERT CONTENT  */
          if(preg_match("/add/", $_GET['page'])){
            $page_flag = "Insert";
            if($_POST){
              if($_POST['title']==NULL OR $_POST['show']==NULL OR $_POST['price']==NULL OR $_POST['featured']==NULL OR $_POST['subdescription']==NULL OR $_FILES['image']['name']==""){
                if($_POST['title']==NULL){ $validation_title = "Nama tidak boleh kosong."; }
                if($_POST['price']==NULL){ $validation_price = "Harga tidak boleh kosong."; }
                if($_POST['subdescription']==NULL){ $validation_subdescription = "Deskripsi tidak boleh kosong."; }
                if($_POST['show']==NULL){ $validation_show = "Pilih apakah konten akan ditampilkan atau tidak."; }
                if($_POST['featured']==NULL){ $validation_show = "Pilih apakah konten akan ditampilkan dihalaman home atau tidak."; }
                if($_POST['category']==NULL){ $validation_category = "Kategori tidak boleh kosong."; }
                if($_FILES['image']['name']==""){ $validation_image = "Image tidak boleh kosong."; }
              }
              else{
                /* INSERT PROSES */
                $id = $generate->generate_custom_id("S","ymd","services","ServicesID",5);
                $data['ServicesID'] = $id;
                $data['ServicesName'] = $validation->default_rules($_POST['title']);
                $data['ServicesPrice'] = $validation->default_rules($_POST['price']);
                $data['ServicesDescription'] = $validation->default_rules($_POST['subdescription']);
                $data['ServicesShow'] = $validation->default_rules($_POST['show']);
                $data['ServicesFeatured'] = $validation->default_rules($_POST['featured']);
                $data['ServicesDate'] = $date->getCurrentDate();
                $data['ServicesCategoryID'] = $validation->default_rules($_POST['category']);
                if($_FILES['image']['name'] != NULL){
                  $temp = explode(".", $_FILES["image"]["name"]);
                  $newfilename = $id . '.' . end($temp);
                  $data['ServicesImage'] = $newfilename;
                  copy($_FILES ["image"]["tmp_name"], "../img/services/".$data['ServicesImage']);
                }
                $data['AdminID'] = $_SESSION['AdminID'];
                $query_insert = $db->insert("services",$data);
                if($query_insert==false){
                  $proccess->generateSuccessMessage("danger","services",$_GET['id'],"failed","Insert");
                  echo "<script>window.location=('?page=services');</script>";
                }else{
                  $proccess->generateSuccessMessage("success","services",$_GET['id'],"success","Insert");
                  echo "<script>window.location=('?page=services');</script>";
                }
              }
            }
        ?>
          <?php
            $getCategory = $db->query("SELECT * FROM servicescategory WHERE ServicesCategoryShow = '1' ORDER BY ServicesCategoryName ASC","result");
          ?>
              <form enctype="multipart/form-data" role="form" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
                <div class="box-body">
                  <div class="form-group">
                    <label>Pilih Kategori</label>
                    <select name="category" class="form-control">
                      <option value="">--pilih--</option>
                  <?php
                  foreach ($getCategory as $data_category){ ?>
                      <option value="<?= $data_category->ServicesCategoryID; ?>" <?php if($_POST['category']!=NULL){ if($_POST['category']==$data_category->ServicesCategoryID){ echo "selected"; } } ?>><?= $data_category->ServicesCategoryName; ?></option>
                  <?php
                  }
                  ?>
                    </select>
                <?php
                  if(isset($validation_category)){ ?>
                    <em><?php echo $validation_category; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <hr>
                  <div class="form-group">
                    <label>Nama Layanan</label>
                    <input name="title" type="text" value="<?php if($_POST['title']!=NULL){ echo $_POST['title']; } ?>" class="form-control" placeholder="Nama layanan ..">
                <?php
                  if(isset($validation_title)){ ?>
                    <em><?php echo $validation_title; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                    <label>Harga Layanan</label>
                    <input name="price" type="number" min="0" value="<?php if($_POST['price']!=NULL){ echo $_POST['price']; } ?>" class="form-control">
                <?php
                  if(isset($validation_price)){ ?>
                    <em><?php echo $validation_price; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                      <label>Deskripsi</label>
                      <textarea class="form-control" rows="5" name="subdescription"><?php if($_POST['subdescription']!=NULL){ echo $_POST['subdescription']; } ?></textarea>
                <?php
                  if(isset($validation_subdescription)){ ?>
                    <em><?php echo $validation_subdescription; ?></em>
                <?php
                  }
                ?>
                  </div>
                  
                 <div class="form-group">
                      <label id="label_image">Gambar</label>
                      <div class="fileupload fileupload-new" data-provides="fileupload">
                          <div class="fileupload-new thumbnail thumbnail-o-upload">
                          <?php
                              if($page_flag == "Update"){
                                  if($data['ServicesImage'] == ''){
                          ?>
                              <img src="<?php echo "http://" . $_SERVER['SERVER_NAME']; ?>/vaneshasalon/img/default/noimage.png"/>
                          <?php
                                  } else {
                          ?>
                                  <img src="<?= "../img/services/".$data['ServicesImage'] ?>"/>
                          <?php
                                  }
                              } else {
                          ?>
                              <img src="<?php echo "http://" . $_SERVER['SERVER_NAME']; ?>/vaneshasalon/img/default/noimage.png"/>
                          <?php
                              }
                          ?>
                          </div>
                          <div class="fileupload-preview fileupload-exists thumbnail thumbnail-o-upload"></div>
                          <div>
                              <span class="btn btn-default btn-file">
                              <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Pilih gambar</span>
                              <span class="fileupload-exists"><i class="fa fa-undo"></i> Ubah</span>
                              <input type="file" class="default" name="image" onchange="checkImage('image')" accept="image/*"/>
                              </span>
                          </div>
                          <p class="help-block" id="error_image_photo"></p>
                        <em><?php echo $validation_image; ?></em>
                      </div>
                  </div>
                  
                  <div class="form-group">
                    <label>Featured</label><br>
                    <label class="radio-inline">
                      <input type="radio" name="featured" value="1" <?php if($_POST['featured']!=NULL){ if($_POST['featured']=="1"){ echo "checked"; } } ?>/> Yes
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="featured" value="0" <?php if($_POST['featured']!=NULL){ if($_POST['featured']=="0"){ echo "checked"; } } ?>/> No
                    </label>
                <?php
                  if(isset($validation_featured)){ ?>
                    <br>
                    <em><?php echo $validation_featured; ?></em>
                <?php
                  }
                ?>
                  </div>

                  <div class="form-group">
                    <label>Show / Hide</label><br>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="1" <?php if($_POST['show']!=NULL){ if($_POST['show']=="1"){ echo "checked"; } } ?>/> Show
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="0" <?php if($_POST['show']!=NULL){ if($_POST['show']=="0"){ echo "checked"; } } ?>/> Hide
                    </label>
                <?php
                  if(isset($validation_show)){ ?>
                    <br>
                    <em><?php echo $validation_show; ?></em>
                <?php
                  }
                ?>
                  </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                  <button type="button" onclick="window.location=('?page=services');" class="btn btn-warning pull-right" style="margin-right:5px;">Batal</button>
                </div>
              </form>

          <?php
          }

          /*  UPDATE CONTENT  */
          elseif(preg_match("/edit/", $_GET['page'])){
            $page_flag = "Update";
            $data = $db->query("SELECT * FROM services WHERE ServicesID='".$_GET['id']."'","row");
            if($_POST){
              if($_POST['title']==NULL OR $_POST['show']==NULL OR $_POST['price']==NULL OR $_POST['featured']==NULL OR $_POST['subdescription']==NULL){
                if($_POST['title']==NULL){ $validation_title = "Nama tidak boleh kosong."; }
                if($_POST['price']==NULL){ $validation_price = "Harga tidak boleh kosong."; }
                if($_POST['subdescription']==NULL){ $validation_subdescription = "Deskripsi tidak boleh kosong."; }
                if($_POST['show']==NULL){ $validation_show = "Pilih apakah konten akan ditampilkan atau tidak."; }
                if($_POST['featured']==NULL){ $validation_featured = "Pilih apakah konten akan ditampilkan dihalaman home atau tidak."; }
                if($_POST['category']==NULL){ $validation_category = "Kategori tidak boleh kosong."; }
              }
              else{
                /* UPDATE PROSES */
                $id = $_GET['id'];
                $cek_data = $db->query("SELECT * FROM services WHERE ServicesID = '$id'","row");
                $data['ServicesName'] = $validation->default_rules($_POST['title']);
                $data['ServicesPrice'] = $validation->default_rules($_POST['price']);
                $data['ServicesDescription'] = $validation->default_rules($_POST['subdescription']);
                $data['ServicesShow'] = $validation->default_rules($_POST['show']);
                $data['ServicesFeatured'] = $validation->default_rules($_POST['featured']);
                $data['ServicesDate'] = $date->getCurrentDate();
                $data['ServicesCategoryID'] = $validation->default_rules($_POST['category']);
                $data['AdminID'] = $_SESSION['AdminID'];
                $where = ['ServicesID'=>$_GET['id']];
                if($_FILES['image']['name'] != NULL){
                  unlink("../img/services/".$cek_data['ServicesImage']);
                  $data['ServicesImage'] = $_FILES['image']['name'];
                }
                copy($_FILES ["image"]["tmp_name"], "../img/services/".$_FILES['image']['name']);
                $query_update = $db->update("services",$data,$where,"notlike");
                if($query_update==false){
                  $proccess->generateSuccessMessage("danger","services",$_GET['id'],"failed","Update");
                  echo "<script>window.location=('?page=services');</script>";
                }else{
                  $proccess->generateSuccessMessage("success","services",$_GET['id'],"success","Update");
                  echo "<script>window.location=('?page=services');</script>";
                }
              }
            }
        ?>
        <?php
            $getCategory = $db->query("SELECT * FROM servicescategory ORDER BY servicescategoryName ASC","result");
        ?>
              <form enctype="multipart/form-data" role="form" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
                <div class="box-body">
                  <div class="box-body">
                  <div class="form-group">
                    <label>Pilih Kategori</label>
                    <select name="category" class="form-control">
                      <option value="">--pilih--</option>
                  <?php
                  foreach ($getCategory as $data_category){ ?>
                      <option value="<?= $data_category->ServicesCategoryID; ?>" <?php if($_POST){ if($_POST['category']!=NULL){ if($_POST['category']==$data_category->ServicesCategoryID){ echo "selected"; } } else{echo "";} }else{ if($data_category->ServicesCategoryID==$data['ServicesCategoryID']){ echo "selected"; } } ?>><?= $data_category->ServicesCategoryName; ?></option>
                  <?php
                  }
                  ?>
                    </select>
                <?php
                  if(isset($validation_category)){ ?>
                    <em><?php echo $validation_category; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <hr>

                  <div class="form-group">
                    <label>Nama Layanan</label>
                    <input name="title" type="text" value="<?php if($_POST){ if($_POST['title']!=NULL){ echo $_POST['title']; } else{echo "";} }else{ echo $data['ServicesName']; } ?>" class="form-control" placeholder="Nama layanan ..">
                <?php
                  if(isset($validation_title)){ ?>
                    <em><?php echo $validation_title; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                    <label>Harga Layanan</label>
                    <input name="price" type="number" min="0" value="<?php if($_POST){ if($_POST['price']!=NULL){ echo $_POST['price']; } else{echo "";} }else{ echo $data['ServicesPrice']; } ?>" class="form-control">
                <?php
                  if(isset($validation_price)){ ?>
                    <em><?php echo $validation_price; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                      <label>Deskripsi</label>
                      <textarea class="form-control" rows="5" name="subdescription"><?php if($_POST){ if($_POST['subdescription']!=NULL){ echo $_POST['subdescription']; } else{echo "";} }else{ echo $data['ServicesDescription']; } ?></textarea>
                <?php
                  if(isset($validation_subdescription)){ ?>
                    <em><?php echo $validation_subdescription; ?></em>
                <?php
                  }
                ?>
                  </div>
                  
                 <div class="form-group">
                      <label id="label_image">Gambar</label>
                      <div class="fileupload fileupload-new" data-provides="fileupload">
                          <div class="fileupload-new thumbnail thumbnail-o-upload">
                          <?php
                              if($page_flag == "Update"){
                                  if($data['ServicesImage'] == ''){
                          ?>
                              <img src="<?php echo "http://" . $_SERVER['SERVER_NAME']; ?>/vaneshasalon/img/default/noimage.png"/>
                          <?php
                                  } else {
                          ?>
                                  <img src="<?= "../img/services/".$data['ServicesImage'] ?>"/>
                          <?php
                                  }
                              } else {
                          ?>
                              <img src="<?php echo "http://" . $_SERVER['SERVER_NAME']; ?>/vaneshasalon/img/default/noimage.png"/>
                          <?php
                              }
                          ?>
                          </div>
                          <div class="fileupload-preview fileupload-exists thumbnail thumbnail-o-upload"></div>
                          <div>
                              <span class="btn btn-default btn-file">
                              <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Pilih gambar</span>
                              <span class="fileupload-exists"><i class="fa fa-undo"></i> Ubah</span>
                              <input type="file" class="default" name="image" onchange="checkImage('image')" accept="image/*"/>
                              </span>
                          </div>
                          <p class="help-block" id="error_image_photo"></p>
                      </div>
                  </div>

                  <div class="form-group">
                    <label>Featured</label><br>
                    <label class="radio-inline">
                      <input type="radio" name="featured" value="1" <?php if($_POST['featured']!=NULL){ if($_POST['featured']=="0"){ echo "checked"; } }else{if($data['ServicesFeatured']=="1"){ echo "checked"; }}?>/> Yes
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="featured" value="0" <?php if($_POST['featured']!=NULL){ if($_POST['featured']=="0"){ echo "checked"; } }else{ if($data['ServicesFeatured']=="0"){ echo "checked"; } } ?>/> No
                    </label>
                <?php
                  if(isset($validation_featured)){ ?>
                    <br>
                    <em><?php echo $validation_featured; ?></em>
                <?php
                  }
                ?>
                  </div>

                  <div class="form-group">
                    <label>Show / Hide</label><br>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="1" <?php if($_POST['show']!=NULL){ if($_POST['show']=="0"){ echo "checked"; } }else{if($data['ServicesShow']=="1"){ echo "checked"; }}?>/> Show
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="0" <?php if($_POST['show']!=NULL){ if($_POST['show']=="0"){ echo "checked"; } }else{ if($data['ServicesShow']=="0"){ echo "checked"; } } ?>/> Hide
                    </label>
                <?php
                  if(isset($validation_show)){ ?>
                    <br>
                    <em><?php echo $validation_show; ?></em>
                <?php
                  }
                ?>
                  </div>

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                  <button type="button" onclick="window.location=('?page=services');" class="btn btn-warning pull-right" style="margin-right:5px;">Batal</button>
                </div>
              </form>
            <?php } ?>

            </div>
        </div>
    </div>
    
  </div>
</div>
<?php include('../templates/admin2/script_view.php'); ?>
</body>
</html>