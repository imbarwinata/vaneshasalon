<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Ruang Administrator</title>
<?php include('../templates/admin2/css_view.php'); ?>
</head>
<body>
<div id="wrapper">
    <?php include('../templates/admin2/header_view.php'); ?>
    <div id="bodi">
        <?php include('../templates/admin2/sidebar_view.php'); ?>
        
        <div id="content1">
          <h2 class="judul_artikel"> 
        <?php
          if(preg_match("/add/", $_GET['page'])){
              $description = "Tambah";
          }elseif(preg_match("/edit/", $_GET['page'])){
              $description = "Perbaharui";
          }
        ?>
          <?= $description; ?> Social Media Link</h2>
            <div class="content2" style="font-size:18px; padding-top: 20px;">
        <?php
          /*  INSERT CONTENT  */
          if(preg_match("/add/", $_GET['page'])){
            $page_flag = "Insert";
            if($_POST){
              if($_POST['title']==NULL OR $_POST['show']==NULL){
                if($_POST['title']==NULL){ $validation_title = "The title can not be empty."; }
                if($_POST['link']==NULL){ $validation_link = "The link can not be empty."; }
                if($_POST['icon']==NULL){ $validation_icon = "The icon can not be empty."; }
                if($_POST['show']==NULL){ $validation_show = "The show can not be empty."; }
              }
              else{
                /* INSERT PROSES */
                $id = $generate->generate_custom_id("SML","ymd","contentpoint","ContentPointID",5);
                $data['ContentPointID'] = $id;
                $data['ContentPointLabel'] = "Social Media Link";
                $data['ContentPointTitle'] = $validation->default_rules($_POST['title']);
                $data['ContentPointSubDescription'] = $validation->default_rules($_POST['link']);
                $data['ContentPointIcon'] = $validation->wysiwyg($_POST['icon']);
                $data['ContentPointShow'] = $validation->default_rules($_POST['show']);
                $data['ContentPointDate'] = $date->getCurrentDate();
                $data['AdminID'] = $_SESSION['AdminID'];
                $query_insert = $db->insert("contentpoint",$data);
                if($query_insert==false){
                  $proccess->generateSuccessMessage("danger","Social Media Link",$id,"failed","Insert");
                  echo "<script>window.location=('?page=social_media_link');</script>";
                }else{
                  $proccess->generateSuccessMessage("success","Social Media Link",$id,"success","Insert");
                  echo "<script>window.location=('?page=social_media_link');</script>";
                }
              }
            }
        ?>
              <form enctype="multipart/form-data" role="form" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
                <div class="box-body">
                  <div class="form-group">
                    <label>Nama Social Media</label>
                    <input name="title" type="text" value="<?php if($_POST['title']!=NULL){ echo $_POST['title']; } ?>" class="form-control" placeholder="Social Media Name..">
                <?php
                  if(isset($validation_title)){ ?>
                    <em><?php echo $validation_title; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                    <label>Link</label>
                    <input name="link" type="url" value="<?php if($_POST['link']!=NULL){ echo $_POST['link']; } ?>" class="form-control" placeholder="Social Media Link..">
                <?php
                  if(isset($validation_link)){ ?>
                    <em><?php echo $validation_link; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputFile">Icon</label>
                    <input type="text" name="icon" class="form-control icp icp-auto icon_picker" value="<?php if($_POST['icon']!=NULL){ echo $_POST['icon']; } ?>">
                <?php
                  if(isset($validation_icon)){ ?>
                    <em><?php echo $validation_icon; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                    <label>Show / Hide</label><br>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="1" <?php if($_POST['show']!=NULL){ if($_POST['show']=="1"){ echo "checked"; } } ?>/> Show
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="0" <?php if($_POST['show']!=NULL){ if($_POST['show']=="0"){ echo "checked"; } } ?>/> Hide
                    </label>
                <?php
                  if(isset($validation_show)){ ?>
                    <br>
                    <em><?php echo $validation_show; ?></em>
                <?php
                  }
                ?>
                  </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                  <button type="button" onclick="window.location=('?page=social_media_link');" class="btn btn-warning pull-right" style="margin-right:5px;">Batal</button>
                </div>
              </form>

          <?php
          }

          /*  UPDATE CONTENT  */
          elseif(preg_match("/edit/", $_GET['page'])){
            $page_flag = "Update";
            $data = $db->query("SELECT * FROM contentpoint WHERE ContentPointID='".$_GET['id']."'","row");
            if($_POST){
              if($_POST['title']==NULL OR $_POST['show']==NULL){
                if($_POST['title']==NULL){ $validation_title = "The title can not be empty."; }
                if($_POST['link']==NULL){ $validation_link = "The link can not be empty."; }
                if($_POST['icon']==NULL){ $validation_icon = "The icon can not be empty."; }
                if($_POST['show']==NULL){ $validation_show = "The show can not be empty."; }
              }
              else{
                /* UPDATE PROSES */
                $id = $_GET['id'];
                $cek_data = $db->query("SELECT * FROM contentpoint WHERE ContentPointID = '$id'","row");
                $data['ContentPointTitle'] = $validation->default_rules($_POST['title']);
                $data['ContentPointSubDescription'] = $validation->default_rules($_POST['link']);
                $data['ContentPointDescription'] = $validation->wysiwyg($_POST['icon']);
                $data['ContentPointShow'] = $validation->default_rules($_POST['show']);
                $data['ContentPointShow'] = $validation->default_rules($_POST['show']);
                $data['ContentPointDate'] = $date->getCurrentDate();
                $data['AdminID'] = $_SESSION['AdminID'];
                $where = ['ContentPointID'=>$_GET['id']];
                $query_update = $db->update("contentpoint",$data,$where,"notlike");
                if($query_update==false){
                  $proccess->generateSuccessMessage("danger","Social Media Link",$_GET['id'],"failed","Update");
                  echo "<script>window.location=('?page=social_media_link');</script>";
                }else{
                  $proccess->generateSuccessMessage("success","Social Media Link",$_GET['id'],"success","Update");
                  echo "<script>window.location=('?page=social_media_link');</script>";
                }
              }
            }
        ?>

              <form enctype="multipart/form-data" role="form" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
                <div class="box-body">
                  <div class="box-body">
                  <div class="form-group">
                    <label>Nama Social Media</label>
                    <input name="title" type="text" value="<?php if($_POST){ if($_POST['title']!=NULL){ echo $_POST['title']; } else{echo "";} }else{ echo $data['ContentPointTitle']; } ?>" class="form-control" placeholder="Social Media Name ..">
                <?php
                  if(isset($validation_title)){ ?>
                    <em><?php echo $validation_title; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                    <label>Link</label>
                    <input name="link" type="url" value="<?php if($_POST){ if($_POST['link']!=NULL){ echo $_POST['link']; } else{echo "";} }else{ echo $data['ContentPointSubDescription']; } ?>" class="form-control" placeholder="Link Social Media ..">
                <?php
                  if(isset($validation_link)){ ?>
                    <em><?php echo $validation_link; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputFile">Icon</label>
                    <input type="text" name="icon" class="form-control icp icp-auto icon_picker" value="<?php if($_POST){ if($_POST['icon']!=NULL){ echo $_POST['icon']; } else{echo "";} }else{ echo $data['ContentPointIcon']; } ?>">
                <?php
                  if(isset($validation_icon)){ ?>
                    <em><?php echo $validation_icon; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                    <label>Show / Hide</label><br>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="1" <?php if($_POST['show']!=NULL){ if($_POST['show']=="0"){ echo "checked"; } }else{if($data['ContentPointShow']=="1"){ echo "checked"; }}?>/> Show
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="0" <?php if($_POST['show']!=NULL){ if($_POST['show']=="0"){ echo "checked"; } }else{ if($data['ContentPointShow']=="0"){ echo "checked"; } } ?>/> Hide
                    </label>
                <?php
                  if(isset($validation_show)){ ?>
                    <br>
                    <em><?php echo $validation_show; ?></em>
                <?php
                  }
                ?>
                  </div>

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                  <button type="button" onclick="window.location=('?page=social_media_link');" class="btn btn-warning pull-right" style="margin-right:5px;">Batal</button>
                </div>
              </form>
            <?php } ?>

            </div>
        </div>
    </div>
    
  </div>
</div>
<?php include('../templates/admin2/script_view.php'); ?>
</body>
</html>