<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Ruang Administrator</title>
<?php include('../templates/admin2/css_view.php'); ?>
</head>
<body>
<div id="wrapper">
    <?php include('../templates/admin2/header_view.php'); ?>
    <div id="bodi">
        <?php include('../templates/admin2/sidebar_view.php'); ?>
        
        <div id="content1">
        	<h2 class="judul_artikel">Daftar Pelanggan</h2>
            <div class="content2" style="font-size:18px; padding-top: 20px;">
                <?php
                    if(isset($_SESSION['success_message'])){
                  ?>
                      <div class="alert alert-<?= $_SESSION['success_type']; ?> alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong><?= $_SESSION['success_parameter']; ?></strong> <?= $_SESSION['success_message']; ?>
                      </div>
                      <!-- Success Message - End -->
                  <?php
                      unset($_SESSION['success_type']);
                      unset($_SESSION['success_message']);
                      unset($_SESSION['success_parameter']);
                    }
                  ?>
            
                <table id="example" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Nomor</th>
                    <th>Lokasi</th>
                    <th>Status</th>
                    <th style="width:110px;">Action</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                  $data = $db->query("SELECT * FROM member AS m LEFT JOIN location AS l ON m.LocationID=l.LocationID ORDER BY MemberName ASC","result");
                  foreach ($data as $data_list){
                ?>
                    <tr>
                      <td><?= $data_list->MemberName; ?></td>
                      <td><?= $data_list->MemberEmail; ?></td>
                      <td><?= $data_list->MemberPhone;; ?></td>
                      <td><?= $data_list->LocationTitle==""? $data_list->MemberLocation:$data_list->LocationTitle; ?></td>
                      <td>
                        <?php
                            if($data_list->MemberActive == 1){
                        ?>
                            <span class="label label-success">Aktif</span>
                        <?php
                            } else {
                        ?>
                            <span class="label label-danger">Nonaktif</span>
                        <?php
                            }
                        ?>
                      </td>
                      <td>
                        <?php
                        if($data_list->MemberActive==0){ ?>
                        <a href="?page=<?php echo $_GET['page']; ?>_active&id=<?= $data_list->MemberID; ?>" class="btn btn-default btn-flat" data-toggle="tooltip" title="Aktif"><i class="fa fa-close"></i></a>
                      <?php
                        }else{ ?>
                        <a href="?page=<?php echo $_GET['page']; ?>_nonactive&id=<?= $data_list->MemberID; ?>" class="btn btn-primary btn-flat" data-toggle="tooltip" title="Nonaktif"><i class="fa fa-check"></i></a>
                      <?php
                        }
                      ?>
                      </td>
                    </tr>
                <?php 
                  }
                ?>
                </tbody>
              </table>
            </div>
            


        </div>
		</div>
		
	</div>
</div>
<?php include('../templates/admin2/script_view.php'); ?>
</body>
</html>