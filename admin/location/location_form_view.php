<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Ruang Administrator</title>
<?php include('../templates/admin2/css_view.php'); ?>
<style type="text/css">
  .form-group em{ font-size: 16px; font-style: italic; color: #d26a5c;}
</style>
</head>
<body>
<div id="wrapper">
    <?php include('../templates/admin2/header_view.php'); ?>
    <div id="bodi">
        <?php include('../templates/admin2/sidebar_view.php'); ?>
        
        <div id="content1">
          <h2 class="judul_artikel"> 
        <?php
          if(preg_match("/add/", $_GET['page'])){
              $description = "Tambah";
          }elseif(preg_match("/edit/", $_GET['page'])){
              $description = "Perbaharui";
          }
        ?>
          <?= $description; ?> Lokasi</h2>
            <div class="content2" style="font-size:18px; padding-top: 20px;">
        <?php
          $getCity = $db->query("SELECT * FROM city WHERE CityShow = '1' ORDER BY CityID ASC","result");
          /*  INSERT CONTENT  */
          if(preg_match("/add/", $_GET['page'])){
            if($_POST){
              if($_POST['title']==NULL OR $_POST['show']==NULL OR $_POST['fare']==NULL OR $_POST['pos']==NULL OR $_POST['city']==NULL){
                if($_POST['title']==NULL){ $validation_title = "Judul tidak boleh kosong."; }
                if($_POST['pos']==NULL){ $validation_pos = "Kode pos tidak boleh kosong."; }
                if($_POST['fare']==NULL){ $validation_fare = "Ongkos tidak boleh kosong."; }
                if($_POST['city']==NULL){ $validation_city = "Kota tidak boleh kosong."; }
                if($_POST['show']==NULL){ $validation_show = "Pilih apakah konten akan ditampilkan atau tidak."; }
              }
              else{
                /* INSERT PROSES */
                $id = $generate->generate_custom_id("L","ymd","location","LocationID",3);
                $data['LocationID'] = $id;
                $data['LocationTitle'] = $validation->default_rules($_POST['title']);
                $data['LocationKodePos'] = $validation->default_rules($_POST['pos']);
                $data['LocationFare'] = $validation->default_rules($_POST['fare']);
                $data['LocationShow'] = $validation->default_rules($_POST['show']);                
                $data['CityID'] = $validation->default_rules($_POST['city']);                
                $data['LocationDate'] = $date->getCurrentDate();
                $data['AdminID'] = $_SESSION['admin_id'];
                $query_insert = $db->insert("location",$data);  /* Parameter 1 ( nama tabel : contoh tabel location), parameter ke 2 data yang akan disimpan */
                
                if($query_insert==false){
                  $_SESSION['success_type'] = "danger"; /* succes,info,warning,danger */
                  $_SESSION['success_message'] = "Insert location $id failed.";
                  $_SESSION['success_parameter'] = "Insert failed.";
                  echo "<script>window.location=('?page=location');</script>";
                }else{
                  $_SESSION['success_type'] = "success"; /* succes,info,warning,danger */
                  $_SESSION['success_message'] = "Insert location $id success.";
                  $_SESSION['success_parameter'] = "Insert success.";
                  echo "<script>window.location=('?page=location');</script>";
                }
              }
            }
        ?>
              <form role="form" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
                <div class="box-body">
                  <div class="form-group">
                    <label>Kota</label>
                    <select name="city" class="form-control">
                      <option value="">--pilih--</option>
                  <?php
                  foreach ($getCity as $data_city){ ?>
                      <option value="<?= $data_city->CityID; ?>" <?php if($_POST['city']!=NULL){ if($_POST['city']==$data_city->CityID){ echo "selected"; } } ?>><?= $data_city->CityName; ?></option>
                  <?php
                  }
                  ?>
                    </select>
                <?php
                  if(isset($validation_city)){ ?>
                    <em><?php echo $validation_city; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                    <label>Lokasi</label>
                    <input name="title" type="text" value="<?php if($_POST['title']!=NULL){ echo $_POST['title']; } ?>" class="form-control" placeholder="Nama Lokasi ..">
                <?php
                  if(isset($validation_title)){ ?>
                    <em><?php echo $validation_title; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                    <label>Kode Pos</label>
                    <input name="pos" type="text" value="<?php if($_POST['pos']!=NULL){ echo $_POST['pos']; } ?>" class="form-control" placeholder="Kode Pos ..">
                <?php
                  if(isset($validation_pos)){ ?>
                    <em><?php echo $validation_pos; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                    <label>Ongkos</label>
                    <div class="input-group">
                      <label class="input-group-addon">Rp. </label>
                      <input name="fare" type="text" value="<?php if($_POST['fare']!=NULL){ echo $_POST['fare']; } ?>" class="form-control" placeholder="Ongkos ..">
                    </div>
                <?php
                  if(isset($validation_pos)){ ?>
                    <em><?php echo $validation_fare; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                    <label>Show / Hide</label><br>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="1" <?php if($_POST['show']!=NULL){ if($_POST['show']=="1"){ echo "checked"; } } ?>/> Show
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="0" <?php if($_POST['show']!=NULL){ if($_POST['show']=="0"){ echo "checked"; } } ?>/> Hide
                    </label>
                <?php
                  if(isset($validation_show)){ ?>
                    <br>
                    <em><?php echo $validation_show; ?></em>
                <?php
                  }
                ?>
                  </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                  <button type="button" onclick="window.location=('?page=location');" class="btn btn-warning pull-right" style="margin-right:5px;">Batal</button>
                </div>
              </form>

          <?php
          }

          /*  UPDATE CONTENT  */
          elseif(preg_match("/edit/", $_GET['page'])){
            $data = $db->query("SELECT * FROM location WHERE LocationID='".$_GET['id']."'","row");
            if($_POST){
              if($_POST['title']==NULL OR $_POST['show']==NULL OR $_POST['fare']==NULL OR $_POST['pos']==NULL OR $_POST['city']==NULL){
                if($_POST['title']==NULL){ $validation_title = "Judul tidak boleh kosong."; }
                if($_POST['pos']==NULL){ $validation_pos = "Kode pos tidak boleh kosong."; }
                if($_POST['fare']==NULL){ $validation_fare = "Ongkos tidak boleh kosong."; }
                if($_POST['city']==NULL){ $validation_city = "Kota tidak boleh kosong."; }
                if($_POST['show']==NULL){ $validation_show = "Pilih apakah konten akan ditampilkan atau tidak."; }
              }
              else{
                /* UPDATE PROSES */
                $data['LocationTitle'] = $validation->default_rules($_POST['title']);
                $data['LocationKodePos'] = $validation->default_rules($_POST['pos']);
                $data['LocationFare'] = $validation->default_rules($_POST['fare']);
                $data['LocationShow'] = $validation->default_rules($_POST['show']);
                $data['CityID'] = $validation->default_rules($_POST['city']);
                $data['AdminID'] = $_SESSION['AdminID'];
                $data['LocationDate'] = $date->getCurrentDate();
                $where = ['LocationID'=>$_GET['id']];
                $query_update = $db->update("location",$data,$where,"notlike");
                if($query_update==false){
                  $_SESSION['success_type'] = "danger"; /* succes,info,warning,danger */
                  $_SESSION['success_message'] = "Update location ".$_GET['id']." failed.";
                  $_SESSION['success_parameter'] = "Update failed.";
                  echo "<script>window.location=('?page=location');</script>";
                }else{
                  $_SESSION['success_type'] = "success"; /* succes,info,warning,danger */
                  $_SESSION['success_message'] = "Update location ".$_GET['id']." success.";
                  $_SESSION['success_parameter'] = "Update success.";
                  echo "<script>window.location=('?page=location');</script>";
                }
              }
            }
        ?>

              <form role="form" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
                <div class="box-body">
                  <div class="form-group">
                    <label>Kota</label>
                    <select name="city" class="form-control">
                      <option value="">--pilih--</option>
                  <?php
                  foreach ($getCity as $data_city){ ?>
                      <option value="<?= $data_city->CityID; ?>" <?php if($_POST){ if($_POST['city']!=NULL){ if($_POST['city']==$data_city->CityID){ echo "selected"; } } else{echo "";} }else{ if($data_city->CityID==$data['CityID']){ echo "selected"; } } ?>><?= $data_city->CityName; ?></option>
                  <?php
                  }
                  ?>
                    </select>
                <?php
                  if(isset($validation_city)){ ?>
                    <em><?php echo $validation_city; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <hr>
                  <div class="form-group">
                    <label>Lokasi</label>
                    <input name="title" type="text" value="<?php if($_POST){ if($_POST['title']!=NULL){ echo $_POST['title']; } else{echo "";} }else{ echo $data['LocationTitle']; } ?>" class="form-control" placeholder="Nama Lokasi ..">
                <?php
                  if(isset($validation_title)){ ?>
                    <em><?php echo $validation_title; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                    <label>Kode Pos</label>
                    <input name="pos" type="text" value="<?php if($_POST){ if($_POST['pos']!=NULL){ echo $_POST['pos']; } else{echo "";} }else{ echo $data['LocationKodePos']; } ?>" class="form-control" placeholder="Kode Pos ..">
                <?php
                  if(isset($validation_pos)){ ?>
                    <em><?php echo $validation_pos; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                    <label>Ongkos</label>
                    <input name="fare" type="number" step="1000" value="<?php if($_POST){ if($_POST['fare']!=NULL){ echo $_POST['fare']; } else{echo "";} }else{ echo $data['LocationFare']; } ?>" class="form-control" placeholder="Ongkos ..">
                <?php
                  if(isset($validation_pos)){ ?>
                    <em><?php echo $validation_fare; ?></em>
                <?php
                  }
                ?>
                  </div>
                  <div class="form-group">
                    <label>Show / Hide</label><br>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="1" <?php if($_POST['show']!=NULL){ if($_POST['show']=="0"){ echo "checked"; } }else{if($data['LocationShow']=="1"){ echo "checked"; }}?>/> Show
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="show" value="0" <?php if($_POST['show']!=NULL){ if($_POST['show']=="0"){ echo "checked"; } }else{ if($data['LocationShow']=="0"){ echo "checked"; } } ?>/> Hide
                    </label>
                <?php
                  if(isset($validation_show)){ ?>
                    <br>
                    <em><?php echo $validation_show; ?></em>
                <?php
                  }
                ?>
                  </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                  <button type="button" onclick="window.location=('?page=location');" class="btn btn-warning pull-right" style="margin-right:5px;">Batal</button>
                </div>
              </form>
            <?php } ?>

            </div>
        </div>
    </div>
    
  </div>
</div>
<?php include('../templates/admin2/script_view.php'); ?>
</body>
</html>