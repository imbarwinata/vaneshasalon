<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Ruang Administrator</title>
<?php include('../templates/admin2/css_view.php'); ?>
</head>
<body>
<div id="wrapper">
    <?php include('../templates/admin2/header_view.php'); ?>
    <div id="bodi">
        <?php include('../templates/admin2/sidebar_view.php'); ?>
        
        <div id="content1">
        	<h2 class="judul_artikel">Daftar Lokasi</h2>
            <div class="content2" style="font-size:18px; padding-top: 20px;">
                <?php
                    if(isset($_SESSION['success_message'])){
                  ?>
                      <div class="alert alert-<?= $_SESSION['success_type']; ?> alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong><?= $_SESSION['success_parameter']; ?></strong> <?= $_SESSION['success_message']; ?>
                      </div>
                      <!-- Success Message - End -->
                  <?php
                      unset($_SESSION['success_type']);
                      unset($_SESSION['success_message']);
                      unset($_SESSION['success_parameter']);
                    }
                $cek_city = $db->query("SELECT * FROM city","result");
                if($cek_city==false){
                  ?>
              <a href="?page=city_add" class="btn btn-primary pull-right" data-toggle="tooltip" data-placement="left" title="Tambah Data"><span class="fa fa-plus"></span> Tambah Data Kota</a><br><br>
                <?php
                }else{ ?>
              <a href="?page=<?php echo $_GET['page']; ?>_add" class="btn btn-primary pull-right" data-toggle="tooltip" data-placement="left" title="Tambah Data"><span class="fa fa-plus"></span> Tambah Data</a><br><br>
              <?php
                }
              ?>
                <table id="example" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Lokasi</th>
                    <th>Ongkos</th>
                    <th>Terakhir Diperbaharui</th>
                    <th>Show</th>
                    <th style="width:110px;">Action</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                  $data = $db->query("SELECT * FROM location ORDER BY LocationTitle ASC","result");
                  foreach ($data as $data_list){
                ?>
                    <tr>
                      <td><?= $data_list->LocationTitle; ?></td>
                      <td><?= "Rp. ".number_format($data_list->LocationFare,0,",","."); ?></td>
                      <td><?= $date->time_elapsed_string($data_list->LocationDate); ?></td>
                      <td>
                        <?php
                            if($data_list->LocationShow == 1){
                        ?>
                            <span class="label label-success">Show</span>
                        <?php
                            } else {
                        ?>
                            <span class="label label-danger">Hide</span>
                        <?php
                            }
                        ?>
                      </td>
                      <td>
                        <a href="?page=<?php echo $_GET['page']; ?>_edit&id=<?= $data_list->LocationID; ?>" class="btn btn-warning btn-flat" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a>
                        <a onclick="if(confirm('Data will be deleted ?')){ window.location=('?page=<?php echo $_GET['page']; ?>_delete&id=<?= $data_list->LocationID; ?>') }" class="btn btn-danger btn-flat" data-toggle="tooltip" title="Delete"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                <?php 
                  }
                ?>
                </tbody>
              </table>
            </div>
            


        </div>
		</div>
		
	</div>
</div>
<?php include('../templates/admin2/script_view.php'); ?>
</body>
</html>