<?php
error_reporting(0);
session_start();
include_once('../config/koneksi.php');
include('fungsi.php');
if(cek_login($mysqli) == true){
    header('location: home.php?page=status');
    exit(); 
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Vanesha Salon</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../assets/plugins/bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <script src="../assets/plugins/jQuery/jQuery-2.2.0.min.js"></script>
    <script src="../assets/plugins/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../assets/plugins/font-awesome-4.7.0/css/font-awesome.min.css">
    <?php include('../assets/css/login.css'); ?>
</head>
<body>
<?php
    $pesan = "";
    if($_POST){
        if($_POST['username']==NULL || $_POST['password']==NULL){
            if($_POST['username']!=NULL){ $validation_username = $_POST['username'];}
            if($_POST['password']!=NULL){ $validation_password = $_POST['password'];}
            if($_POST['password']==NULL && $_POST['username']==NULL){ $pesan = "Enter  Username & Password!";}

        }
        else{
            $username = $_POST['username'];
            $password = $_POST['password'];
            if(login($username, $password, $mysqli) == true){
                // Berhasil login
               header('location: home.php?page=status');
               exit();
            }else{
                $validation_username = $_POST['username'];
                $validation_password = $_POST['password'];
                $validation_cek = "Username dan Password tidak sama.";
                // Gagal login
#                header('location: login.php');
#                exit(); 
            }
        }
    }
?>
<div class="container">
	<div class="login-container">
            <div id="output"></div>
            <div class="avatar"><i class="fa fa-user-secret" style="font-size:60px; margin-top:15px;"></i></div>
            <div class="form-box">
                <form action="<?php $_SERVER["PHP_SELF"] ?>" method="post">
                    <input name="username" type="text" placeholder="username" value="<?php if($validation_username!=NULL){ echo $validation_username; } ?>">
                    <input type="password" placeholder="password" name="password" value="<?php if($validation_password!=NULL){ echo $validation_password; } ?>">
                    <button style="margin-bottom:10px;" class="btn btn-info btn-block login" type="submit" value="<?php if($validation_password!=NULL){ echo $validation_password; } ?>">Login</button>
            <?= $pesan!="" ? "<label style='color:red;'>$pesan</label>":""; ?>
            <?php
                if($validation_username==""){ 
                    if($_POST['password']){ ?>
                    <label style="color:red;">Enter Username !</label>
            <?php   }
                } ?>
            <?php
                if($validation_password==""){ 
                    if($_POST['username']){ ?>
                    <label style="color:red;">Enter Password !</label>
            <?php   }
                } ?>
            <?php
                if(isset($validation_cek)){ ?>
                    <label style="color:red;"><?= $validation_cek; ?></label>
            <?php
                } ?>                
                </form>
            </div>
        </div>
        
</div>
<?php /*include('../assets/js/login.js');*/ ?>
</body>
</html>
