<?php
error_reporting(0);
session_start();
include_once('../config/koneksi.php');
include_once('../model/query_model.php');
include_once('../model/validation_model.php');
include_once('../model/date_model.php');
include_once('../model/generate_model.php');
include_once('../model/proccess_model.php');
include_once('fungsi.php');
$db = new query_model();
$db->path = "../"; // Setting root configurasi config database
$validation = new validation_model();
$date = new date_model();
$generate = new generate_model();
$generate->path = "../";
$proccess = new proccess_model();

/* Cek login */
if(cek_login($mysqli) == false){ // Jika user tidak login
	header('location: login.php'); // Alihkan ke halaman login (index.php)
	exit();	
}
$stmt = $mysqli->prepare("SELECT AdminUserName FROM admin WHERE AdminID = ?");
$stmt->bind_param('i', $_SESSION['AdminID']);
$stmt->execute();
$stmt->store_result();
$stmt->bind_result($username);
$stmt->fetch();
/* Cek login - End */

/* FAQ */
if($_GET['page']=="faq"){ 
	include "faq/faq_view.php"; 
}
elseif($_GET['page']=="faq_add"){ 
	include "faq/faq_form_view.php"; 
}
elseif($_GET['page']=="faq_edit"){ 
	include "faq/faq_form_view.php";
}
elseif($_GET['page']=="faq_show"){ 
	$data['ContentPointShow'] = 1;
    $where = ['ContentPointID'=>$_GET['id']];
	$query_update = $db->update("contentpoint",$data,$where,"notlike");
	$proccess->generateSuccessMessage("success","faq",$_GET['id'],"success","Show");
	header('Location: ?page=faq');
}
elseif($_GET['page']=="faq_hide"){
	$data['ContentPointShow'] = 0;
    $where = ['ContentPointID'=>$_GET['id']];
	$query_update = $db->update("contentpoint",$data,$where,"notlike");
	$proccess->generateSuccessMessage("success","faq",$_GET['id'],"success","Hide");
	header('Location: ?page=faq');
}
elseif($_GET['page']=="faq_delete"){
	if($_GET['id']!=NULL){
		$where = ['ContentPointID'=>$_GET['id']];
		$delete = $db->delete("contentpoint",$where,"notlike");
		if($delete==true){ 
	        $proccess->generateSuccessMessage("success","faq",$_GET['id'],"success","Delete");
            header('Location: ?page=faq');
		}
		else{ 
			$proccess->generateSuccessMessage("danger","faq",$_GET['id'],"failed","Delete");
			header('Location: ?page=faq');
		}
	}
	else{
		header('Location: ?page=faq');
	}
}
/* FAQ - end */

/* Social Media Link */
elseif($_GET['page']=="social_media_link"){ 
	include "social_media_link/social_media_link_view.php"; 
}
elseif($_GET['page']=="social_media_link_add"){ 
	include "social_media_link/social_media_link_form_view.php"; 
}
elseif($_GET['page']=="social_media_link_edit"){ 
	include "social_media_link/social_media_link_form_view.php";
}
elseif($_GET['page']=="social_media_link_show"){ 
	$data['ContentPointShow'] = 1;
    $where = ['ContentPointID'=>$_GET['id']];
	$query_update = $db->update("contentpoint",$data,$where,"notlike");
	$proccess->generateSuccessMessage("success","social media link",$_GET['id'],"success","Show");
	header('Location: ?page=social_media_link');
}
elseif($_GET['page']=="social_media_link_hide"){
	$data['ContentPointShow'] = 0;
    $where = ['ContentPointID'=>$_GET['id']];
	$query_update = $db->update("contentpoint",$data,$where,"notlike");
	$proccess->generateSuccessMessage("success","social media link",$_GET['id'],"success","Hide");
	header('Location: ?page=social_media_link');
}
elseif($_GET['page']=="social_media_link_delete"){
	if($_GET['id']!=NULL){
		$where = ['ContentPointID'=>$_GET['id']];
		$delete = $db->delete("contentpoint",$where,"notlike");
		if($delete==true){ 
	        $proccess->generateSuccessMessage("success","social media link",$_GET['id'],"success","Delete");
            header('Location: ?page=social_media_link');
		}
		else{ 
			$proccess->generateSuccessMessage("danger","social media link",$_GET['id'],"failed","Delete");
			header('Location: ?page=social_media_link');
		}
	}
	else{
		header('Location: ?page=social_media_link');
	}
}
/* Social Media Link - end */

/* Privacy and Policy */
elseif($_GET['page']=="privacy_policy"){
	include "content/privacy_policy_view.php";
}
/* Privacy and Policy-End */

/* Terms and Condition */
elseif($_GET['page']=="terms_and_condition"){
	include "content/terms_and_condition_view.php";
}
/* Terms and Condition-End */

/* Contact Us */
elseif($_GET['page']=="contact_us"){
	include "content/contact_us_view.php";
}
/* Contact Us-End */

/* Logo */
elseif($_GET['page']=="logo"){
	include "content/logo_view.php";
}
/* Logo-End */
else{ header('HTTP/1.0 404 Not Found', true, 404); }
?>