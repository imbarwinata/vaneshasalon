<?php
class date_model{
	function __construct(){
		date_default_timezone_set("Asia/Jakarta");
	}
	function getCurrentDate(){
		$date = date("Y-m-d H:i:s");
		return $date;
	}
	function getDate(){
		$date = date("Y-m-d");
		return $date;
	}
	function getMonth(){
		$date = date("m");
		return $date;
	}
	function getYear(){
		$date = date("Y");
		return $date;
	}
	function getTime(){
		$date = date("H:i:s");
		return $date;
	}

	function getDay($type = "", $input_date = ""){
		if($input_date != ""){
			if($type == "full"){
				$date = date("l", strtotime($input_date));
			} else if($type == "number"){
				$date = date("N", strtotime($input_date));
			} else {
				$date = date("D", strtotime($input_date));
			}
		} else {
			if($type == "full"){
				$date = date("l");
			} else if($type == "number"){
				$date = date("N");
			} else {
				$date = date("D");
			}
		}

		return $date;
	}

	function convertFormat($format = "", $input_date = ""){
		return date($format, strtotime($input_date));
	}

	function time_elapsed_string($datetime, $full = false) {
	    $now = new DateTime;
	    $ago = new DateTime($datetime);
	    $diff = $now->diff($ago);

	    $diff->w = floor($diff->d / 7);
	    $diff->d -= $diff->w * 7;

	    $string = array(
	        'y' => 'year',
	        'm' => 'month',
	        'w' => 'week',
	        'd' => 'day',
	        'h' => 'hour',
	        'i' => 'minute',
	        's' => 'second',
	    );
	    foreach ($string as $k => &$v) {
	        if ($diff->$k) {
	            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
	        } else {
	            unset($string[$k]);
	        }
	    }

	    if (!$full) $string = array_slice($string, 0, 1);
	    return $string ? implode(', ', $string) . ' ago' : 'just now';
	}
}
?>