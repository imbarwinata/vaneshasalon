<?php
function koneksi(){
	if (!defined('HOST')) define('HOST', 'localhost');
	if (!defined('USER')) define('USER', 'root');
	if (!defined('PASSWORD')) define('PASSWORD', '');
	if (!defined('DATABASE')) define('DATABASE', 'vaneshasalon');
	$mysqli = new mysqli(HOST, USER, PASSWORD, DATABASE);
	if($mysqli->connect_error){
		trigger_error('Koneksi ke database gagal: ' . $mysqli->connect_error, E_USER_ERROR);	
	}
	return $mysqli;
}
function query($query="",$type="") {
	$db = koneksi();
	$results = array();
	if($query!=""){
		if($type=="result"){
			$result = $db->query($query);
			while($row = $result->fetch_object()){ // fetch_object() / fetch_array(MYSQLI_BOTH|MYSQLI_NUM|MYSQLI_ASSOC)
				$results[] = $row;
			}	
			return $results;
		}
		elseif($type="row"){
			$test = $db->query($query);
			return $test->fetch_array(MYSQLI_ASSOC); // fetch_object() / fetch_array(MYSQLI_BOTH|MYSQLI_NUM|MYSQLI_ASSOC)	
		}
	}
	else{
		echo "enter query!!!";
	}
}


if($_POST['type'] == "getLocation"){
	$location = query("SELECT * FROM location WHERE CityID='".$_POST['city_id']."'","result");
	if($location==true){
			echo "<optgroup label='--pilih--'>";
		foreach ($location as $list_location):
			echo "<option value='$list_location->LocationID'>$list_location->LocationTitle</option>";
		endforeach;
			echo "<option value='0'>Lokasi lain.</option>";
			echo "</optgroup>";
	}else{
		echo "<optgroup label='--pilih--'>";
		echo "<option value='0'>Lokasi lain.</option>";
		echo "</optgroup>";
	}
}
?>