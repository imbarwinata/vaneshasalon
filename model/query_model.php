<?php
class query_model{
	public $path = "";
	protected function connect(){
		include($this->path.'config/koneksi.php');
		//include('../config/koneksi.php');
		return $mysqli;
#		return new mysqli('localhost', 'root', '', 'portalberita');
	}
	public function query($query="",$type="") {
		$db = $this->connect();
		$results = array();
		if($query!=""){
			if($type=="result"){
				$result = $db->query($query);
				while($row = $result->fetch_object()){ // fetch_object() / fetch_array(MYSQLI_BOTH|MYSQLI_NUM|MYSQLI_ASSOC)
					$results[] = $row;
				}	
				return $results;
			}
			elseif($type="row"){
				$test = $db->query($query);
				return $test->fetch_array(MYSQLI_ASSOC); // fetch_object() / fetch_array(MYSQLI_BOTH|MYSQLI_NUM|MYSQLI_ASSOC)	
			}
		}
		else{
			echo "enter query!!!";
		}
	}
	
	public function insert($table="", $data="") {
		if(empty($table) || empty($data)){
			return false;
		}
		$db = $this->connect();
		foreach($data as $key => $value){
			$field[] = $key;
			$values[] = $value;
		}
		$field = implode(", ",$field);
		$value = "'".implode("','",$values)."'";
		$simpan = $db->query("insert into ".$table." (".$field.") values (".$value.")");
		if($simpan==true){
			return true;
		}
		else{
			return false;
		}
	}

	public function update($table="",$data="",$where="",$type=""){
		if(empty($table) || empty($data) || empty($where) || empty($type)){
			return false;
		}
		$db = $this->connect();
		foreach($data as $key => $value){
			$field[] = $key."='".$value."'";
		}
		foreach($where as $key1 => $value1){
			if($type=="notlike"){
				$where_data[] = $key1."='".$value1."'";
			}
			elseif($type=="like"){
				$where_data[] = $key1." LIKE '%".$value1."%'";
			}
		}
		$field = implode(", ",$field);
		$where_data = implode(", ",$where_data);
		
		if($type=="like"){
			$update = $db->query("update ".$table." set ".$field." where ".$where_data." ");
		}
		elseif($type=="notlike"){
			$update = $db->query("update ".$table." set ".$field." where ".$where_data." ");
		}
		if($update==true){
			return true;
		}
		else{
			return false;
		}
	}

	public function delete($table="",$where="",$type=""){
		if(empty($table) || empty($where) || empty($type)){
			return false;
		}
		$db = $this->connect();
		foreach($where as $key => $value){
			if($type=="notlike"){
				$where_data[] = $key."='".$value."'";
			}
			elseif($type=="like"){
				$where_data[] = $key."LIKE '%".$value."%'";
			}
		}	
		$where_data = implode(", ",$where_data);	
		$delete = $db->query("delete from ".$table." where ".$where_data."");
		if(mysqli_affected_rows($db)){
			return true;
		}
		else{
			return false;
		}
	}

	/* This night & file model & generate model */
	function max($select="",$table=""){
		$db = $this->connect();
		if(empty($select) || empty($table)){
			header("HTTP/1.0 404 Not Found");
			echo "<h1>404 Not Found</h1>";
			echo "The page that you have requested could not be found.";
			exit();
		}
		$query = $db->query("SELECT MAX(".$select.") AS ".$select." FROM ".$table);
		return $query->fetch_object();
	}
	function maxIncrement($select="",$table=""){
		$db = $this->connect();
		if(empty($select) || empty($table)){
			header("HTTP/1.0 404 Not Found");
			echo "<h1>404 Not Found</h1>";
			echo "The page that you have requested could not be found.";
			exit();
		}
		$query = $db->query("SELECT MAX(".$select.") AS ".$select." FROM ".$table);
		$data = $query->fetch_object();
		$increment = ++$data->$select;
		return $increment;
	}

	function countAll($query="",$type=""){
		$db = $this->connect();
		if(empty($query) || empty($type) || !isset($query) || !isset($type)){
			header("HTTP/1.0 404 Not Found");
			echo "<h1>404 Not Found</h1>";
			echo "The page that you have requested could not be found.";
			exit();
		}
		$count = $this->query($query,$type);
		$count = count($count);
		return $count;
	}
}
?>