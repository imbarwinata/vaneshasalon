<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Vanesha Salon</title><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<?php include('templates/user/css_view.php'); ?>
</head>
<body>
<?php include('templates/user/navbar_view.php'); ?>

<div class="body">
	<div class="space-30"></div>
	<div class="block-main container">
		<div class="row">
			<ol class="breadcrumb hidden-content">
				<li><a href="?link=index">Home</a></li>
				<li><a href="#" class="current">F.A.Q</a></li>
			</ol>
		</div>
	</div>
		

	<div class="block-main container container-fix">
		<div class="row">
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
<?php
    $data = $db->query("SELECT * FROM contentpoint WHERE ContentPointLabel = 'FAQ' AND ContentPointShow = '1' ORDER BY ContentPointTitle ","result");
    if($data!=false){
        $i=0;
        foreach ($data as $list_data):
            if($i==0){ ?>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#<?= $list_data->ContentPointPermalink; ?>" aria-expanded="true" aria-controls="<?= $list_data->ContentPointPermalink; ?>">
                                <?= $list_data->ContentPointTitle; ?>
                            </a>
                        </h4>
                    </div>
                    <div id="<?= $list_data->ContentPointPermalink; ?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <?= $list_data->ContentPointDescription; ?>
                        </div>
                    </div>
                </div>
<?php
            }else{ ?>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#<?= $list_data->ContentPointPermalink; ?>" aria-expanded="false" aria-controls="<?= $list_data->ContentPointPermalink; ?>" class="collapsed">
                                <?= $list_data->ContentPointTitle; ?>
                            </a>
                        </h4>
                    </div>
                    <div id="<?= $list_data->ContentPointPermalink; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                        <div class="panel-body">
                            <?= $list_data->ContentPointDescription; ?>
                        </div>
                    </div>
                </div>
<?php
            }
            $i++;
        endforeach;
    }
?>
            </div>
		</div>
	</div>
	

	<?php include('templates/user/footer_view.php'); ?>
</div>

<?php include('templates/user/script_view.php'); ?>
<script type="text/javascript" src="design/js/jquery-1.12.0.min.js"></script>
<script src="design/js//bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

<link href="design/plugins/bxslider/jquery.bxslider.css" rel="stylesheet" />
<!-- Testimonial -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
<script>
	$(document).ready(function(){
    $("#testimonial-slider").owlCarousel({
        items:1,
        itemsDesktop:[1199,1],
        itemsDesktopSmall:[979,1],
        itemsTablet:[768,1],
        pagination: false,
        navigation:true,
        navigationText:["",""],
        autoPlay:true
    });
});
</script>
<!-- Testimonial -end -->
<style>
.bx-viewport{width: 100%; border: 1px solid red;}
.bx-wrapper .bx-prev {left: -45px;}
.bx-wrapper .bx-next {right: -35px;}
@media screen and (max-width: 768px) {
	.services-content{ 
		background-size: 95% 100%, 100% 100%;
	}
</style>
</body>
</html>