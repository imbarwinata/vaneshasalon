<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Vanesha Salon</title><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<?php include('templates/user/css_view.php'); ?>
</head>
<body>
<?php include('templates/user/navbar_view.php'); ?>

<div class="body">
	<div class="space-30"></div>
	<div class="block-main container">
		<div class="row">
			<ol class="breadcrumb hidden-content">
				<li><a href="?link=index">Home</a></li>
				<li><a href="#" class="current">Layanan</a></li>
			</ol>
		</div>
	</div>
	
<!--	<section class="services-banner" style="background-image: url(design/img/castadiva-spa-wellness-01.jpg) url(design/img/layer-services.png);">
		<div class="services-banner-image" style="background-position:0px 0px; background-image: url(design/img/castadiva-spa-wellness-01.jpg);"></div>
	</section>-->
	
	<div id="banner2" style="background-position:0px 0px; background-image:url(design/img/castadiva-spa-wellness-01.jpg)">
		<h2></h2>
	</div>
	<div class="services-content" style="background-image: url(design/img/layer-services2.png), url(design/img/background-services2.png);
background-repeat: no-repeat, no-repeat;
background-position: right 10vw, 0px 10vw;">
		<div class="services-description">
			<h2><?= $cek_category['ServicesCategoryName']; ?></h2>
			<p><?= $cek_category['ServicesCategoryDescription']; ?></p>
		</div>
<?php
if($services!=false){
	foreach ($services as $list_services): ?>
		<div class="space-60"></div>
		<form class="services-item" method="post" action="?link=cart">
			<div class="services-item-left" style="background-image: url(img/services/<?= $list_services->ServicesImage ?>);">
			</div>
			<div class="services-item-right">
				<h3><?= $list_services->ServicesName; ?></h3>
				<p>
				<?= $list_services->ServicesDescription; ?></p>
				<label>Rp. <?= number_format($list_services->ServicesPrice,2,",","."); ?></label>
		<?php
			if(!isset($_SESSION['MemberID'])){ ?>
				<input name="services" value="<?= $list_services->ServicesID; ?>" hidden="hidden">
				<button class="btn btn-flat btn-default pull-right" onClick="alert('Silahkan login dahulu!')" disabled="disabled">Pesan sekarang</button>
		<?php
			}else{ 
				if($status['StatusActive']==0){ ?>
				<input name="services" value="<?= $list_services->ServicesID; ?>" hidden="hidden">
				<button class="btn btn-flat btn-default pull-right" onclick="alert('Maaf salon untuk sementara ditutup.')" type="button">Pesan sekarang</button>
		<?php
				}elseif($status['StatusActive']==1){ ?>
				<input name="services" value="<?= $list_services->ServicesID; ?>" hidden="hidden">
				<button class="btn btn-flat btn-default pull-right" type="submit">Pesan sekarang</button>
		<?php
				}elseif($status['StatusActive']==2){ ?>
				<input name="services" value="<?= $list_services->ServicesID; ?>" hidden="hidden">
				<button class="btn btn-flat btn-default pull-right" onclick="alert('Maaf pegawai kami sedang sibuk, silahkan anda mencoba beberapa saat lagi.')" type="button">Pesan sekarang</button>
		<?php
				}?>
		<?php
			}
		?>
			</div>
		</form>
<?php
	endforeach;
}
?>		
		<div class="space-30"></div>
	</div>

	<?php include('templates/user/footer_view.php'); ?>
</div>

<?php include('templates/user/script_view.php'); ?>
<script src="design/plugins/bxslider/jquery.bxslider.min.js"></script>
<link href="design/plugins/bxslider/jquery.bxslider.css" rel="stylesheet" /><script>
$(document).ready(function(){
  $('.slider1').bxSlider({
    slideWidth: 600,
    slideHeight: 200,
    minSlides: 3,
    maxSlides: 3,
    moveSlides: 1,
    slideMargin: 5
  });
});
</script>
<!-- Testimonial -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
<script>
	$(document).ready(function(){
    $("#testimonial-slider").owlCarousel({
        items:1,
        itemsDesktop:[1199,1],
        itemsDesktopSmall:[979,1],
        itemsTablet:[768,1],
        pagination: false,
        navigation:true,
        navigationText:["",""],
        autoPlay:true
    });
});
</script>
<!-- Testimonial -end -->
<style>
.bx-viewport{width: 100%; border: 1px solid red;}
.bx-wrapper .bx-prev {left: -45px;}
.bx-wrapper .bx-next {right: -35px;}
@media screen and (max-width: 768px) {
	.services-content{ 
		background-size: 95% 100%, 100% 100%;
	}
</style>
</body>
</html>