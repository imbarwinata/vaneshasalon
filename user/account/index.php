<?php
if(isset($_SESSION['MemberID'])){
    header('location: ?link=my_account');
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Vanesha Salon</title><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<?php include('templates/user/css_view.php'); ?>
</head>
<body>
<?php include('templates/user/navbar_view.php'); ?>

<div class="body">
	<div class="space-30"></div>
	<div class="block-main container">
		<div class="row">
			<ol class="breadcrumb hidden-content">
				<li><a href="index.php">Home</a></li>
				<li><a href="#" class="current">Masuk</a></li>
			</ol>
		</div>
	</div>
		

	<div class="block-main container container-fix">
		<div class="row">
			<!-- MY ACCOUNT -->
<?php
include('fungsi.php');
$validation_email = "";
$validation_password = "";
    if($_POST){
        if($_POST['email']==NULL || $_POST['password']==NULL){
            if($_POST['email']!=NULL){ $validation_email = $_POST['email'];}
            if($_POST['password']!=NULL){ $validation_password = $_POST['password'];}
        }
        else{
            $email = $_POST['email'];
            $password = $_POST['password'];
            if(login($email, $password, $mysqli) == true){
                // Berhasil login
                echo "<script>alert('Berhasil masuk, selamat datang ".$_SESSION['MemberName'].".');</script>";
                echo "<script>window.location=('index.php');</script>";
            }else{
                $validation_email = $_POST['email'];
                $validation_password = $_POST['password'];
                $validation_cek = "Username dan Password tidak sama.";
                // Gagal login
            }
        }
    }
?>
<div class="account-wrap">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-6">
                <!-- HTML -->
                <div id="account-id">
                    <h4 class="account-title"><span class="fa fa-chevron-right"></span>Masuk</h4>
                    <div class="account-form">
                        <form method="post" action="?link=account">
							<div class="form-group">
								<label for="email">Email</label>
								<input type="email" class="form-control" value="<?php if($validation_email!=NULL){ echo $validation_email; } ?>" name="email" style="border-radius: 0px;">
							</div>
                            <?php
                                if($_POST){
                                    if($_POST['email']==""){  ?>
                                    <em style="color:#d26a5c; position:relative;">Masukan Email !</em><br><br>
                            <?php 
                                    }
                                } ?>
                       		<div class="form-group">
								<label for="email">Password</label>
								<input type="password" class="form-control" value="<?php if($validation_password!=NULL){ echo $validation_password; } ?>" name="password" style="border-radius: 0px;">
							</div>
                            <?php
                                if($_POST){
                                    if($_POST['password']==""){  ?>
                                    <em style="color:#d26a5c; position:relative;">Masukan Password !</em><br><br>
                            <?php 
                                    }
                                } ?>
							<button type="submit" class="btn btn-primary btn-white-purple" style="border-radius: 0px; padding: 6px 50px;">
								Masuk
							</button>
            <?php
                if(isset($validation_cek)){ ?>
                    <br><br><em style="color:#d26a5c; position:relative;"><?= $validation_cek; ?></em>
            <?php
                } ?>         
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-md-6">
                <!-- HTML -->
                <div id="account-id2">
                    <h4 class="account-title"><span class="fa fa-chevron-right"></span>Buat Akun Baru</h4>
                    <div class="account-form create-new-account">
                        <img width="100%" src="design/img/register.png" style="margin-bottom: 10px;">
                            <button class="btn btn-default btn-white-purple" type="button" onclick="window.location='?link=register'" style="border-radius: 0px;">
                            <span>Buat Akun</span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



		</div>
	</div>
	

	<?php include('templates/user/footer_view.php'); ?>
</div>

<?php include('templates/user/script_view.php'); ?>
<script type="text/javascript" src="design/js/jquery-1.12.0.min.js"></script>
<script src="design/js//bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

<link href="design/plugins/bxslider/jquery.bxslider.css" rel="stylesheet" />
<!-- Testimonial -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
<script>
	$(document).ready(function(){
    $("#testimonial-slider").owlCarousel({
        items:1,
        itemsDesktop:[1199,1],
        itemsDesktopSmall:[979,1],
        itemsTablet:[768,1],
        pagination: false,
        navigation:true,
        navigationText:["",""],
        autoPlay:true
    });
});
</script>
<!-- Testimonial -end -->
<style>
.bx-viewport{width: 100%; border: 1px solid red;}
.bx-wrapper .bx-prev {left: -45px;}
.bx-wrapper .bx-next {right: -35px;}
@media screen and (max-width: 768px) {
	.services-content{ 
		background-size: 95% 100%, 100% 100%;
	}
</style>
</body>
</html>