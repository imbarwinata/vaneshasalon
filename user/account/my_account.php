<?php
if($_POST){
    if($_POST['nama']==NULL OR $_POST['telp']==NULL OR $_POST['location']==NULL OR $_POST['city']==NULL OR $_POST['address']==NULL){
      if($_POST['nama']==NULL){ $validation_nama = "Nama tidak boleh kosong."; }
      if($_POST['telp']==NULL){ $validation_telp = "Nomor telepon tidak boleh kosong."; }
      if($_POST['address']==NULL){ $validation_address = "Alamat tidak boleh kosong."; }
      if($_POST['location']==NULL){ $validation_location = "Lokasi tidak boleh kosong."; }
    }else{
      	  	$query['MemberName'] = $_POST['nama'];
	      	$query['MemberPhone'] = $_POST['telp'];
	      	$query['MemberAddress'] = $_POST['address'];
	      	$query['LocationID'] = $_POST['location'];
	      	$query['CityID'] = $_POST['city'];
	      	if($query['LocationID']=="0"){
		      	$query['MemberLocation'] = $_POST['location_other'];	      		
	      	}else{
		      	$query['MemberLocation'] = "";
	      	}
	      	$where = ['MemberEmail' => $_SESSION['MemberEmail']];
	      	$query_update = $db->update('member',$query,$where,"notlike");
	      	if($query_update==true){
	      		echo "<script>alert('Pembaruan berhasil.');</script>";
				echo "<script>window.location=('index.php');</script>";
	      	}else{
	      		echo "<script>alert('Pembaruan gagal, silahkan coba lagi.');</script>";
				echo "<script>window.location=('index.php');</script>";      		
	      	}
    }
  }
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Vanesha Salon</title><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<?php include('templates/user/css_view.php'); ?>
<style type="text/css">
	.form-error{ color: #d26a5c; position: relative; top: 2px; }
</style>
</head>
<body>
<?php include('templates/user/navbar_view.php'); ?>

<div class="body">
	<div class="space-30"></div>
	<div class="block-main container">
		<div class="row">
			<ol class="breadcrumb hidden-content">
				<li><a href="index.php">Home</a></li>
				<li><a href="?link=my_account">Akun</a></li>
				<li><a href="#" class="current">Akun Saya</a></li>
			</ol>
		</div>
	</div>
		

	<div class="block-main container container-fix">
		<div class="row">
			<!-- MY ACCOUNT -->
			<div class="account-wrap">
				<div class="container">
					<div class="row">
						<div class="col-sm-8 col-md-8">
							<!-- HTML -->
							<div id="account-id">
								<h4 class="account-title"><span class="fa fa-chevron-right"></span>Ubah Informasi</h4>
								<div class="account-form">
									<form method="post">
										<div class="form-group">
											<label for="email">Nama *</label>
											<input value="<?php if($_POST){ if($_POST['nama']!=NULL){ echo $_POST['nama']; } else{echo "";} }else{ echo $my_account['MemberName']; } ?>" type="text" class="form-control" name="nama" style="border-radius: 0px;">
										 <?php
						                    if(isset($validation_nama)){ ?>
						                      <em class="form-error"><?php echo $validation_nama; ?></em>
						                  <?php
						                    }
						                  ?>
										</div>
										<div class="form-group">
											<label for="email">Nomor Telepon *</label>
											<input value="<?php if($_POST){ if($_POST['telp']!=NULL){ echo $_POST['telp']; } else{echo "";} }else{ echo $my_account['MemberPhone']; } ?>" type="number" class="form-control" name="telp" style="border-radius: 0px;">
										 <?php
						                    if(isset($validation_telp)){ ?>
						                      <em class="form-error"><?php echo $validation_telp; ?></em>
						                  <?php
						                    }
						                  ?>
										</div>
										<div class="form-group">
											<label for="email">Kota *</label>
											<select id="city" onchange="changeLocation()" style="width:100%;" name="city" class="js-select2 form-control">
						                      <option value="">--pilih--</option>
						                  <?php
						                  foreach ($getCity as $data_city){ ?>
						                      <option value="<?= $data_city->CityID; ?>" <?php if($_POST){ if($_POST['city']!=NULL){ if($_POST['city']==$data_city->CityID){ echo "selected"; } } else{echo "";} }else{ if($data_city->CityID==$my_account['CityID']){ echo "selected"; } } ?>><?= $data_city->CityName; ?></option>
						                  <?php
						                  }
						                  ?>
						                    </select>
										 <?php
						                    if(isset($validation_location)){ ?>
						                      <em class="form-error"><?php echo $validation_location; ?></em>
						                  <?php
						                    }
						                  ?>
										</div>
										<div class="form-group">
											<label for="email">Lokasi *</label>
											<select id="location" onchange="changeLocationOther()" style="width:100%;" name="location" class="js-select2 form-control">
						                      <option value="">--pilih--</option>
						                  <?php
						                  foreach ($getLocation as $data_location){ ?>
						                      <option value="<?= $data_location->LocationID; ?>" <?php if($_POST){ if($_POST['location']!=NULL){ if($_POST['location']==$data_location->LocationID){ echo "selected"; } } else{echo "";} }else{ if($data_location->LocationID==$my_account['LocationID']){ echo "selected"; } } ?>><?= $data_location->LocationTitle; ?></option>
						                  <?php
						                  }
						                  ?>
												<option value="0" <?= $my_account['LocationID']== "0" ? "selected":""; ?>>Lokasi lain.</option>
						                    </select>
										 <?php
						                    if(isset($validation_location)){ ?>
						                      <em class="form-error"><?php echo $validation_location; ?></em>
						                  <?php
						                    }
						                  ?>
										</div>
										<div class="form-group" id="location_other">
											<label>Tambah Lokasi Lain *</label>
											<input maxlength="50" value="<?php if($_POST){ if($_POST['location_other']!=NULL){ echo $_POST['location_other']; } else{echo "";} }else{ echo $my_account['MemberLocation']; } ?>" type="text" class="form-control" name="location_other" style="border-radius: 0px;">
										 <?php
						                    if(isset($validation_location_other)){ ?>
						                      <em class="form-error"><?php echo $validation_location_other; ?></em>
						                  <?php
						                    }
						                  ?>
										</div>
										<div class="form-group">
					                      <label>Alamat</label>
					                      <textarea class="form-control" rows="5" name="address"><?php if($_POST){ if($_POST['address']!=NULL){ echo $_POST['address']; } else{echo "";} }else{ echo $my_account['MemberAddress']; } ?></textarea>
						                <?php
						                  if(isset($validation_address)){ ?>
						                    <em class="form-error"><?php echo $validation_address; ?></em>
						                <?php
						                  }
						                ?>
					                  </div>
										<button type="submit" class="btn btn-primary btn-white-purple" style="border-radius: 0px; padding: 6px 50px;">
											Perbaharui 

										</button>

									</form>
								</div>
							</div>
						</div>

						<div class="col-sm-4 col-md-4">
							<!-- HTML -->
							<div id="account-id2">
								<h3 class="account-title" style="margin-bottom: 20px; margin-top: 15px;">Akun</h3>
								<div class="account">
									<ul>
										<li>
											<a href="#" class="current"><i class="fa fa-user"></i> &nbsp; Akun Saya</a>
										</li>
										<li>
											<a href="?link=change_password"><i class="fa fa-lock"></i> &nbsp; Ganti Kata Sandi</a>
										</li>
										<li>
											<a href="?link=history_order"><i class="fa fa-book"></i> &nbsp; Daftar Pesanan</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="space-60"></div>

	<?php include('templates/user/footer_view.php'); ?>
</div>

<?php include('templates/user/script_view.php'); ?>
<script type="text/javascript" src="design/js/jquery-1.12.0.min.js"></script>
<script src="design/js//bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

<link href="design/plugins/bxslider/jquery.bxslider.css" rel="stylesheet" />
<!-- Testimonial -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
<script>
	$(document).ready(function(){
	    var location = $("#location option:selected").val();
		if(location==0){
			$("#location_other").slideDown();
		}else{
			$("#location_other").slideUp();
		}
	    $("#testimonial-slider").owlCarousel({
	        items:1,
	        itemsDesktop:[1199,1],
	        itemsDesktopSmall:[979,1],
	        itemsTablet:[768,1],
	        pagination: false,
	        navigation:true,
	        navigationText:["",""],
	        autoPlay:true
	    });
	});
	function changeLocation(){
		var city = $("#city option:selected").val();
		$('#location').empty().append('<option value="">--pilih--</option>');
		$("#location_other").slideUp();
	   	$.ajax({  
		    type: 'POST',  
		    url: 'model/get_model.php', 
		    data: { type: 'getLocation', city_id: city },
		    success: function(response) {
		        $("#location").html(response);
				$("#location").val($("#location option[selected]").val());
		    }
		});
	}
	function changeLocationOther(){
		var location = $("#location option:selected").val();
		if(location==0){
			$("#location_other").slideDown();
		}else{
			$("#location_other").slideUp();
		}
	}
</script>
<!-- Testimonial -end -->
<style>
.bx-viewport{width: 100%; border: 1px solid red;}
.bx-wrapper .bx-prev {left: -45px;}
.bx-wrapper .bx-next {right: -35px;}
@media screen and (max-width: 768px) {
	.services-content{ 
		background-size: 95% 100%, 100% 100%;
	}
</style>
</body>
</html>