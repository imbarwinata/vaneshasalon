<?php
if($_POST){
    if($_POST['old_password']==NULL OR $_POST['new_password']==NULL OR $_POST['konfir_password']==NULL){
      if($_POST['old_password']==NULL){ $validation_old_password = "Password lama tidak boleh kosong."; }
      if($_POST['new_password']==NULL){ $validation_new_password = "Password baru tidak boleh kosong."; }
      if($_POST['konfir_password']==NULL){ $validation_konfir_password = "Konfirmasi password baru tidak boleh kosong."; }
      print_r($_POST);
    }else{
    	$email = $_SESSION['MemberEmail'];
    	$password = md5($_POST['old_password']);
    	$cek_password = $db->query("SELECT * FROM member WHERE MemberEmail='$email' AND MemberPassword='$password'","row");
    	if($_POST['new_password']!=$_POST['konfir_password']){
    		$validation_status = "Password baru dengan konfirmasi password tidak sama.";
    		echo $_POST['new_password']."<br>";
    		echo $_POST['konfir_password'];
    	}elseif ($cek_password==false) {
    		$validation_status = "Password lama tidak sama.";
    	}else{
    		$query['MemberPassword'] = md5($_POST['new_password']);
    		$where = ['MemberEmail'=>$email];
    		$query_update = $db->update('member',$query,$where,"notlike");
    		if($query_update==true){
    			echo "<script>alert('Perubahan kata sandi berhasil.');</script>";
				echo "<script>window.location=('?link=change_password');</script>";
    		}else{
    			echo "<script>alert('Perubahan kata sandi berhasil, silahkan coba lagi.');</script>";
				echo "<script>window.location=('?link=change_password');</script>";
    		}
    	}
    }
  }
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Vanesha Salon</title><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<?php include('templates/user/css_view.php'); ?>
<style type="text/css">
	.form-error{ color: #d26a5c; position: relative; top: 2px; }
</style>
</head>
<body>
<?php include('templates/user/navbar_view.php'); ?>

<div class="body">
	<div class="space-30"></div>
	<div class="block-main container">
		<div class="row">
			<ol class="breadcrumb hidden-content">
				<li><a href="index.php">Home</a></li>
				<li><a href="?link=my_account">Akun</a></li>
				<li><a href="#" class="current">Ganti Kata Sandi</a></li>
			</ol>
		</div>
	</div>
		

	<div class="block-main container container-fix">
		<div class="row">
			<!-- MY ACCOUNT -->
			<div class="account-wrap">
				<div class="container">
					<div class="row">
						<div class="col-sm-8 col-md-8">
							<!-- HTML -->
							<div id="account-id">
								<h4 class="account-title"><span class="fa fa-chevron-right"></span>Ganti Kata Sandi</h4>
								<div class="account-form">
									<form method="post">
										<?php
						                    if(isset($validation_status)){ ?>
						                    <div class="alert alert-danger">
										    	<strong>Error ! </strong><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?= $validation_status; ?>
										  	</div>
						                  <?php
						                    }
						                  ?>
										<div class="form-group">
											<label>Kata Sandi Lama</label>
											<input type="password" value="<?php if($_POST){ if($_POST['old_password']!=NULL){ echo $_POST['old_password']; } } ?>" class="form-control" name="old_password" style="border-radius: 0px;">
											<?php
							                    if(isset($validation_old_password)){ ?>
							                      <em class="form-error"><?php echo $validation_old_password; ?></em>
							                  <?php
							                    }
							                  ?>
										</div>
										<hr>
										<div class="form-group">
											<label>Kata Sandi Baru</label>
											<input type="password" value="<?php if($_POST){ if($_POST['new_password']!=NULL){ echo $_POST['new_password']; } } ?>" class="form-control" name="new_password" style="border-radius: 0px;">
											<?php
							                    if(isset($validation_new_password)){ ?>
							                      <em class="form-error"><?php echo $validation_new_password; ?></em>
							                  <?php
							                    }
							                  ?>
										</div>
										<div class="form-group">
											<label>Konfirmasi Kata Sandi Baru</label>
											<input type="password" value="<?php if($_POST){ if($_POST['konfir_password']!=NULL){ echo $_POST['konfir_password']; } } ?>" class="form-control" name="konfir_password" style="border-radius: 0px;">
											<?php
							                    if(isset($validation_konfir_password)){ ?>
							                      <em class="form-error"><?php echo $validation_konfir_password; ?></em>
							                  <?php
							                    }
							                  ?>
										</div>
										<button type="submit" class="btn btn-primary btn-white-purple" style="border-radius: 0px; padding: 6px 50px;">
											Simpan
										</button>
									</form>
								</div>
							</div>
						</div>

						<div class="col-sm-4 col-md-4">
							<!-- HTML -->
							<div id="account-id2">
								<h3 class="account-title" style="margin-bottom: 20px; margin-top: 15px;">Akun</h3>
								<div class="account">
									<ul>
										<li>
											<a href="?link=my_account"><i class="fa fa-user"></i> &nbsp; Akun Saya</a>
										</li>
										<li>
											<a href="#" class="current"><i class="fa fa-lock"></i> &nbsp; Ganti Kata Sandi</a>
										</li>
										<li>
											<a href="?link=history_order"><i class="fa fa-book"></i> &nbsp; Daftar Pesanan</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>



		</div>
	</div>
	<div class="space-60"></div>

	<?php include('templates/user/footer_view.php'); ?>
</div>

<?php include('templates/user/script_view.php'); ?>
<script type="text/javascript" src="design/js/jquery-1.12.0.min.js"></script>
<script src="design/js//bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

<link href="design/plugins/bxslider/jquery.bxslider.css" rel="stylesheet" />
<!-- Testimonial -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
<script>
	$(document).ready(function(){
    $("#testimonial-slider").owlCarousel({
        items:1,
        itemsDesktop:[1199,1],
        itemsDesktopSmall:[979,1],
        itemsTablet:[768,1],
        pagination: false,
        navigation:true,
        navigationText:["",""],
        autoPlay:true
    });
});
</script>
<!-- Testimonial -end -->
<style>
.bx-viewport{width: 100%; border: 1px solid red;}
.bx-wrapper .bx-prev {left: -45px;}
.bx-wrapper .bx-next {right: -35px;}
@media screen and (max-width: 768px) {
	.services-content{ 
		background-size: 95% 100%, 100% 100%;
	}
</style>
</body>
</html>