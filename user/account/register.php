<?php
if($_POST){
    if($_POST['email']==NULL OR $_POST['nama']==NULL OR $_POST['konfir_password']==NULL OR $_POST['password']==NULL OR $_POST['telp']==NULL OR $_POST['location']==NULL OR $_POST['city']==NULL OR $_POST['address']==NULL){
      if($_POST['nama']==NULL){ $validation_nama = "Nama tidak boleh kosong."; }
      if($_POST['email']==NULL){ $validation_email = "Email tidak boleh kosong."; }
      if($_POST['konfir_password']==NULL){ $validation_konfir_password = "Konfirmasi password tidak boleh kosong."; }
      if($_POST['password']==NULL){ $validation_password = "Password tidak boleh kosong."; }
      if($_POST['telp']==NULL){ $validation_telp = "Nomor telepon tidak boleh kosong."; }
      if($_POST['address']==NULL){ $validation_address = "Alamat tidak boleh kosong."; }
      if($_POST['location']==NULL){ $validation_location = "Lokasi tidak boleh kosong."; }
      if($_POST['city']==NULL){ $validation_city = "Kota tidak boleh kosong."; }
    }else{
      if($_POST['password'] != $_POST['konfir_password']){
      	$validation_password_error = "Password tidak sama.";
      }else{
      	$cek_member = $db->query("SELECT * FROM member WHERE MemberEmail='".$_POST['email']."'","row");
      	if($cek_member==true){
      		$validation_status = "Email ini sudah digunakan.";
      	}
        else{
        	$id = $generate->generate_custom_id("M","ymd","member","MemberID",5);
	      	$query['MemberID'] = $id;
	      	$query['MemberName'] = $_POST['nama'];
	      	$query['MemberEmail'] = $_POST['email'];
	      	$query['MemberPassword'] = md5($_POST['password']);
	      	$query['MemberPhone'] = $_POST['telp'];
	      	$query['MemberAddress'] = $_POST['address'];
	      	$query['MemberActive'] = 1;
	      	$query['LocationID'] = $_POST['location'];
	      	$query['CityID'] = $_POST['city'];
	      	if($query['LocationID']==0){
		      	$query['MemberLocation'] = $_POST['location_other'];	      		
	      	}else{
		      	$query['MemberLocation'] = "";
	      	}
        	$query_simpan = $db->insert('member',$query);
	      	if($query_simpan==true){
	      		echo "<script>alert('Pendaftaran berhasil, silahkan masuk.');</script>";
				echo "<script>window.location=('?link=account');</script>";
	      	}else{
	      		echo "<script>alert('Pendaftaran gagal, silahkan coba lagi.');</script>";
				echo "<script>window.location=('?link=account');</script>";      		
	      	}
        }
      }
    }
  }
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Vanesha Salon</title><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<?php include('templates/user/css_view.php'); ?>
<style type="text/css">
	.form-error{ color: #d26a5c; position: relative; top: 2px; }
</style>
</head>
<body>
<?php include('templates/user/navbar_view.php'); ?>

<div class="body">
	<div class="space-30"></div>
	<div class="block-main container">
		<div class="row">
			<ol class="breadcrumb hidden-content">
				<li><a href="index.php">Home</a></li>
				<li><a href="#" class="current">Daftar</a></li>
			</ol>
		</div>
	</div>
		

	<div class="block-main container container-fix">
		<div class="row">
			<!-- MY ACCOUNT -->
			<div class="account-wrap">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 col-md-12">
							<!-- HTML -->
							<div id="account-id">
								<h4 class="account-title"><span class="fa fa-chevron-right"></span>Daftar</h4>
								<div class="account-form">
									<form method="post" action="?link=register">
										<?php
						                    if(isset($validation_status)){ ?>
						                    <div class="alert alert-danger">
										    	<strong>Error ! </strong><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?= $validation_status; ?>.
										  	</div>
						                  <?php
						                    }
						                  ?>
										<div class="form-group">
											<label for="email">Email *</label>
											<input value="<?php if($_POST){ if($_POST['email']!=NULL){ echo $_POST['email']; } } ?>" type="email" class="form-control" name="email" style="border-radius: 0px;">
										 <?php
						                    if(isset($validation_email)){ ?>
						                      <em class="form-error"><?php echo $validation_email; ?></em>
						                  <?php
						                    }
						                  ?>
										</div>
										<div class="form-group">
											<label for="email">Password *</label>
											<input value="<?php if($_POST){ if($_POST['password']!=NULL){ echo $_POST['password']; } } ?>" type="password" class="form-control" name="password" style="border-radius: 0px;">
										 <?php
						                    if(isset($validation_password)){ ?>
						                      <em class="form-error"><?php echo $validation_password; ?></em>
						                  <?php
						                    }
						                  ?>
						                  <?php
						                    if(isset($validation_password_error)){ ?>
						                      <em class="form-error"><?php echo $validation_password_error; ?></em>
						                  <?php
						                    }
						                  ?>
										</div>
										<div class="form-group">
											<label for="email">Konfirmasi Password *</label>
											<input value="<?php if($_POST){ if($_POST['konfir_password']!=NULL){ echo $_POST['konfir_password']; } } ?>" type="password" class="form-control" name="konfir_password" style="border-radius: 0px;">
										 <?php
						                    if(isset($validation_konfir_password)){ ?>
						                      <em class="form-error"><?php echo $validation_konfir_password; ?></em>
						                  <?php
						                    }
						                  ?>
						                  <?php
						                    if(isset($validation_password_error)){ ?>
						                      <em class="form-error"><?php echo $validation_password_error; ?></em>
						                  <?php
						                    }
						                  ?>
										</div>
										<br>
										<div class="form-group">
											<label for="email">Nama *</label>
											<input value="<?php if($_POST){ if($_POST['nama']!=NULL){ echo $_POST['nama']; } } ?>" type="text" class="form-control" name="nama" style="border-radius: 0px;">
										 <?php
						                    if(isset($validation_nama)){ ?>
						                      <em class="form-error"><?php echo $validation_nama; ?></em>
						                  <?php
						                    }
						                  ?>
										</div>
										<div class="form-group">
											<label for="email">Kota *</label>
											<select onchange="changeLocation()" id="city" style="width:100%;" name="city" class="js-select2 form-control">
						                      <option value="">--pilih--</option>
						                  <?php
						                  foreach ($getCity as $data_city){ ?>
						                      <option value="<?= $data_city->CityID; ?>" <?php if($_POST){ if($_POST['city']!=NULL){ if($_POST['city']==$data_city->CityID){ echo "selected"; } } } ?>><?= $data_city->CityName; ?></option>
						                  <?php
						                  }
						                  ?>
						                    </select>
										 <?php
						                    if(isset($validation_city)){ ?>
						                      <em class="form-error"><?php echo $validation_city; ?></em>
						                  <?php
						                    }
						                  ?>
										</div>
										<div class="form-group">
											<label for="email">Lokasi *</label>
											<select onchange="changeLocationOther()" id="location" style="width:100%;" name="location" class="js-select2 form-control">
						                      <optgroup label="--pilih kota dahulu--"></optgroup>
						                     </select>
										 <?php
						                    if(isset($validation_location)){ ?>
						                      <em class="form-error"><?php echo $validation_location; ?></em>
						                  <?php
						                    }
						                  ?>
										</div>
										<div class="form-group" id="location_other">
											<label>Tambah Lokasi Lain *</label>
											<input maxlength="50" value="<?php if($_POST){ if($_POST['location_other']!=NULL){ echo $_POST['location_other']; } } ?>" type="text" class="form-control" name="location_other" style="border-radius: 0px;">
										 <?php
						                    if(isset($validation_location_other)){ ?>
						                      <em class="form-error"><?php echo $validation_location_other; ?></em>
						                  <?php
						                    }
						                  ?>
										</div>
										<div class="form-group">
											<label for="email">Nomor Telepon *</label>
											<input value="<?php if($_POST){ if($_POST['telp']!=NULL){ echo $_POST['telp']; } } ?>" type="number" class="form-control" name="telp" style="border-radius: 0px;">
										 <?php
						                    if(isset($validation_telp)){ ?>
						                      <em class="form-error"><?php echo $validation_telp; ?></em>
						                  <?php
						                    }
						                  ?>
										</div>

										<div class="form-group">
					                      <label>Alamat</label>
					                      <textarea class="form-control" rows="5" name="address"><?php if($_POST){ if($_POST['address']!=NULL){ echo $_POST['address']; } } ?></textarea>
						                <?php
						                  if(isset($validation_address)){ ?>
						                    <em class="form-error"><?php echo $validation_address; ?></em>
						                <?php
						                  }
						                ?>
					                  </div>
										<button type="submit" class="btn btn-primary btn-white-purple" style="border-radius: 0px; padding: 6px 50px;">
											Daftar
										</button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>



		</div>
	</div>
	<div class="space-60"></div>

	<?php include('templates/user/footer_view.php'); ?>
</div>

<?php include('templates/user/script_view.php'); ?>
<script type="text/javascript" src="design/js/jquery-1.12.0.min.js"></script>
<script src="design/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

<link href="design/plugins/bxslider/jquery.bxslider.css" rel="stylesheet" />
<!-- Testimonial -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
<script>
	$(document).ready(function(){
	    $("#testimonial-slider").owlCarousel({
	        items:1,
	        itemsDesktop:[1199,1],
	        itemsDesktopSmall:[979,1],
	        itemsTablet:[768,1],
	        pagination: false,
	        navigation:true,
	        navigationText:["",""],
	        autoPlay:true
	    });
	});
	$("#location_other").slideUp();
	function changeLocation(){
		var city = $("#city option:selected").val();
		$('#location').empty().append('<option value="">--pilih--</option>');
		$("#location_other").slideUp();
	   	$.ajax({  
		    type: 'POST',  
		    url: 'model/get_model.php', 
		    data: { type: 'getLocation', city_id: city },
		    success: function(response) {
		        $("#location").html(response);
				$("#location").val($("#location option[selected]").val());
		    }
		});
	}
	function changeLocationOther(){
		var location = $("#location option:selected").val();
		if(location==0){
			$("#location_other").slideDown();
		}else{
			$("#location_other").slideUp();
		}
	}
</script>
<!-- Testimonial -end -->
<style>
.bx-viewport{width: 100%; border: 1px solid red;}
.bx-wrapper .bx-prev {left: -45px;}
.bx-wrapper .bx-next {right: -35px;}
@media screen and (max-width: 768px) {
	.services-content{ 
		background-size: 95% 100%, 100% 100%;
	}
</style>
</body>
</html>