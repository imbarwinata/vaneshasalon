<?php
if($_POST){
    if($_POST['nama']==NULL OR $_POST['rekening']==NULL){
      if($_POST['nama']==NULL){ $validation_nama = "Nama tidak boleh kosong."; }
      if($_POST['rekening']==NULL){ $validation_rekening = "Rekening tidak boleh kosong."; }
    }else{
            $id = $generate->generate_custom_id("OC","ymd","confirmation","ConfirmationID",3);
    		$order_id = $_GET['id'];
	      	$query['ConfirmationID'] = $id;
      	  	$query['ConfirmationName'] = $_POST['nama'];
      	  	$query['ConfirmationAccount'] = $_POST['rekening'];
      	  	$query['ConfirmationStatus'] = 0;
      	  	$query['ConfirmationDate'] = $date->getCurrentDate();
	      	$query['MemberID'] = $_SESSION['MemberID'];
	      	$query['OrderID'] = $order_id;
	      	if($_FILES['bukti']['name'] != NULL){
	      		$temp = explode(".", $_FILES["bukti"]["name"]);
				$newfilename = $id . '.' . end($temp);
				$query['ConfirmationImage'] = $newfilename;
				copy($_FILES ["bukti"]["tmp_name"], "img/bukti/".$query['ConfirmationImage']);
            }
			$query_save_confirmation = $db->insert("confirmation",$query);
	      	if($query_save_confirmation==true){
	      		echo "<script>alert('Data konfirmasi berhasil disimpan.');</script>";
				echo "<script>window.location=('?link=history_order');</script>";
	      	}else{
	      		echo "<script>alert('Data konfirmasi gagal disimpan, silahkan coba lagi.');</script>";
				echo "<script>window.location=('?link=payment_confirmation&id=$order_id');</script>";      		
	      	}
    }
  }
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Vanesha Salon</title><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<?php include('templates/user/css_view.php'); ?>
<style type="text/css">
	.form-error{ color: #d26a5c; position: relative; top: 2px; }
</style>
</head>
<body>
<?php include('templates/user/navbar_view.php'); ?>

<div class="body">
	<div class="space-30"></div>
	<div class="block-main container">
		<div class="row">
			<ol class="breadcrumb hidden-content">
				<li><a href="index.php">Home</a></li>
				<li><a href="?link=my_account">Pemesanan</a></li>
				<li><a href="#" class="current">Konfirmasi</a></li>
			</ol>
		</div>
	</div>
		

	<div class="block-main container container-fix">
		<div class="row">
			<!-- MY ACCOUNT -->
			<div class="account-wrap">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 col-md-12">
							<!-- HTML -->
							<div id="account-id">
								<h4 class="account-title"><span class="fa fa-chevron-right"></span>Konfirmasi Pesanan</h4>
								<div class="account-form">
									<form method="post" enctype="multipart/form-data">
										<div class="form-group">
											<label for="email">Nama Sesuai Buku Tabungan *</label>
											<input value="<?php if($_POST){ if($_POST['nama']!=NULL){ echo $_POST['nama']; } else{echo "";} } ?>" type="text" class="form-control" name="nama" style="border-radius: 0px;">
										 <?php
						                    if(isset($validation_nama)){ ?>
						                      <em class="form-error"><?php echo $validation_nama; ?></em>
						                  <?php
						                    }
						                  ?>
										</div>
										<div class="form-group">
											<label for="email">Rekening Tujuan *</label>
											<select style="width:100%;" name="rekening" class="js-select2 form-control">
						                      <option value="">--pilih--</option>
						                  <?php
						                  $rekening = ['BCA','BRI'];
						                  foreach ($rekening as $key => $value){ ?>
						                      <option value="<?= $key; ?>" <?php if($_POST){ if($_POST['rekening']!=NULL){ if($_POST['rekening']==$key){ echo "selected"; } } } ?>><?= $value; ?></option>
						                  <?php
						                  }
						                  ?>
						                    </select>						                    
										 <?php
						                    if(isset($validation_rekening)){ ?>
						                      <em class="form-error"><?php echo $validation_rekening; ?></em>
						                  <?php
						                    }
						                  ?>
										</div>
										<div class="form-group">
											<label for="email">Unggah Bukti Pembayaran *</label>
											<input type="file" name="bukti" required accept="image/*">
										</div>
										
										<button type="submit" class="btn btn-primary btn-white-purple" style="border-radius: 0px; padding: 6px 50px;">
											Simpan
										</button>
									</form>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="space-60"></div>

	<?php include('templates/user/footer_view.php'); ?>
</div>

<?php include('templates/user/script_view.php'); ?>
<script type="text/javascript" src="design/js/jquery-1.12.0.min.js"></script>
<script src="design/js//bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

<link href="design/plugins/bxslider/jquery.bxslider.css" rel="stylesheet" />
<!-- Testimonial -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
<script>
	$(document).ready(function(){
    $("#testimonial-slider").owlCarousel({
        items:1,
        itemsDesktop:[1199,1],
        itemsDesktopSmall:[979,1],
        itemsTablet:[768,1],
        pagination: false,
        navigation:true,
        navigationText:["",""],
        autoPlay:true
    });
});
</script>
<!-- Testimonial -end -->
<style>
.bx-viewport{width: 100%; border: 1px solid red;}
.bx-wrapper .bx-prev {left: -45px;}
.bx-wrapper .bx-next {right: -35px;}
@media screen and (max-width: 768px) {
	.services-content{ 
		background-size: 95% 100%, 100% 100%;
	}
</style>
</body>
</html>