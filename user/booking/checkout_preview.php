<?php
	if(isset($_SESSION['MemberID']) AND isset($_SESSION['nama'])){
		$get_location = $db->query("SELECT * FROM location WHERE LocationID='".$_SESSION['location']."'","row");
		//$format_hours = time() + (1 * 3 * 60 * 60); // 1 days 3 hours 60 minute 60 seconds
		//$hours = date('Y-m-d H:i:s', $format_hours);
		$date2 = strtotime("+4 hours", strtotime($_SESSION['date'])); // day hours
		$expired = date("Y-m-d H:i:s", $date2);
		if($_POST){
            $id = $generate->generate_custom_id("O","ymd","trorder","OrderID",3);
            if($_SESSION['location']!="0"){
	            $get_price = $db->query("SELECT * FROM location WHERE LocationID='".$_SESSION['location']."'","row")['LocationFare'];            	
            }else{
	            $get_price = $db->query("SELECT * FROM city WHERE CityID='".$_SESSION['city']."'","row")['CityFare'];
            }
			$query['OrderID'] = $id;
			$query['MemberID'] = $_SESSION['MemberID'];
			$query['LocationID'] = $_SESSION['location'];
			$query['CityID'] = $_SESSION['city'];
			$query['OrderTotal'] = $proccess->totalCart()+$get_price;
			$query['OrderCart'] = $proccess->totalCart();
			$query['OrderName'] = $_SESSION['nama'];
			$query['OrderPhone'] = $_SESSION['telp'];
			$query['OrderAddress'] = $_SESSION['address'];
			$query['OrderStatus'] = 0;
			// 0 ( Pending ), 1 (Cancel) , 2 ( Process ), 3 ( Departure to destination ), 4 ( Finish )
			$query['OrderExpired'] = $expired;
			$query['OrderDate'] = $_SESSION['date'];
			$query['OrderDateEntry'] = $date->getCurrentDate();
			$query_save_order = $db->insert("trorder",$query);

			foreach ($_SESSION['cart'] as $key => $value):
				$services = $db->query("SELECT * FROM services WHERE ServicesID='$key'","row");
				$subtotal = $services['ServicesPrice'] * $_SESSION['cart'][$key]['quantity'];
				$member_id = $_SESSION['MemberID'];

				$id_detail = $generate->generate_custom_id("OD","ymd","trorderdetail","OrderDetailID",5);
				$query_detail['OrderDetailID'] = $id_detail;
				$query_detail['OrderID'] = $id;
				$query_detail['ServicesID'] = $key;
				$query_detail['OrderDetailQuantity'] = $_SESSION['cart'][$key]['quantity'];
				$query_detail['OrderDetailSubtotal'] = $_SESSION['cart'][$key]['quantity'] * $services['ServicesPrice'];
				$query_save_order_detal = $db->insert("trorderdetail",$query_detail);
			endforeach;
			if($query_save_order == true AND $query_save_order_detal == true){
				$_SESSION['order_id'] = $id;
				unset($_SESSION['cart']);
				unset($_SESSION['choose_member_address']);
				unset($_SESSION['nama']);
				unset($_SESSION['telp']);
				unset($_SESSION['location']);
				unset($_SESSION['city']);
				unset($_SESSION['address']);
				echo "<script>alert('Pemesanan berhasil.');</script>";
				echo "<script>window.location=('?link=checkout_review');</script>";
			}else{
				echo "<script>alert('Pemesanan gagal.');</script>";				
				echo "<script>window.location=('?link=checkout_review');</script>";
			}
		}
?>
<!doctype html>
<html>
<head>
<title>Vanesha Salon</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<?php include('templates/user/css_view.php'); ?>
<style type="text/css">
	.padding-bottom{ margin-bottom: 15px; margin-left: -55px; }
	.form-error{ color: #d26a5c; position: relative; top: 2px; }
	.account-title span {
		background: #80437b;
	}
</style>
</head>
<body>
<?php include('templates/user/navbar_view.php'); ?>

<div class="body">
	<div class="space-30"></div>
	<div class="block-main container">
		<div class="row">
			<ol class="breadcrumb hidden-content">
				<li><a href="index.php">Home</a></li>
				<li><a href="?link=my_account">Pesanan</a></li>
				<li><a href="#" class="current">Checkout</a></li>
			</ol>
		</div>
	</div>
	<div class="block-main container container-fix">
		<div class="row">
			<!-- MY ACCOUNT -->
			<div class="account-wrap">
				<div class="container">
					<div class="row">
						<form method="post" class="col-sm-12 col-md-12">
							<!-- HTML -->
							<div id="account-id">
								<h4 class="account-title"><span class="fa fa-chevron-right"></span>Pilih Alamat</h4>
								<div class="account-form"  style="padding:30px; padding-left: 50px;">
									<p><?= "<strong>".$_SESSION['nama']."</strong> (".$_SESSION['telp'].") "; ?></p>
									<p><?= $_SESSION['address']; ?></p>
									<p><?= $get_location['LocationTitle']; ?></p>
								</div>
							</div><br>

<!-- 
							<div id="account-id">
								<h4 class="account-title"><span class="fa fa-chevron-right"></span>Tinjuan Pemesanan</h4>
							</div>
 -->

							<div id="account-id">
								<h4 class="account-title"><span class="fa fa-chevron-right"></span>Tinjuan Pemesanan</h4>
								<div class="account-form">
									<div class="table-responsive">
										<table class="table table">
											<tr>
												<th>Nama Layanan</th>
												<th>Total Pesan</th>
												<th>Harga Satuan</th>
												<th>Subtotal</th>
											</tr>
								<?php
									$total = 0;
									if(isset($_SESSION['cart'])){
										$location = $_SESSION['location'];
										if($location!="0"){
											$get_ongkir = $db->query("SELECT * FROM location WHERE LocationID='".$_SESSION['location']."'","row");
											$flag_fare = 1;
										}else{
											$get_ongkir = $db->query("SELECT * FROM city WHERE CityID='".$_SESSION['city']."'","row");
											$flag_fare = 0;
										}
										foreach ($_SESSION['cart'] as $key => $value):
											$services = $db->query("SELECT * FROM services WHERE ServicesID='$key'","row");
											$subtotal = $services['ServicesPrice'] * $_SESSION['cart'][$key]['quantity'];
											$total += $subtotal;
											$member_id = $_SESSION['MemberID'];
								?>
											<tr>
												<td><?= $services['ServicesName']; ?></td>
												<td><?= $_SESSION['cart'][$key]['quantity']; ?></td>
												<td>Rp. <?= number_format($services['ServicesPrice'],2,",","."); ?></td>
												<td>Rp. <?= number_format($subtotal,2,",","."); ?></td>
											</tr>
								<?php
										endforeach;
									}else{ ?>
											<tr>
												<td colspan="4" style="text-align:center;">Keranjang masih kosong.</td>
											</tr>
								<?php
									}
								?>
										</table>
										<div class="col-lg-4 pull-right checkout">
											<table class="table table-checkout">
												<tr>
													<td>Subtotal</td>
													<td>:</td>
													<td>Rp. <?= number_format($total,2,",","."); ?>
														<input type="hidden" name="subtotal" value="<?= $total ?>">
													</td>
												</tr>
												<tr>
													<td>Ongkos</td>
													<td>:</td>
													<td>Rp. <?= $flag_fare==1? number_format($get_ongkir['LocationFare'],2,",","."):number_format($get_ongkir['CityFare'],2,",","."); ?> (<?= isset($get_ongkir['LocationTitle'])? $get_ongkir['LocationTitle']:$get_ongkir['CityName'];?>)</td>
												</tr>
												<tr>
													<td>Total</td>
													<td>:</td>
													<td>Rp. <?= $flag_fare==1? number_format($total+$get_ongkir['LocationFare'],2,",","."):number_format($total+$get_ongkir['CityFare'],2,",","."); ?>
														<input type="hidden" name="total" value="<?= isset($get_ongkir['LocationFare'])? $total+$get_ongkir['LocationFare']:$total+$get_ongkir['CityFare']; ?>">
													</td>
												</tr>
												<tr>
													<td colspan="2"></td>
													<td><button type="submit" class="btn btn-danger btn-block">Selesai</button></td>
												</tr>
											</table>
										</div>
									</div>
								</div>			
							</div>
							
						</form>

					</div>
				</div>
			</div>



		</div>
	</div>
	<div class="space-60"></div>

	<?php include('templates/user/footer_view.php'); ?>
</div>

<?php include('templates/user/script_view.php'); ?>
<script type="text/javascript" src="design/js/jquery-1.12.0.min.js"></script>
<script src="design/js//bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

<link href="design/plugins/bxslider/jquery.bxslider.css" rel="stylesheet" />
<!-- Testimonial -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
<script>
	$(document).ready(function(){
    $("#testimonial-slider").owlCarousel({
        items:1,
        itemsDesktop:[1199,1],
        itemsDesktopSmall:[979,1],
        itemsTablet:[768,1],
        pagination: false,
        navigation:true,
        navigationText:["",""],
        autoPlay:true
    });
});
</script>
<!-- Testimonial -end -->
<style>
.bx-viewport{width: 100%; border: 1px solid red;}
.bx-wrapper .bx-prev {left: -45px;}
.bx-wrapper .bx-next {right: -35px;}
@media screen and (max-width: 768px) {
	.services-content{ 
		background-size: 95% 100%, 100% 100%;
	}
</style>
</body>
</html>
<?php
	}else{
		header('location: ?link=account');
	}
?>