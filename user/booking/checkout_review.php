<?php
	if(isset($_SESSION['MemberID']) AND isset($_SESSION['order_id'])){
		$order = $db->query("SELECT * FROM trorder WHERE OrderID='".$_SESSION['order_id']."'","row")
?>
<!doctype html>
<html>
<head>
<title>Vanesha Salon</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<?php include('templates/user/css_view.php'); ?>
<style type="text/css">
	.padding-bottom{ margin-bottom: 15px; margin-left: -55px; }
	.form-error{ color: #d26a5c; position: relative; top: 2px; }
	.account-title span {
		background: #80437b;
	}
</style>
</head>
<body>
<?php include('templates/user/navbar_view.php'); ?>

<div class="body">
	<div class="space-30"></div>
	<div class="block-main container container-fix">
		<div class="row">
			<!-- MY ACCOUNT -->
			<div class="account-wrap">
				<div class="container">
					<div class="row">
						<form method="post" class="col-sm-12 col-md-12">
							<!-- HTML -->
							<div id="account-id">
								<div class="account-form" style="border:none; padding:50px 200px 50px 200px;">
									<p class="review-p">Terima Kasih, anda telah berhasil melakukan checkout pemesanan. Untuk melanjutkan pemesanan silahkan anda transfer ke rekening kami sesuai biaya yang dicantumkan dibawah.</p>
									<p class="review-p" style="padding-top:20px;">Berikut adalah besar yang harus ditransfer.</p>
									<div class="review-div">
										<p>Rp. <?= number_format($order['OrderTotal'],2,",","."); ?></p>
									</div>

									<p class="review-p" style="padding-top:20px;">Daftar rekening untuk di transfer :</p>
									<div class="review-div">
										<ul>
											<li>BCA : xxxxxx</li>
											<li>BRI : xxxxxx</li>
										</ul>
									</div>
									<p class="review-p" style="padding-top:20px;">
										Pesanan akan otomatis dibatalkan jika dalam waktu 4jam konfirmasi pembayaran belum dilakukan. 
										<br>Sudah melakukan pembayaran? Segera lakukan konfirmasi pembayaran.<br><br>
										<a href="?link=payment_confirmation&id=<?= $_SESSION['order_id']; ?>" class="btn-black">Konfirmasi</a>
									</p>
								</div>
							</div><br>

						</form>

					</div>
				</div>
			</div>



		</div>
	</div>
	<div class="space-60"></div>

	<?php include('templates/user/footer_view.php'); ?>
</div>

<?php include('templates/user/script_view.php'); ?>
<script type="text/javascript" src="design/js/jquery-1.12.0.min.js"></script>
<script src="design/js//bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

<link href="design/plugins/bxslider/jquery.bxslider.css" rel="stylesheet" />
<!-- Testimonial -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
<script>
	$(document).ready(function(){
    $("#testimonial-slider").owlCarousel({
        items:1,
        itemsDesktop:[1199,1],
        itemsDesktopSmall:[979,1],
        itemsTablet:[768,1],
        pagination: false,
        navigation:true,
        navigationText:["",""],
        autoPlay:true
    });
});
</script>
<!-- Testimonial -end -->
<style>
.bx-viewport{width: 100%; border: 1px solid red;}
.bx-wrapper .bx-prev {left: -45px;}
.bx-wrapper .bx-next {right: -35px;}
@media screen and (max-width: 768px) {
	.services-content{ 
		background-size: 95% 100%, 100% 100%;
	}
</style>
</body>
</html>
<?php
	}else{
		header('location: ?link=account');
	}
?>