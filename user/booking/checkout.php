<?php
	if(isset($_SESSION['MemberID'])){
		if($_POST){
			if($_POST['tanggal'] == NULL OR $_POST['waktu'] == NULL){
				if($_POST['tanggal'] == NULL){ $validation_tanggal = "Isi tanggal pemesan.";  }
				if($_POST['waktu'] == NULL){ $validation_waktu = "Isi waktu pemesan.";  }
			}else{
				if($_POST['tanggal'] < $date->getDate()){
					$validation_confirm_date = "Tanggal dan waktu harus lebih atau sama dengan tanggal dan waktu sekarang.";
				}else{
					if($_POST['tanggal'] == $date->getDate() AND $_POST['waktu'] < $date->getTime()){
						$validation_confirm_date = "Tanggal dan waktu harus lebih atau sama dengan tanggal dan waktu sekarang.";
					}else{
						if($_POST['choose_member_address']=="old"){
							$_SESSION['choose_member_address'] = "old";
							$get_data_member = $db->query("SELECT * FROM member WHERE MemberID='".$_SESSION['MemberID']."'","row");
							$_SESSION['choose_member_address'] = "new";
							$_SESSION['nama'] = $get_data_member['MemberName'];
							$_SESSION['telp'] = $get_data_member['MemberPhone'];
							$_SESSION['location'] = $get_data_member['LocationID'];
							$_SESSION['city'] = $get_data_member['CityID'];
							$_SESSION['address'] = $get_data_member['MemberAddress'];
							$_SESSION['date'] = $_POST['tanggal']." ".$_POST['waktu'].":00";
							echo "<script>window.location=('?link=checkout_preview');</script>";
						}elseif($_POST['choose_member_address']=="new"){
							if($_POST['nama'] == NULL OR $_POST['telp'] == NULL OR $_POST['location'] == NULL OR $_POST['address'] == NULL){
								if($_POST['nama'] == NULL){ $validation_nama = "Isi nama pemesan.";  }
								if($_POST['telp'] == NULL){ $validation_telp = "Isi nomor telepon pemesan.";  }
								if($_POST['location'] == NULL){ $validation_location = "Isi lokasi pemesan.";  }
								if($_POST['address'] == NULL){ $validation_address = "Isi alamat pemesan.";  }
							}else{
								$_SESSION['choose_member_address'] = "new";
								$_SESSION['nama'] = $_POST['nama'];
								$_SESSION['telp'] = $_POST['telp'];
								$_SESSION['location'] = $_POST['location'];
								$_SESSION['city'] = $_POST['city'];
								$_SESSION['address'] = $_POST['address'];
								$_SESSION['date'] = $_POST['tanggal']." ".$_POST['waktu'].":00";
								echo "<script>window.location=('?link=checkout_preview');</script>";
							}
						}	
					}						
				}
			}
		}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<?php include('templates/user/css_view.php'); ?>
<style type="text/css">
	.padding-bottom{ margin-bottom: 15px; margin-left: -55px; }
	.form-error{ color: #d26a5c; position: relative; top: 2px; }
</style>
</head>
<body>
<?php include('templates/user/navbar_view.php'); ?>

<div class="body">
	<div class="space-30"></div>
	<div class="block-main container">
		<div class="row">
			<ol class="breadcrumb hidden-content">
				<li><a href="index.php">Home</a></li>
				<li><a href="?link=my_account">Pesanan</a></li>
				<li><a href="#" class="current">Checkout</a></li>
			</ol>
		</div>
	</div>
	<div class="block-main container container-fix">
		<div class="row">
			<!-- MY ACCOUNT -->
			<div class="account-wrap">
				<div class="container">
					<div class="row">
						<form method="post" class="col-sm-12 col-md-12">
							<!-- HTML -->
						<?php
		                    if(isset($validation_confirm_date)){ ?>
		                    <div class="alert alert-danger alert-dismissable">
								<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
								<strong>Failed!</strong> <?php echo $validation_confirm_date; ?>
							</div>
		                  <?php
		                    }
		                  ?>
							<div id="account-id">
								<h4 class="account-title"><span class="fa fa-chevron-right"></span>Pilih Alamat & Tanggal</h4>
								<div class="account-form"  style="padding-bottom:0px;">
								<table class="table table-striped">
			                        <tbody>
			                        	<tr>
			                                <td>
			                                    <input type="radio" name="choose_member_address" value="old" <?php if($_POST){ if($_POST['choose_member_address']!=NULL){ if($_POST['choose_member_address']=="old"){ echo "checked"; } } } else{ echo "checked"; }?>>
			                                </td>
			                                <td>
			                                <?php
			                                $get_member = $db->query("SELECT * FROM member AS m LEFT JOIN location AS l ON m.LocationID=l.LocationID LEFT JOIN city AS c ON m.CityID=c.CityID WHERE m.MemberEmail='".$_SESSION['MemberEmail']."'","row");
			                                ?>
			                                    <p><b><?= $get_member['MemberName'] ?></b> (<?= $get_member['MemberPhone'] ?>)</p>
			                                    <p><?= $get_member['MemberAddress'] ?></p>
			                                <?php
			                                if($get_member['LocationTitle']!=NULL){ ?>
			                                    <p><?= $get_member['LocationTitle'].", ".$get_member['CityName']; ?></p>			                                	
			                                <?php
			                                }else{ ?>
			                                    <p><?= $get_member['MemberLocation'].", ".$get_member['CityName']; ?></p>			                                	
			                                <?php
			                                }
			                                ?>
			                                </td>
			                            </tr>
			                            <tr>
			                                
			                                <td>
			                                
			                                                </div>
			                                            </div>

			                                        </ul>
			                                    </div>
			                                </td>
			                            </tr>
			                            <tr>
											<td colspan="2">
												<div class="col-lg-12">
													<div class="form-group">
														<label>Tanggal</label>
														<div class="input-group date form-group" data-provide="datepicker">
										                    <input name="tanggal" type="text" value="<?php if($_POST){ if($_POST['tanggal']!=NULL){ echo $_POST['tanggal']; } } ?>" placeholder="Tanggal Pemesanan ..." class="form-control datepicker">
										                    <div class="input-group-addon">
										                        <span class="glyphicon glyphicon-th"></span>
										                    </div>
										                </div>
										                <?php
										                    if(isset($validation_tanggal)){ ?>
										                      <em class="form-error"><?php echo $validation_tanggal; ?></em>
										                  <?php
										                    }
										                  ?>
													</div>
												</div>
												<div class="col-lg-12">
													<div class="form-group">
									                  <label>Jam</label>
									                  <input name="waktu" value="<?php if($_POST){ if($_POST['waktu']!=NULL){ echo $_POST['waktu']; } } ?>" type="text" class="form-control" placeholder="Waktu Pemesanan ..." data-field="time">
									                  <div id="waktu"></div>
									                  <?php
										                    if(isset($validation_waktu)){ ?>
										                      <em class="form-error"><?php echo $validation_waktu; ?></em>
										                  <?php
										                    }
										                  ?>
									                </div>
												</div>
											</td>
										</tr>
			                            <tr>
											<td colspan="2"><button type="submit" href="?link=checkout" class="btn btn-danger btn-block">Selanjutnya</button></td>
										</tr>
			                        </tbody>
			                    </table>

								</div>
							</div><br>

<!-- 
							<div id="account-id">
								<h4 class="account-title"><span class="fa fa-chevron-right"></span>Tinjuan Pemesanan</h4>
							</div>
 -->

							<div id="account-id">
								<h4 class="account-title"><span class="fa fa-chevron-right"></span>Tinjuan Pemesanan</h4>			
							</div>
							
						</form>

					</div>
				</div>
			</div>



		</div>
	</div>
	<div class="space-60"></div>

	<?php include('templates/user/footer_view.php'); ?>
</div>

<?php include('templates/user/script_view.php'); ?>
<script type="text/javascript" src="design/js/jquery-1.12.0.min.js"></script>

<!-- Plugins Tanggal -->
<script src="design/plugins/datepicker-bootstrap/js/bootstrap-datepicker.js"></script>
<script type="text/javascript">
	$.fn.datepicker.defaults.format = "yyyy-mm-dd";
	$('.datepicker').datepicker({
	    startDate: '-3d'
	});
</script>

<!-- Plugins Jam -->
<script src="design/plugins/datepicker/DateTimePicker.js"></script>
<script>
  $(document).ready(function(){
        $("#waktu").DateTimePicker({
            //dateFormat: "dd-MMM-yyyy",
            addEventHandlers: function()
            {
              var oDTP = this;
              oDTP.settings.maxDate = oDTP.getDateTimeStringInFormat("Date", "dd-MMM-yyyy", new Date());
            }
        });

    });
</script>

<script type="text/javascript">
$(document).ready(function() {
	$("#location_other").slideUp();
	function changeLocation(){
		var city = $("#city option:selected").val();
		$('#location').empty().append('<option value="">--pilih--</option>');
		$("#location_other").slideUp();
	   	$.ajax({  
		    type: 'POST',  
		    url: 'model/get_model.php', 
		    data: { type: 'getLocation', city_id: city },
		    success: function(response) {
		        $("#location").html(response);
				$("#location").val($("#location option[selected]").val());
		    }
		});
	}
	function changeLocationOther(){
		var location = $("#location option:selected").val();
		if(location==0){
			$("#location_other").slideDown();
		}else{
			$("#location_other").slideUp();
		}
	}
});
</script>

<style>
.bx-viewport{width: 100%; border: 1px solid red;}
.bx-wrapper .bx-prev {left: -45px;}
.bx-wrapper .bx-next {right: -35px;}
@media screen and (max-width: 768px) {
	.services-content{ 
		background-size: 95% 100%, 100% 100%;
	}
</style>
</body>
</html>
<?php
	}else{
		header('location: ?link=account');
	}
?>