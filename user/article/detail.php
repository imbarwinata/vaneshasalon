<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Vanesha Salon</title><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<?php include('templates/user/css_view.php'); ?>
</head>
<body>
<?php include('templates/user/navbar_view.php'); ?>

<div class="body">
	<div class="space-30"></div>
	<div class="block-main container">
		<div class="row">
			<ol class="breadcrumb hidden-content">
				<li><a href="?link=index">Home</a></li>
				<li><a href="?link=article">Article</a></li>
				<li><a href="#" class="current">Detail</a></li>
			</ol>
		</div>
	</div>
	
<!--	<section class="services-banner" style="background-image: url(design/img/castadiva-spa-wellness-01.jpg) url(design/img/layer-services.png);">
		<div class="services-banner-image" style="background-position:0px 0px; background-image: url(design/img/castadiva-spa-wellness-01.jpg);"></div>
	</section>-->
	
	<div id="banner2" style="background-position:0px 0px; background-image:url(design/img/castadiva-spa-wellness-01.jpg)">
		<h2><p style="text-align: center; padding-top: 6vw; color: rgba(255,255,255,1.00); font-family: 'Honeymoon-Up'; font-size: 3.3vw; font-weight: bold"></p></h2>
	</div>
	<section class="article-content-detail">
		<h2><?= $cek_permalink['ContentPointTitle']; ?></h2>
		<img src="img/content/article/<?= $cek_permalink['ContentPointImage']; ?>">
		<div class="article-detail-subtitle">
			<?= $date->convertFormat('d F Y', $cek_permalink['ContentPointDate']) ?> | <?= $cek_permalink['ContentPointView']; ?> Views
		</div>
		<div class="article-detail">
			<?= $cek_permalink['ContentPointDescription']; ?>
		</div>
	</section>
	
	<section class="article-content-related">
		<h3>Top Article</h3>
		
		<section class="top-article-list">
<?php
	if($top_article!=false){
		foreach ($top_article as $list_top_article): ?>
			<div>
				<div class="top-article-image" style="background-image: url(img/content/article/<?= $list_top_article->ContentPointImage; ?>)"></div>
				<div class="double-description">
					<label><?= $list_top_article->ContentPointTitle; ?></label>
					<a href="?link=article_detail&id=<?= $list_top_article->ContentPointPermalink; ?>">Read More</a>
				</div>
			</div>
<?php
		endforeach;
	}
?>
		</section>
	</section>
	<div style="clear: both;"></div>
	<?php include('templates/user/footer_view.php'); ?>
</div>

<?php include('templates/user/script_view.php'); ?>
<script src="design/plugins/bxslider/jquery.bxslider.min.js"></script>
<link href="design/plugins/bxslider/jquery.bxslider.css" rel="stylesheet" /><script>
$(document).ready(function(){
  $('.slider1').bxSlider({
    slideWidth: 600,
    slideHeight: 200,
    minSlides: 3,
    maxSlides: 3,
    moveSlides: 1,
    slideMargin: 5
  });
});
</script>
<!-- Testimonial -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
<script>
	$(document).ready(function(){
    $("#testimonial-slider").owlCarousel({
        items:1,
        itemsDesktop:[1199,1],
        itemsDesktopSmall:[979,1],
        itemsTablet:[768,1],
        pagination: false,
        navigation:true,
        navigationText:["",""],
        autoPlay:true
    });
});
</script>
<!-- Testimonial -end -->
<style>
.bx-viewport{width: 100%; border: 1px solid red;}
.bx-wrapper .bx-prev {left: -45px;}
.bx-wrapper .bx-next {right: -35px;}
@media screen and (max-width: 768px) {
	.services-content{ 
		background-size: 95% 100%, 100% 100%;
	}
</style>
</body>
</html>