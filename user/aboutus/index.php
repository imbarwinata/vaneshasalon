<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Vanesha Salon</title><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<?php include('templates/user/css_view.php'); ?>
</head>
<body>
<?php include('templates/user/navbar_view.php'); ?>

<div class="body">
	<div class="space-30"></div>
	
	<div class="block-main container">
		<div class="row">
			<ol class="breadcrumb hidden-content">
				<li><a href="?link=index">Home</a></li>
				<li><a href="#" class="current">Tentang Kami</a></li>
			</ol>
		</div>
	</div>
	
	<section class="home-service" style="margin-top: -20px;">
		<h2 class="text-center" style="font-size: 4vw;">Tentang Kami</h2>
		<div class="space-15"></div>
<?php
$data = $db->query("SELECT * FROM contentpoint WHERE ContentPointLabel = 'About' AND ContentPointShow = '1' ORDER BY ContentPointDate ","result");
if($data!=false){
	foreach ($data as $list_data): ?>
		<div class="home-service-content" onClick="">
			<div class="home-service-content-content">
				<h3><?= $list_data->ContentPointTitle; ?></h3>
				<?= $list_data->ContentPointDescription; ?>
			</div>
			<div class="home-services-background" style="background-image:url('img/content/about/<?= $list_data->ContentPointImage; ?>');"></div>
		</div>	
<?php
	endforeach;
	}
?>
		<div class="space-60"></div>
	</section>
	<?php include('templates/user/footer_view.php'); ?>
</div>

<?php include('templates/user/script_view.php'); ?>
<script src="design/plugins/bxslider/jquery.bxslider.min.js"></script>
<link href="design/plugins/bxslider/jquery.bxslider.css" rel="stylesheet" /><script>
$(document).ready(function(){
  $('.slider1').bxSlider({
    slideWidth: 600,
    slideHeight: 200,
    minSlides: 3,
    maxSlides: 3,
    moveSlides: 1,
    slideMargin: 5
  });
});
</script>
<!-- Testimonial -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
<script>
	$(document).ready(function(){
    $("#testimonial-slider").owlCarousel({
        items:1,
        itemsDesktop:[1199,1],
        itemsDesktopSmall:[979,1],
        itemsTablet:[768,1],
        pagination: false,
        navigation:true,
        navigationText:["",""],
        autoPlay:true
    });
});
</script>
<!-- Testimonial -end -->
<style>
.bx-viewport{width: 100%; border: 1px solid red;}
.bx-wrapper .bx-prev {left: -45px;}
.bx-wrapper .bx-next {right: -35px;}
</style>
</body>
</html>