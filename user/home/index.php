<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Vanesha Salon</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<?php include('templates/user/css_view.php'); ?>
<link rel="stylesheet" href="design/plugins/Swiper-3.4.2/dist/css/swiper.min.css">
</head>
<body>
<?php include('templates/user/navbar_view.php'); ?>
<div class="body">
<?php
	$banner = $db->query("SELECT * FROM content WHERE ContentLabel='Home Banner'","row");
?>
	<section class="banner" id="image" style="background-image: url(img/content/banner/<?= $banner['ContentImage']; ?>);">
		<div id="banner2"></div>
		<section class="banner-content">
			<h2><?= $banner['ContentTitle']; ?></h2>
			<p><?= $banner['ContentSubTitle']; ?></p>
			<button onClick="window.location=('<?= $banner['ContentLink']; ?>')">Kunjungi</button>
		</section>
	</section>
	
	<section class="home-service">
		<h2 class="text-center">Top services</h2>
		<div class="space-15"></div>
<?php
$top_sevices = $db->query("SELECT * FROM services WHERE ServicesShow='1' AND ServicesFeatured='1' ORDER BY ServicesName","result");
if($top_sevices!=false){
	foreach ($top_sevices as $list_top_sevices): ?>
		<form method="post" action="?link=cart" class="home-service-content">
			<div class="home-service-content-content">
				<h3><?= $list_top_sevices->ServicesName; ?></h3>
				<p><?= $list_top_sevices->ServicesDescription; ?></p>
				<label class="price_featured">Rp. <?= number_format($list_top_sevices->ServicesPrice,0,',','.'); ?></label><br>
		<?php
			if(!isset($_SESSION['MemberID'])){ ?>
				<input name="services" value="<?= $list_top_sevices->ServicesID; ?>" hidden="hidden">
				<button class="btn btn-white-purple" onClick="alert('Silahkan login dahulu!')" disabled="disabled">Pesan sekarang</button>
		<?php
			}else{ 
				if($status['StatusActive']==0){ ?>
					<input name="services" value="<?= $list_top_sevices->ServicesID; ?>" hidden="hidden">
					<button class="btn btn-white-purple" onclick="alert('Maaf salon untuk sementara ditutup.')" type="button">Pesan sekarang</button>
		<?php
				}elseif($status['StatusActive']==1){ ?>
				<input name="services" value="<?= $list_top_sevices->ServicesID; ?>" hidden="hidden">
				<button class="btn btn-white-purple" type="submit">Pesan sekarang</button>
		<?php
				}elseif($status['StatusActive']==2){ ?>
				<input name="services" value="<?= $list_top_sevices->ServicesID; ?>" hidden="hidden">
				<button class="btn btn-white-purple" onclick="alert('Maaf pegawai kami sedang sibuk, silahkan anda mencoba beberapa saat lagi.')" type="button">Pesan sekarang</button>
		<?php
				}
				?>
		<?php
			}
		?>
			</div>
			<div class="home-services-background" style="background-image:url('img/services/<?= $list_top_sevices->ServicesImage; ?>');"></div>
		</form>
<?php
	endforeach;
}
?>
		<div class="space-60"></div>
	</section>
<?php
	$why = $db->query("SELECT * FROM content WHERE ContentLabel='Why Vanesha'","row");
?>
	<section class="why-vanesha" style="background: url(img/content/why_vanesha/<?= $why['ContentImage']; ?>);">
		<div class="why-vanesha-layer">
			<h1><?= $why['ContentTitle']; ?></h2>
			<p><?= $why['ContentDescription']; ?></p>
			<button class="btn btn-purple-white" onClick="window.location=('<?= $why['ContentLink']; ?>')">baca selengkapnya</button>
		</div>
	</section>
	
	<section class="home-article">
		<h1 class="text-center">Top article</h1>
		<!-- Swiper -->
	    <div class="swiper-container">
	        <div class="swiper-wrapper">
            <?php
			$top_article = $db->query("SELECT * FROM contentpoint WHERE ContentPointLabel='Article' ORDER BY ContentPointView DESC LIMIT 6","result");
			if($top_article!=false){
				foreach($top_article as $list_top_article): ?>
				<div class="swiper-slide">
	            	<div class="swiper-slide-img" style="background-image:url(img/content/article/<?= $list_top_article->ContentPointImage; ?>)"></div>
	            	<div class="swiper-slide-content">
	            		<h4>
	            			<a style="color: rgba(128,67,123,1.00);" href="?link=article_detail&id=<?= $list_top_article->ContentPointPermalink; ?>"><?= $list_top_article->ContentPointTitle; ?></a> 
	            		</h4>
	            	</div>
	            </div>
			<?php
				endforeach;
			}
			?>
	            
	        </div>
	        <!-- Add Pagination -->
	        <div class="swiper-pagination"></div>
	    </div>
	</section>
	
<?php
$testimony = $db->query("SELECT * FROM contentpoint WHERE ContentPointLabel='Testimony' AND ContentPointShow='1' ORDER BY ContentPointTitle ASC","result");
if($testimony==true){
?>
	<section class="testimonial2" style="background: url(design/img/testimonial-background2.png);">
		<div class="testimonial2-layer">
			<h1>our <label>client</label> say</h1>
			<img src="design/img/testimonial-pager.png">
			<div id="testimonial-slider" class="owl-carousel">
        <?php
        	foreach ($testimony as $list_testimony): ?>
        		<div class="testimonial">
                    <div class="pic">
                        <img src="img/content/testimony/<?= $list_testimony->ContentPointImage; ?>" alt="">
                    </div>
                    <p class="description"><?= $list_testimony->ContentPointDescription; ?></p>
                    <div class="testimonial-prof">
                        <span class="title"><?= $list_testimony->ContentPointTitle; ?></span>
                        <small><?= $list_testimony->ContentPointSubDescription; ?></small>
                    </div>
                </div>
        <?php
        	endforeach;
        ?>
            </div>
		</div>
	</section>
<?php
}
?>
	<?php include('templates/user/footer_view.php'); ?>
</div>

<?php include('templates/user/script_view.php'); ?>
<script src="design/plugins/Swiper-3.4.2/dist/js/swiper.min.js"></script>

<script>
$(document).ready(function(){
	var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        effect: 'coverflow',
        grabCursor: true,
        centeredSlides: true,
        slidesPerView: 'auto',
        coverflow: {
            rotate: 50,
            stretch: 0,
            depth: 100,
            modifier: 1,
            slideShadows : true
        }
    });
});
</script>
<!-- Testimonial -->
<script type="text/javascript" src="design/js/owl.carousel.min.js"></script>
<script>
	$(document).ready(function(){
    $("#testimonial-slider").owlCarousel({
        items:1,
        itemsDesktop:[1199,1],
        itemsDesktopSmall:[979,1],
        itemsTablet:[768,1],
        pagination: false,
        navigation:true,
        navigationText:["",""],
        autoPlay:true
    });
});
</script>
<!-- Testimonial -end -->
</body>
</html>