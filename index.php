<?php
session_start();
include_once('config/koneksi.php');
include_once('model/query_model.php');
include_once('model/validation_model.php');
include_once('model/date_model.php');
include_once('model/generate_model.php');
include_once('model/proccess_model.php');
$db = new query_model();
$db->path = ""; // Setting root configurasi config database
$validation = new validation_model();
$date = new date_model();
$generate = new generate_model();
$generate->path = "";
$proccess = new proccess_model();
$proccess->path = "";
$status = $db->query("SELECT * FROM status","row");
if(isset($_GET['link']) AND $_GET['link']=="aboutus"){
	include "user/aboutus/index.php";
}
elseif(isset($_GET['link']) AND $_GET['link']=="layanan"){ 
	if(isset($_GET['category'])){
		$category = $_GET['category'];
		/* Seleksi category berdasarkan category permalink */
		$cek_category = $db->query("SELECT * FROM servicescategory WHERE ServicesCategoryPermalink='$category'","row");
		if($cek_category==false){
			header('HTTP/1.0 404 Not Found', true, 404);
		}else{
			$services = $db->query("SELECT * FROM services WHERE ServicesCategoryID='".$cek_category['ServicesCategoryID']."'","result");
			include "user/services/index.php";
		}
	}
}
elseif(isset($_GET['link']) AND $_GET['link']=="article"){ 
	include "user/article/index.php";
}
elseif(isset($_GET['link']) AND $_GET['link']=="article_detail"){ 
	if(!isset($_GET['id'])){
		header('HTTP/1.0 404 Not Found', true, 404);
		exit();
	}
	$permalink = $_GET['id'];
	$cek_permalink = $db->query("SELECT * FROM contentpoint WHERE ContentPointLabel='Article' AND ContentPointPermalink='$permalink'","row");
	if($cek_permalink==false){
		header('HTTP/1.0 404 Not Found', true, 404);
	}else{
		if(!isset($_COOKIE[$permalink])) {
			$data['ContentPointView'] = $cek_permalink['ContentPointView'] + 1;
            $where = ['ContentPointPermalink'=>$permalink];
            $query_update = $db->update("contentpoint",$data,$where,"notlike");
			setcookie($permalink, $permalink, time() + (86400 * 30));
		}
		$top_article = $db->query("SELECT * FROM contentpoint WHERE ContentPointLabel='article' AND ContentPointShow='1' ORDER BY ContentPointView DESC","result");
		include "user/article/detail.php";
	}
}
elseif(isset($_GET['link']) AND $_GET['link']=="contactus"){
	$contactus = $db->query("SELECT * FROM content WHERE ContentLabel='Contact Us'","row");
	include "user/contactus/index.php";
}
elseif(isset($_GET['link']) AND $_GET['link']=="terms_and_condition"){ 
	$terms_and_condition = $db->query("SELECT * FROM content WHERE ContentLabel='Terms and Condition'","row");
	include "user/other/terms_and_condition.php";
}
elseif(isset($_GET['link']) AND $_GET['link']=="privacy_policy"){ 
	$privacy_policy = $db->query("SELECT * FROM content WHERE ContentLabel='Privacy and Policy'","row");
	include "user/other/privacy_policy.php";
}
elseif(isset($_GET['link']) AND $_GET['link']=="faq"){ 
	include "user/faq/index.php";
}
elseif(isset($_GET['link']) AND $_GET['link']=="account"){ 
	include "user/account/index.php";
}
elseif(isset($_GET['link']) AND $_GET['link']=="my_account"){ 
	if(!isset($_SESSION['MemberEmail'])){
		header('HTTP/1.0 404 Not Found', true, 404);
		exit();
	}
	$getCity = $db->query("SELECT * FROM city WHERE CityShow='1' ORDER BY CityName ASC","result");
	$my_account = $db->query("SELECT * FROM member WHERE MemberEmail='".$_SESSION['MemberEmail']."'","row");
	$getLocation = $db->query("SELECT * FROM location WHERE LocationShow='1' AND CityID='".$my_account['CityID']."' ORDER BY LocationTitle ASC","result");
	include "user/account/my_account.php";
}
elseif(isset($_GET['link']) AND $_GET['link']=="register"){
	$getCity = $db->query("SELECT * FROM city WHERE CityShow='1' ORDER BY CityName ASC","result");
	$getLocation = $db->query("SELECT * FROM location WHERE LocationShow='1' AND CityID='' ORDER BY LocationTitle ASC","result");
	include "user/account/register.php";
}
elseif(isset($_GET['link']) AND $_GET['link']=="change_password"){
	if(isset($_SESSION['MemberID'])){
		include "user/account/change_password.php";		
	}
}
elseif(isset($_GET['link']) AND $_GET['link']=="history_order"){
	if(isset($_SESSION['MemberID'])){
		include "user/account/history_order.php";		
	}
}
elseif(isset($_GET['link']) AND $_GET['link']=="detail_order"){
	if(isset($_SESSION['MemberID'])){
		if(isset($_GET['id'])){
			$cek_order = $db->query("SELECT * FROM trorder WHERE OrderID='".$_GET['id']."'","row");
			if($cek_order==true){
				include "user/account/detail_order.php";
			}
		}
	}
}

/* cart */
elseif(isset($_GET['link']) AND $_GET['link']=="cart"){
	if($_POST){
		if(isset($_SESSION['cart'][$_POST['services']])){
			$_SESSION['cart'][$_POST['services']]['quantity']++;
		}else{
			$_SESSION['cart'][$_POST['services']] = ['quantity'=>1];		
		}
		header('location: ?link=cart');
	}else{
		include "user/booking/cart.php";		
	}
}
elseif(isset($_GET['link']) AND $_GET['link']=="cart_delete" AND $_GET['id']!=""){
	unset($_SESSION['cart'][$_GET['id']]);
	header('location: ?link=cart');
}
elseif(isset($_GET['link']) AND $_GET['link']=="update_cart"){
	if($_POST){
		$_SESSION['cart'][$_GET['id']] = ['quantity'=>$_POST['quantity']];
		header('location: ?link=cart');
	}else{
		include "user/booking/cart.php";		
	}
}
/* cart-end */

elseif(isset($_GET['link']) AND $_GET['link']=="checkout"){ 
	$getCity = $db->query("SELECT * FROM city WHERE CityShow='1' ORDER BY CityName ASC","result");
	include "user/booking/checkout.php";
}
elseif(isset($_GET['link']) AND $_GET['link']=="checkout_preview"){ 
	include "user/booking/checkout_preview.php";
}
elseif(isset($_GET['link']) AND $_GET['link']=="checkout_review"){ 
	include "user/booking/checkout_review.php";
}
elseif(isset($_GET['link']) AND $_GET['link']=="payment_confirmation"){ 
	include "user/booking/payment_confirmation.php";
}
elseif(isset($_GET['link']) AND $_GET['link']=="logout"){ 
	unset($_SESSION['MemberID']);
	unset($_SESSION['MemberPassword']);
	unset($_SESSION['MemberEmail']);
	unset($_SESSION['MemberName']);
	header('location: ?link=index');
}
elseif(isset($_GET['link']) AND $_GET['link']=="training"){ 
	include "training.php";
}
else{
	include "user/home/index.php";
}
?>